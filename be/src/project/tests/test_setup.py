from django.test import TestCase

from perks.profiles.factories import UserFactory


class PasswordHasherTestCase(TestCase):
    def test_should_use_argon2_password_hasher(self):
        user = UserFactory()
        user.set_password("hunter2")
        self.assertEqual(user.password[:7], "argon2$")
