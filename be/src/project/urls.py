"""perks URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from graphql_jwt.decorators import jwt_cookie

from perks.api.views import CustomGraphQLView

admin.site.site_header = "Perks and Recreation Administration"

urlpatterns = [
    path("graphql/", jwt_cookie(csrf_exempt(CustomGraphQLView.as_view(graphiql=True)))),
    path("admin/", admin.site.urls),
]

# Here we add a development method for serving static media (used for both the admin site and graphiQL)
# For production, as part of the build process, we should be running `./manage.py collectstatic` to store
# any new media (js/css/img) in our STATIC_ROOT directory.
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
