from django.contrib.auth import get_user_model
from django.db.models import OneToOneField, CASCADE, ForeignKey
from django.db.models.signals import post_save
from django.dispatch import receiver

from perks.companies.models import Company
from perks.common.models import BaseModel


class Profile(BaseModel):
    user = OneToOneField(get_user_model(), on_delete=CASCADE)
    company = ForeignKey(Company, related_name="company_users", on_delete=CASCADE, null=True)

    def __str__(self):
        return self.user.email


@receiver(post_save, sender=get_user_model())
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=get_user_model())
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
