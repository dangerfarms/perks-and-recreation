import factory
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from factory import SubFactory
from factory.django import DjangoModelFactory

from perks.companies.factories import CompanyFactory
from perks.profiles.models import Profile


# This pattern is taken from:
# https://factoryboy.readthedocs.io/en/latest/recipes.html#example-django-s-profile


class ProfileFactory(DjangoModelFactory):
    class Meta:
        model = Profile

    company = SubFactory(CompanyFactory)
    # We pass in profile=None to prevent UserFactory from creating another profile
    # (this disables the RelatedFactory)
    user = factory.SubFactory("perks.profiles.factories.UserFactory", profile=None)


@factory.django.mute_signals(post_save)
class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    username = "mufasa@mutationtest.com"
    email = "mufasa@mutationtest.com"
    password = "correct-horse-battery-staple"
    first_name = "Mufasa"
    last_name = "Testington"

    # We pass in 'user' to link the generated Profile to our just-generated User
    # This will call ProfileFactory(user=our_new_user), thus skipping the SubFactory.
    profile = factory.RelatedFactory(ProfileFactory, "user")
