from django.contrib import admin
from reversion.admin import VersionAdmin

from perks.companies.models import Company


class CompanyAdmin(VersionAdmin):
    list_display = ("name", "budget")


admin.site.register(Company, CompanyAdmin)
