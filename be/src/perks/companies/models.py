from django.db.models import TextField
from djmoney.models.fields import MoneyField
import reversion

from perks.common.models import BaseModel


@reversion.register()
class Company(BaseModel):
    class Meta:
        verbose_name_plural = "Companies"

    name = TextField(unique=True)
    budget = MoneyField(max_digits=14, decimal_places=2, default=0, default_currency="GBP")

    def __str__(self):
        return self.name
