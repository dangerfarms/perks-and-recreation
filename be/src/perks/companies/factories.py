from factory.django import DjangoModelFactory
from djmoney.money import Money

from perks.companies.models import Company


class CompanyFactory(DjangoModelFactory):
    class Meta:
        model = Company
        django_get_or_create = ("name", "budget")

    name = "Company Name"
    budget = Money(0, "GBP")
