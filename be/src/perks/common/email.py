from django.core.mail import EmailMultiAlternatives


def send_email(subject, body, from_email, to_emails, reply_to_emails, html=None, tags=None):
    msg = EmailMultiAlternatives(
        subject=subject, body=body, from_email=from_email, to=to_emails, reply_to=reply_to_emails
    )

    if html:
        msg.attach_alternative(html, "text/html")

    if tags:
        msg.tags = tags

    msg.send()


def send_invite_email(email_address, user_name, company_name, invite_token):
    subject = "Invitation"
    body = f"Hello {user_name}. You have been invited to sign up for Perks by {company_name}"
    from_email = "developers@dangerfarms.com"
    to_emails = [email_address]
    reply_to_emails = ["developers@dangerfarms.com"]

    template = "<h1>Invite!</h1>" "<p>Hello {user_name}. You have been invited to join Perks by {company_name}</p>"

    html = template.format(user_name=user_name, company_name=company_name)
    tags = ["invitation"]

    send_email(subject, body, from_email, to_emails, reply_to_emails, html, tags)


# Optional Anymail extensions:
# msg.metadata = {"user_id": "8675309", "experiment_variation": 1}
# msg.track_clicks = True
