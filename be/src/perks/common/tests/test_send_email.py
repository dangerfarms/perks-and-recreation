from django.core import mail
from django.test import TestCase
from django.test.utils import override_settings

from perks.common.email import send_invite_email


@override_settings(EMAIL_BACKEND="anymail.backends.test.EmailBackend")
class EmailTestCase(TestCase):
    def test_sends_invitation_email(self):
        recipient_email_address = "test@testing.test"
        recipient_name = "Mr Test"
        company_name = "Test Inc"
        send_invite_email(recipient_email_address, recipient_name, company_name, None)

        expected_body = f"Hello {recipient_name}. You have been invited to sign up for Perks by {company_name}"
        expected_html_content = (
            f"<h1>Invite!</h1><p>Hello {recipient_name}. You have been invited to join Perks by {company_name}</p>"
        )

        # Test that one message was sent:
        self.assertEqual(len(mail.outbox), 1)

        # Verify attributes of the EmailMessage that was sent:
        self.assertEqual(mail.outbox[0].body, expected_body)

        html_content = mail.outbox[0].alternatives[0][0]
        self.assertEqual(html_content, expected_html_content)

        self.assertEqual(mail.outbox[0].to, [recipient_email_address])
        self.assertEqual(mail.outbox[0].tags, ["invitation"])
