from perks.companies.models import Company


class AdministrateCompany:
    """This is not actually used, and is only here as an example of a custom permission.

    We may not want to use such permissions in most cases, as Django permissions are automatically created for
    CRUD operations on all models, which may be sufficient for our auth needs."""

    codename = "administrate_company"
    name = "Can be the admin of a company"
    content_type_model = Company
