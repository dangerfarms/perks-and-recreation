from perks.permissions.permissions import AdministrateCompany
from perks.permissions.groups import Admin

default_permissions = [AdministrateCompany]
default_groups = [Admin]
