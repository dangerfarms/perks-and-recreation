from django.contrib.auth.models import Group

from perks.permissions.permissions import AdministrateCompany


class Groups:
    ADMIN = "Admin"


class Admin:
    name = Groups.ADMIN
    permissions = [AdministrateCompany]


def get_group_object(group):
    return Group.objects.get(name=group.name)
