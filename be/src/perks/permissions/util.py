from django.contrib.auth.models import Permission, Group

from perks.permissions.defaults import default_permissions, default_groups


def migrate_default_permissions():
    """Set up permissions as defined in the defaults."""
    for permission in default_permissions:
        if Permission.objects.filter(name=permission.name).count() > 0:
            print(f"Permission with name {permission.name} already exists. Skipping.")
            continue

        permission_object = Permission(
            name=permission.name, codename=permission.codename, content_type=permission.content_type
        )
        permission_object.save()


def migrate_default_groups():
    """Set up groups as defined in the defaults."""
    for group in default_groups:
        if Group.objects.filter(name=group.name).count() > 0:
            print(f"Group with name {group.name} already exists. Skipping.")
            continue

        group_object = Group(name=group.name)
        group_object.save()
        permission_objects = [Permission.objects.get(name=permission.name) for permission in group.permissions]
        for permission_object in permission_objects:
            group_object.permissions.add(permission_object)
