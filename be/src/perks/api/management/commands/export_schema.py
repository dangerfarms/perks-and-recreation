from django.conf import settings
from django.core.management.base import BaseCommand

from perks.api.schema import schema


class Command(BaseCommand):
    help = "Export the API's schema to a .graphql file"

    def handle(self, *args, **options):
        file_name = f"{settings.BASE_DIR}/schema.graphql"

        with open(file_name, "w") as f:
            f.write(str(schema))

        self.stdout.write(f"Schema exported as {file_name}")
