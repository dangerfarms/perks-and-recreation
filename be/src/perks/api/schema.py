import graphene
import graphql_jwt
from graphql_jwt.decorators import login_required
from graphql_jwt.exceptions import PermissionDenied

from perks.api.mutations.log_out import LogoutMutation
from perks.api.mutations.token_auth import ObtainJSONWebToken
from perks.api.types.profile import ProfileType
from perks.companies.models import Company
from perks.api.types.company import CompanyType
from perks.api.mutations.create_company import CreateCompany
from perks.api.mutations.set_budget import SetBudget


class Query(graphene.ObjectType):
    company = graphene.Field(CompanyType, id=graphene.Argument(type=graphene.ID))
    me = graphene.Field(ProfileType)

    @staticmethod
    @login_required
    def resolve_company(self, info, id):
        if str(info.context.user.profile.company.id) != id:
            raise PermissionDenied()

        return Company.objects.get(id=id)

    @staticmethod
    @login_required
    def resolve_me(self, info):
        return info.context.user.profile


class Mutations(graphene.ObjectType):
    create_company = CreateCompany.Field()
    set_budget = SetBudget.Field()

    token_auth = ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    log_out = LogoutMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutations, types=[ProfileType])
