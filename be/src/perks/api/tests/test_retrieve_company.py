from perks.api.tests import PerksAPITestCase
from perks.companies.factories import CompanyFactory
from perks.profiles.factories import UserFactory
from utils.test_utils import hide_graphql_errors


class RetrieveCompanyTestCase(PerksAPITestCase):
    query = """
        query ($id: ID!) {
            company(id: $id) {
                name
            }
        }
    """

    @hide_graphql_errors
    def test_should_return_an_error_when_not_authenticated(self):
        company = CompanyFactory(name="Test Company")

        response = self.client.execute(self.query, variables={"id": company.id})

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "You do not have permission to perform this action")

    @hide_graphql_errors
    def test_should_return_an_error_when_authenticated_but_not_a_member_of_this_company(self):
        company = CompanyFactory(name="Test Company")
        other_company = CompanyFactory(name="Other Test Company")
        user = UserFactory(
            email="test@email.com",
            username="test@email.com",
            password="correct-horse-battery-staple",
            profile__company=other_company,
        )
        self.client.authenticate(user)

        response = self.client.execute(self.query, variables={"id": company.id})

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "You do not have permission to perform this action")

    def test_should_return_a_company_by_id_when_authenticated_as_member_of_that_company(self):
        company = CompanyFactory(name="Test Company")
        user = UserFactory(
            email="test@email.com",
            username="test@email.com",
            password="correct-horse-battery-staple",
            profile__company=company,
        )
        self.client.authenticate(user)

        response = self.client.execute(
            """
            query ($id: ID!) {
                company(id: $id) {
                    name
                }
            }
            """,
            variables={"id": company.id},
        )

        self.assertResponseNoErrors(response)
        self.assertResponseDataEquals(response, {"company": {"name": "Test Company"}})

    @hide_graphql_errors
    def test_should_return_error_when_company_with_id_does_not_exist(self):
        company = CompanyFactory(name="Other Test Company Inc")
        user = UserFactory(
            email="test@email.com",
            username="test@email.com",
            password="correct-horse-battery-staple",
            profile__company=company,
        )
        self.client.authenticate(user)

        response = self.client.execute(query=self.query, variables={"id": "does-not-exist"})

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "You do not have permission to perform this action")
