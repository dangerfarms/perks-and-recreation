from djmoney.money import Money
from reversion.models import Version

from perks.api.tests import PerksAPITestCase
from perks.companies.factories import CompanyFactory
from perks.profiles.factories import UserFactory
from perks.permissions.groups import Admin, get_group_object
from utils.test_utils import hide_graphql_errors


class SetBudgetTestCase(PerksAPITestCase):
    @hide_graphql_errors
    def test_should_return_an_error_when_not_authenticated(self):
        CompanyFactory(name="Mutation Test Inc")

        response = self.client.execute(
            """
            mutation SetBudget(
                $budget: MoneyField!
            ) {
                setBudget(
                    budget: $budget
                ) {
                    budget
                }
            }
            """,
            op_name="SetBudget",
            variables={"budget": "239.37"},
        )

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "You do not have permission to perform this action")

    @hide_graphql_errors
    def test_should_return_an_error_when_authenticated_but_not_authorised(self):
        company_name = "Mutation Test Inc"
        company = CompanyFactory(name=company_name)
        user = UserFactory(
            email="test@email.com",
            username="test@email.com",
            password="correct-horse-battery-staple",
            profile__company=company,
        )
        self.client.authenticate(user)

        response = self.client.execute(
            """
            mutation SetBudget(
                $budget: MoneyField!
            ) {
                setBudget(
                    budget: $budget
                ) {
                    budget
                }
            }
            """,
            op_name="SetBudget",
            variables={"budget": "239.37"},
        )

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "You do not have permission to perform this action")

    def test_should_set_budget(self):
        company_name = "Mutation Test Inc"
        company = CompanyFactory(name=company_name)
        user = UserFactory(
            email="test@email.com",
            username="test@email.com",
            password="correct-horse-battery-staple",
            profile__company=company,
        )
        user.groups.add(get_group_object(Admin))
        self.client.authenticate(user)

        expected_content = {"setBudget": {"budget": "239.37"}}

        response = self.client.execute(
            """
            mutation SetBudget(
                $budget: MoneyField!
            ) {
                setBudget(
                    budget: $budget
                ) {
                    budget
                }
            }
            """,
            op_name="SetBudget",
            variables={"budget": "239.37"},
        )

        self.assertResponseNoErrors(response)
        self.assertResponseDataEquals(response, expected_content)

    def test_budget_change_history_is_recorded(self):
        company_name = "Mutation Test Inc"
        company = CompanyFactory(name=company_name)
        user = UserFactory(
            email="test@email.com",
            username="test@email.com",
            password="correct-horse-battery-staple",
            profile__company=company,
        )
        user.groups.add(get_group_object(Admin))
        self.client.authenticate(user)

        new_budget = "100.25"

        self.client.execute(
            """
            mutation SetBudget(
                $budget: MoneyField!
            ) {
                setBudget(
                    budget: $budget
                ) {
                    budget
                }
            }
            """,
            op_name="SetBudget",
            variables={"budget": new_budget},
        )

        company_history = Version.objects.get_for_object(company)

        self.assertEqual(company_history[0].field_dict.get("budget"), Money(new_budget, "GBP"))
        self.assertEqual(company_history[0].revision.user.__str__(), "test@email.com")
