from json import loads, dumps

from graphene_django.utils import GraphQLTestCase
from graphql.execution import ExecutionResult
from graphql_jwt.testcases import JSONWebTokenClient
from perks.api import schema


class PerksAPITestCase(GraphQLTestCase):
    """There are two main ways to make a query with this test case:

    The first is to use self.query. This makes a post request using Django's test case, under the hood.
    This should be viewed as an actual web request.

    The second is to use self.client.execute. This executes your query on the schema, and should be viewed as
    a test on the schema, rather than an integration test of the whole API.

    The latter case will be used more often, as it enables easy authentication as different user types:
    https://django-graphql-jwt.domake.io/en/latest/tests.html

    The first case will be useful in cases where we're interested in HTTP specificities, or sanity checks
    that a full integration is working.

    TODO: Discuss:
        Having to support two different modes in each assertion is somewhat cumbersome. Consider splitting into two
        TestCases: one that uses queries, and one (PerksSchemaTestCase) that uses self.client.execute and is useful
        for validated schema executions.
    """

    GRAPHQL_SCHEMA = schema
    GRAPHQL_URL = "/graphql/"

    client_class = JSONWebTokenClient

    def assertResponseNoErrors(self, response):
        if isinstance(response, ExecutionResult):
            return self.assertIsNone(response.errors)
        else:
            return super().assertResponseNoErrors(response)

    def assertResponseHasErrors(self, response):
        if isinstance(response, ExecutionResult):
            return self.assertIsNotNone(response.errors)
        else:
            return super().assertResponseHasErrors(response)

    def assertResponseHasError(self, response, error_string):
        if isinstance(response, ExecutionResult):
            self.assertIsNotNone(response.errors, "Response has no errors.")
            error_messages = [str(message) for message in response.errors]
            self.assertIn(error_string, error_messages)
        else:
            error_messages = [error["message"] for error in response.json()["errors"]]
            self.assertIn(error_string, error_messages)

    def assertResponseDataEquals(self, response, expected):
        """This ignores the ordering that is granted by direct schema executions in favour of a consistent,
        easy interface for comparing the result for either schema executions or HTTP queries.

        If you want to test ordering, use assertEquals directly on the response.
        """
        if isinstance(response, ExecutionResult):
            return self.assertEqual(loads(dumps(response.data)), expected)
        else:
            return self.assertEqual(response.json()["data"], expected)
