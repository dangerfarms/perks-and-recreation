from django.test import TestCase
from freezegun import freeze_time

from perks.companies.models import Company


class BaseModelTestCase(TestCase):
    @freeze_time("2019-12-17T11:04:12.827719+00:00")
    def test_should_add_create_at_and_modified_at_on_create(self):
        company = Company(name="Test company")
        company.save()

        self.assertEqual("2019-12-17T11:04:12.827719+00:00", company.created_at.isoformat())
        self.assertEqual("2019-12-17T11:04:12.827719+00:00", company.modified_at.isoformat())

    def test_should_update_modified_at_and_not_change_created_at_on_update(self):
        test_create_at = "2019-12-17T11:04:12.827719+00:00"
        test_modified_at = "2019-12-18T11:04:12.827719+00:00"

        with freeze_time(test_create_at):
            company = Company(name="Test company")
            company.save()

        with freeze_time(test_modified_at):
            company.name = "Updated name"
            company.save()

        self.assertEqual(test_create_at, company.created_at.isoformat())
        self.assertEqual(test_modified_at, company.modified_at.isoformat())
