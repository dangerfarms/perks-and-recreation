import json

from django.contrib.auth import get_user_model

from perks.api.tests import PerksAPITestCase
from perks.profiles.factories import UserFactory
from utils.test_utils import hide_graphql_errors
from perks.companies.factories import CompanyFactory
from perks.companies.models import Company
from perks.permissions.groups import get_group_object, Admin


class CreateCompanyTestCase(PerksAPITestCase):
    @hide_graphql_errors
    def test_should_fail_if_email_address_is_invalid(self):
        response = self.client.execute(
            """
            mutation CreateCompany(
                $name: String!
                $admin: UserInput!
            ) {
                createCompany(
                    name: $name
                    admin: $admin
                ) {
                    company {
                        name
                        companyUsers {
                            user {
                                firstName
                                lastName
                                email
                            }
                        }
                    }
                }
            }
            """,
            op_name="CreateCompany",
            variables={
                "name": "Mutation Test Inc",
                "admin": {
                    "firstName": "Mufasa",
                    "lastName": "Testington",
                    "email": "mufasa",
                    "password": "correct-horse-battery-staple",
                },
            },
        )

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "Email address is invalid.")

        user_count = get_user_model().objects.count()
        self.assertEqual(user_count, 0)

    def test_should_create_company_and_admin_if_both_are_new(self):
        response = self.client.execute(
            """
            mutation CreateCompany(
                $name: String!
                $admin: UserInput!
            ) {
                createCompany(
                    name: $name
                    admin: $admin
                ) {
                    company {
                        name
                        companyUsers {
                            user {
                                firstName
                                lastName
                                email
                            }
                        }
                    }
                }
            }
            """,
            op_name="CreateCompany",
            variables={
                "name": "Mutation Test Inc",
                "admin": {
                    "firstName": "Mufasa",
                    "lastName": "Testington",
                    "email": "mufasa@mutationtest.com",
                    "password": "correct-horse-battery-staple",
                },
            },
        )

        self.assertResponseNoErrors(response)

        self.assertResponseDataEquals(
            response,
            {
                "createCompany": {
                    "company": {
                        "name": "Mutation Test Inc",
                        "companyUsers": [
                            {
                                "user": {
                                    "firstName": "Mufasa",
                                    "lastName": "Testington",
                                    "email": "mufasa@mutationtest.com",
                                }
                            }
                        ],
                    }
                }
            },
        )

        users = get_user_model().objects.all()
        self.assertEqual(len(users), 1)

        user = users[0]
        self.assertEqual(user.username, "mufasa@mutationtest.com")
        self.assertEqual(user.email, "mufasa@mutationtest.com")

        user_groups = user.groups.all()
        admin_group = get_group_object(Admin)

        self.assertIn(admin_group, user_groups)

        company = Company.objects.get(name="Mutation Test Inc")
        self.assertEqual(user.profile.company, company)

    @hide_graphql_errors
    def test_should_fail_when_company_with_same_name_already_exists(self):
        existing_company_name = "Existing Company"
        CompanyFactory(name=existing_company_name)

        response = self.client.execute(
            """
            mutation CreateCompany(
                $name: String!
                $admin: UserInput!
            ) {
                createCompany(
                    name: $name
                    admin: $admin
                ) {
                    company {
                        name
                    }
                }
            }
            """,
            op_name="CreateCompany",
            variables={
                "name": existing_company_name,
                "admin": {
                    "firstName": "Mufasa",
                    "lastName": "Testington",
                    "email": "test@email.com",
                    "password": "correct-horse-battery-staple",
                },
            },
        )

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "A company with this name already exists. Company names must be unique.")

        company_count = Company.objects.count()
        self.assertEqual(company_count, 1)

    @hide_graphql_errors
    def test_should_not_register_if_password_is_too_short(self):
        response = self.client.execute(
            """
            mutation CreateCompany(
                $name: String!
                $admin: UserInput!
            ) {
                createCompany(
                    name: $name
                    admin: $admin
                ) {
                    company {
                        name
                    }
                }
            }
            """,
            op_name="CreateCompany",
            variables={
                "name": "Existing Company",
                "admin": {
                    "firstName": "Mufasa",
                    "lastName": "Testington",
                    "email": "test@email.com",
                    "password": "short",
                },
            },
        )

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "Password length must be at least 8 characters.")

        user_count = get_user_model().objects.count()
        self.assertEqual(user_count, 0)

    @hide_graphql_errors
    def test_should_not_register_if_user_already_exists_with_that_email_address(self):
        get_user_model().objects.create_user(
            username="test@email.com",
            email="test@email.com",
            first_name="Mufasa",
            last_name="Testington",
            password="doesnt_matter",
        )

        response = self.client.execute(
            """
            mutation CreateCompany(
                $name: String!
                $admin: UserInput!
            ) {
                createCompany(
                    name: $name
                    admin: $admin
                ) {
                    company {
                        name
                    }
                }
            }
            """,
            op_name="CreateCompany",
            variables={
                "name": "Existing Company",
                "admin": {
                    "firstName": "Mufasa",
                    "lastName": "Testington",
                    "email": "test@email.com",
                    "password": "correct-horse-battery-staple",
                },
            },
        )

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "A user with this email address already exists.")

        user_count = get_user_model().objects.count()
        self.assertEqual(user_count, 1)

    @hide_graphql_errors
    def test_should_fail_if_already_logged_in(self):
        user = UserFactory()
        self.client.authenticate(user)

        response = self.client.execute(
            """
            mutation CreateCompany(
                $name: String!
                $admin: UserInput!
            ) {
                createCompany(
                    name: $name
                    admin: $admin
                ) {
                    company {
                        name
                        companyUsers {
                            user {
                                firstName
                                lastName
                                email
                            }
                        }
                    }
                }
            }
            """,
            op_name="CreateCompany",
            variables={
                "name": "New Company Inc",
                "admin": {
                    "firstName": "Jane",
                    "lastName": "Bloggs",
                    "email": "janebloggs@mutationtest.com",
                    "password": "correct-horse-battery-staple",
                },
            },
        )

        self.assertResponseHasErrors(response)
        self.assertResponseHasError(response, "You do not have permission to perform this action")

        user_count = get_user_model().objects.count()
        self.assertEqual(user_count, 1)

    def test_create_budget_when_creating_company(self):
        response = self.query(
            """
            mutation CreateCompany(
                $name: String!
                $admin: UserInput!
            ) {
                createCompany(
                    name: $name
                    admin: $admin
                ) {
                    company {
                        budget
                    }
                }
            }
            """,
            op_name="CreateCompany",
            variables={
                "name": "Mutation Test Inc",
                "admin": {
                    "firstName": "Mufasa",
                    "lastName": "Testington",
                    "email": "mufasa@mutationtest.com",
                    "password": "correct-horse-battery-staple",
                },
            },
        )

        content = json.loads(response.content)

        self.assertResponseNoErrors(response)

        budget = content["data"]["createCompany"]["company"]["budget"]

        self.assertEqual(budget, "0.00")
