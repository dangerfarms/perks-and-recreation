from perks.api.tests import PerksAPITestCase
from perks.profiles.factories import UserFactory
from utils.test_utils import hide_graphql_errors


class MeTestCase(PerksAPITestCase):
    @hide_graphql_errors
    def test_should_return_error_if_not_logged_in(self):
        result = self.client.execute("{ me { user { firstName } } }")
        self.assertResponseHasError(result, "You do not have permission to perform this action")
        self.assertResponseDataEquals(result, {"me": None})

    def test_should_return_profile_information_when_user_is_logged_in(self):
        user = UserFactory()
        self.client.authenticate(user)
        result = self.client.execute(
            """
            {
                me {
                    user {
                        firstName
                        lastName
                    }
                    company {
                        name
                    }
                }
            }
        """
        )
        self.assertResponseNoErrors(result)
        self.assertEqual(
            result.data,
            {
                "me": {
                    "user": {"firstName": user.first_name, "lastName": user.last_name},
                    "company": {"name": user.profile.company.name},
                }
            },
        )

    def test_should_not_return_password(self):
        user = UserFactory()
        self.client.authenticate(user)
        result = self.client.execute(
            """
            {
                me {
                    user {
                        firstName
                        lastName
                        password
                    }
                }
            }
        """
        )
        self.assertResponseHasError(result, 'Cannot query field "password" on type "UserType".')
        self.assertResponseDataEquals(result, None)

    def test_should_not_return_user_id(self):
        user = UserFactory()
        self.client.authenticate(user)
        result = self.client.execute(
            """
            {
                me {
                    user {
                        id
                    }
                }
            }
            """
        )
        self.assertResponseHasError(result, 'Cannot query field "id" on type "UserType".')
        self.assertResponseDataEquals(result, None)
