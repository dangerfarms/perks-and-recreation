from perks.api.tests import PerksAPITestCase
from perks.profiles.factories import UserFactory
from perks.companies.models import Company
from utils.test_utils import hide_graphql_errors


class RetrieveBudgetTestCase(PerksAPITestCase):
    @hide_graphql_errors
    def test_should_retrieve_initial_budget_via_company_query_after_creating_company(self):
        company_name = "Mutation Test Inc"

        self.query(
            """
            mutation CreateCompany(
                $name: String!
                $admin: UserInput!
            ) {
                createCompany(
                    name: $name
                    admin: $admin
                ) {
                    company {
                        id
                        name
                    }
                }
            }
            """,
            op_name="CreateCompany",
            variables={
                "name": "Mutation Test Inc",
                "admin": {
                    "firstName": "Mufasa",
                    "lastName": "Testington",
                    "email": "mufasa@mutationtest.com",
                    "password": "correct-horse-battery-staple",
                },
            },
        )

        company = Company.objects.get(name=company_name)

        user = UserFactory(
            email="test@email.com",
            username="test@email.com",
            password="correct-horse-battery-staple",
            profile__company=company,
        )
        self.client.authenticate(user)

        expected_content = {"company": {"budget": "0.00"}}

        response = self.client.execute(
            """
                query ($id: ID!) {
                    company(id: $id) {
                        budget
                    }
                }
            """,
            op_name="GetCompany",
            variables={"id": company.id},
        )

        self.assertResponseNoErrors(response)
        self.assertResponseDataEquals(response, expected_content)
