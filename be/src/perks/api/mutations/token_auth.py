import graphql_jwt
from graphql import GraphQLError
from graphql_jwt.exceptions import JSONWebTokenError


# TODO: Untested, but the only change is to a human-readable string, otherwise the 3rd party lib is assumed to be solid.
#       Also, error handling details will soon change as part of another task anyway.
#       (https://app.clubhouse.io/dangerfarms/story/30/handle-graphql-errors-better)
class ObtainJSONWebToken(graphql_jwt.JSONWebTokenMutation):
    @classmethod
    def mutate(cls, root, info, **kwargs):
        try:
            return super().mutate(root, info, **kwargs)
        except JSONWebTokenError:

            # TODO: just going along with convention here for now, but should handle error better as part of
            #       https://app.clubhouse.io/dangerfarms/story/30/handle-graphql-errors-better
            raise GraphQLError(
                "We couldn't find a user with that email and password. Please double-check your login details."
            )

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls()
