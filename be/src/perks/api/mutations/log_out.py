from graphene import Field, Mutation, String
from graphql_jwt.refresh_token.shortcuts import get_refresh_token


class LogoutMutation(Mutation):
    """Logs the user out by removing the JWT cookie and revokes the given refresh token.

    The removal of the cookie actually happens in the `CustomGraphQLView`.
    """

    class Arguments:
        refresh_token = String(required=True)

    message = Field(String)

    @staticmethod
    def mutate(self, info, refresh_token):
        refresh_token_obj = get_refresh_token(refresh_token, info.context)
        refresh_token_obj.revoke(info.context)

        return LogoutMutation(message="Successfully logged out")
