import graphene
import reversion
from graphql_jwt.decorators import login_required
from graphql_jwt.exceptions import PermissionDenied

from perks.api.types.company import MoneyField
from perks.companies.models import Company
from perks.permissions.groups import Admin, get_group_object


class SetBudget(graphene.Mutation):
    class Arguments:
        budget = MoneyField(required=True)

    budget = MoneyField()

    @staticmethod
    @login_required
    def mutate(self, info, budget):
        user_groups = info.context.user.groups.all()
        if not get_group_object(Admin) in user_groups:
            raise PermissionDenied()

        company = Company.objects.get(id=info.context.user.profile.company.id)

        with reversion.create_revision():
            company.budget = budget
            company.save()
            reversion.set_user(info.context.user)

        return SetBudget(budget=budget)
