import re

import graphene
from django.contrib.auth import get_user_model
from graphql import GraphQLError
from django.db import transaction
from django.db.utils import IntegrityError
from graphql_jwt.decorators import user_passes_test

from perks.api.inputs.user import UserInput
from perks.api.types.company import CompanyType
from perks.companies.models import Company
from perks.permissions.groups import Admin, get_group_object


class CreateCompany(graphene.Mutation):
    """Create a company as an unauthenticated user.
    A Company and a Company Admin is created by this mutation in an atomic transaction."""

    class Arguments:
        name = graphene.String(required=True)
        admin = UserInput(required=True)

    company = graphene.Field(CompanyType)

    @staticmethod
    @user_passes_test(lambda u: not u.is_authenticated)
    def mutate(self, info, name, admin):
        existing_companies = Company.objects.filter(name=name)

        if not re.fullmatch(r"[^@]+@[^@]+\.[^@]+", admin["email"]):
            raise GraphQLError("Email address is invalid.")

        if len(admin["password"]) < 8:
            raise GraphQLError("Password length must be at least 8 characters.")

        if len(existing_companies) > 0:
            raise GraphQLError("A company with this name already exists. Company names must be unique.")

        existing_admin = get_user_model().objects.filter(email=admin["email"])
        if len(existing_admin) > 0:
            raise GraphQLError("A user with this email address already exists.")

        with transaction.atomic():
            admin_object = get_user_model().objects.create_user(
                username=admin["email"],
                email=admin["email"],
                first_name=admin["first_name"],
                last_name=admin["last_name"],
                password=admin["password"],
                is_active=True,
            )
            admin_object.save()
            admin_object.groups.add(get_group_object(Admin))

            try:
                company = Company(name=name)
                company.save()
            except IntegrityError:
                raise GraphQLError("A company with this name already exists. Company names must be unique.")

            admin_object.profile.company = company
            admin_object.save()

        return CreateCompany(company=company)
