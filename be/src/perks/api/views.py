from graphene_django.views import GraphQLView


class CustomGraphQLView(GraphQLView):
    def _delete_cookies_on_response_if_needed(self, request, response):
        data = self.parse_body(request)
        operation_name = self.get_graphql_params(request, data)[2]

        if operation_name and operation_name == "Logout":
            response.delete_cookie("JWT")

        return response

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        response = self._delete_cookies_on_response_if_needed(request, response)
        return response
