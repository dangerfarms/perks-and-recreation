from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = "perks.api"

    def ready(self):
        # This import connects the receivers in the signals module
        import perks.api.signals  # noqa
