import graphene
from graphene_django import DjangoObjectType
from djmoney.money import Money
from graphene.types import Scalar
from graphql.language import ast

from perks.companies.models import Company


class MoneyField(Scalar):
    @staticmethod
    def serialize(money):
        if money is None:
            return None

        elif isinstance(money, Money):
            return f"{money.amount:.{2}f}"

        raise NotImplementedError

    @staticmethod
    def parse_literal(node):
        if isinstance(node, ast.StringValue):
            return MoneyField.parse_value(node.value)

    @staticmethod
    def parse_value(value):
        return Money(amount=float(value), currency="GBP")


__all__ = ("MoneyField",)


class CompanyType(DjangoObjectType):
    class Meta:
        model = Company

    name = graphene.String()
    budget = graphene.Field(MoneyField)
