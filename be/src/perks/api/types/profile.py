from django.contrib.auth import get_user_model
from graphene_django import DjangoObjectType

from perks.profiles.models import Profile


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        exclude = ("id", "password")


class ProfileType(DjangoObjectType):
    class Meta:
        model = Profile
