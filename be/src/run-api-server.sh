#!/usr/bin/env sh

# Start the server
/usr/local/bin/gunicorn project.wsgi:application --workers 1 --threads 8 --bind :$PORT
