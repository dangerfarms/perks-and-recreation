import logging


def hide_graphql_errors(func):
    """When a GraphQLError is handled within the app code, it is still shown in the test output. This decorator
    disables logging so that we don't have a passing test that prints error messages.
    """

    def wrapper(*args, **kwargs):
        logging.disable(logging.CRITICAL)

        func(*args, **kwargs)

        logging.disable(logging.NOTSET)

    return wrapper
