FROM python:3.7.5-slim-buster

RUN apt-get update ; apt-get install -y libpq-dev build-essential

RUN mkdir -p /app
WORKDIR /app

COPY requirements.deploy.txt /app/
COPY requirements.txt /app/

RUN pip install --no-cache-dir -r requirements.deploy.txt

RUN ln -sf /dev/stdout /var/log/access.log && \
    ln -sf /dev/stderr /var/log/error.log

ADD ./src /app
CMD ["./run-api-server.sh"]
