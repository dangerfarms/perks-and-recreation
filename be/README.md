**1. Create a virtualenv and install dependencies:**

From the repo root (**not** the `be` directory!), run the `make setup` command to set up both the backend and the frontend.

**2. Run server locally:**

First start the Postgres database container:

```bash
cd be
docker-compose up
```

Activate venv:

```bash
. ./venv/bin/activate
```

Run the django app:
```bash
python src/manage.py runserver
```


## Django Admin Panel

If you want to use the admin panel, create a superuser:

`python src/manage.py createsuperuser`

This will create a new User object in the database with superuser privileges.

Then navigate to `localhost:8000/admin` and sign in using your superuser details.
