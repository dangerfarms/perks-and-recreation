#!/usr/bin/env bash

# Exit early if any step completes with a bad error code.
# Without this, linting errors will slip through the net
# https://app.clubhouse.io/dangerfarms/story/168/lint-errors-no-longer-prevents-commit-hook-from-failing
set -e

# Ensure hooks are run in virtualenv
if [[ "$VIRTUAL_ENV" == "" ]]
then
  source be/venv/bin/activate
  echo "Activated virtualenv"
fi

yarn lerna run --concurrency 1 --stream precommit

cd fe
CI=true yarn test
