#!/usr/bin/env bash

old_path="$(pwd)"
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

cd $SCRIPTPATH

cp ch-commit-msg.sh ../.git/hooks/commit-msg && \
chmod +x ../.git/hooks/commit-msg && \
echo "commit-msg hook installed"

cd $old_path
