#!/usr/bin/env bash
set -e

if test ! -f "../infra/secrets.tfvars"; then
  echo "You must set the secret variables in infra/secrets.tfvars based on infra/secrets/tfvars.templates."
  exit 1
fi