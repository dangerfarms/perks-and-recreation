#!/usr/bin/env bash
set -e

if [[ ! "$CLOUDFLARE_EMAIL" ]]; then
  echo "You must set the CLOUDFLARE_EMAIL and CLOUDFLARE_API_KEY env vars."
  exit 1
fi

if [[ ! "$CLOUDFLARE_API_KEY" ]]; then
  echo "You must set the CLOUDFLARE_EMAIL and CLOUDFLARE_API_KEY env vars."
  exit 1
fi

