#!/usr/bin/env bash
set -x

cd be || ( echo "Directory 'be' doesn't exist. Please run ./setup.sh from the project root." && exit 1 )
python3 -m venv venv || ( echo "'venv' failed to create - this might be OK if venv already exists." )
. venv/bin/activate
pip install -r requirements.local.txt
docker-compose build
yarn
if [ ! -f src/.env ]; then
  cp src/.env.template src/.env
fi