#!/usr/bin/env bash
set -x
cd infra
if [ ! -f secrets.tfvars ]; then
  cp secret.tfvars.template secret.tfvars
fi
