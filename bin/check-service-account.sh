#!/usr/bin/env bash
set -e

if test ! -f "$GOOGLE_APPLICATION_CREDENTIALS" || [[ ! "$GOOGLE_APPLICATION_CREDENTIALS" == *"perks-service-account.json"* ]]; then
  echo "You must set GOOGLE_APPLICATION_CREDENTIALS env var to a gitignored file perks-service-account.json"
  exit 1
fi

service_account_email=$(jq -r ".client_email" "$GOOGLE_APPLICATION_CREDENTIALS")

roles=$(gcloud projects get-iam-policy danger-farms \
  --flatten="bindings[].members" \
  --format='json(bindings.role)' \
  --filter="bindings.members:$service_account_email" |
  jq -j 'map(.bindings.role) | join(",")')

if [[ ! "$roles" == *"roles/run.admin"* ]] && [[ ! "$roles" == *"roles/editor"* ]]; then
  printf "Not all roles are assigned to the service account.\n%s\n%s" "$service_account_email" "$roles"
  exit 1;
fi
