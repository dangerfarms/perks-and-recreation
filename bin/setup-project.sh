#!/usr/bin/env bash
set -x

yarn

if [ ! -f .deploy.env ]; then
  cp .deploy.env.template .deploy.env
fi

./bin/install-ch-commit-msg.sh
