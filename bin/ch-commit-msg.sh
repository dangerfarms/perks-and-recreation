#!/usr/bin/env bash

# Allows us to read user input below, assigns stdin to keyboard
exec < /dev/tty

temp_commit_msg_file=$1

# Stop if an story id is found in the commit message
if [ "$(grep -E 'ch[0-9]+' $temp_commit_msg_file)" > /dev/null ]; then
  echo "[commit-msg hook] Story id found in commit message"
  exit 0
fi

# Get the story id from the branch name
story_id="$(git branch | grep \* | cut -d ' ' -f2 | grep -o -E 'ch[0-9]+' | tr -d '/')"

# If we got the story id from the branch name...
if [[ "$story_id" =~ ^ch[0-9]+$ ]] ; then
  # Add it to the commit message
  echo "[commit-msg hook] Got story id $story_id from branch name"
  echo "$(cat $temp_commit_msg_file) [$story_id]" > "$temp_commit_msg_file"
else
  # Otherwise, prompt the user for the id
  while true
  do
    read -p "[commit-msg hook] Please enter the story id (leave blank if n/a): " story_id
    if [[ "$story_id" =~ ^[0-9]+$ ]] ; then
      # Accept the id as the number only
      echo "$(cat $temp_commit_msg_file) [ch$story_id]" > "$temp_commit_msg_file"
      exit 0
    elif [[ "$story_id" =~ ^ch[0-9]+$ ]] ; then
      # Accept the id as the number with the 'ch' prefix
      echo "$(cat $temp_commit_msg_file) [$story_id]" > "$temp_commit_msg_file"
      exit 0
    elif [[ "$story_id" == "" ]] ; then
      # Accept no input - story id not applicable
      exit 0
    else
      # Prompt again on invalid story id
      echo "[commit-msg hook] Invalid story id: $story_id"
    fi
  done
fi
