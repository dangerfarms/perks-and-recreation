#!/usr/bin/env bash
set -x
cd fe || ( echo "Could not find 'fe' directory." && exit 1 )
yarn
if [ ! -f .env ]; then
  cp .env.template .env
fi

if [ ! -f cypress.json ]; then
  cp cypress.template.json cypress.json
fi

if [ ! -f deploy/.secrets ]; then
  cp deploy/.secrets.template deploy/.secrets
fi