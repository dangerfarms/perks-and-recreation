These custom docker images are used in Gitlab CI to run jobs. They're currently built and pushed manually.

Built images are hosted in Gitlab Container Registry: that's the easiest place for Gitlab CI to pull them from.

> **Important:** use versioned tags rather than leaning on `latest`, so we can get the usual advantages of version pinning in our Gitlab CI jobs. In particular: caching of build images is more reliable with version numbers.

```
docker build -f cloud-run-build.Dockerfile -t registry.gitlab.com/dangerfarms/perks-and-recreation/cloud-run-build:<tag> .
docker push registry.gitlab.com/dangerfarms/perks-and-recreation/cloud-run-build:<tag>
```

You'll have to authenticate with Gitlab Container Registry the first time you push. Just try and push anyway, and the CLI will give you the right instructions to login.
