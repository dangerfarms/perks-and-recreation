FROM docker:19
RUN apk update && \
    apk add bash curl py-pip python-dev libffi-dev openssl-dev gcc libc-dev make && \
    pip install docker-compose==1.25.0 && \
    curl -sSL https://sdk.cloud.google.com | bash
ENV PATH="$PATH:/root/google-cloud-sdk/bin"
