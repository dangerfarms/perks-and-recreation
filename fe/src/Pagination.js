import { Component } from "react";
import { PropTypes } from "prop-types";

export default class Pagination extends Component {
  componentDidMount() {
    window.addEventListener("scroll", this.onScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.onScroll);
  }

  onScroll = () => {
    const scrollTop =
      (document.documentElement && document.documentElement.scrollTop) ||
      document.body.scrollTop;
    const scrollHeight =
      (document.documentElement && document.documentElement.scrollHeight) ||
      document.body.scrollHeight;
    const clientHeight =
      document.documentElement.clientHeight || window.innerHeight;
    const scrolledToBottom =
      Math.ceil(scrollTop + clientHeight) >= scrollHeight;
    if (scrolledToBottom) {
      this.props.onScrolledToEnd();
    }
  };

  render() {
    return this.props.children;
  }
}

Pagination.propTypes = {
  children: PropTypes.node.isRequired,
  onScrolledToEnd: PropTypes.func.isRequired,
};
