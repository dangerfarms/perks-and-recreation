import { MockedProvider } from "@apollo/client/testing";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

it("renders without crashing", () => {
  const div = document.createElement("div");
  const mockedApp = (
    <MockedProvider addTypename={false} mocks={[]} resolvers={{}}>
      <App />
    </MockedProvider>
  );
  ReactDOM.render(mockedApp, div);
  ReactDOM.unmountComponentAtNode(div);
});
