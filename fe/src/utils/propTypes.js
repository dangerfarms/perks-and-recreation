import PropTypes from "prop-types";

export const budgetShape = {
  remainingBudget: PropTypes.string.isRequired,
  totalBudget: PropTypes.string.isRequired,
};

export const filterShape = {
  displayName: PropTypes.string.isRequired,
  query: PropTypes.shape().isRequired,
  variables: PropTypes.shape(),
};

export const sortShape = {
  displayName: PropTypes.string.isRequired,
  query: PropTypes.shape().isRequired,
  variables: PropTypes.shape(),
};

export const proposalShape = {
  createdAt: PropTypes.string.isRequired,
  estimatedCost: PropTypes.number.isRequired,
  description: PropTypes.string,
  name: PropTypes.string.isRequired,
  owner: PropTypes.string,
  id: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  threshold: PropTypes.number,
  voteCount: PropTypes.number,
  votes: PropTypes.arrayOf(
    PropTypes.shape({
      accept: PropTypes.bool,
      user: PropTypes.string,
    }),
  ),
};
