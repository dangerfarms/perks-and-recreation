import client from "./graphql/client";

const AUTO_DISMISS_TIME_MS = 6000;

/**
 * Hide the flash message
 */
export const hideFlashMessage = () =>
  client.writeData({
    data: {
      flashMessageState: {
        isShown: false,
        message: null,
        type: null,
        __typename: "FlashMessageState",
      },
    },
  });

/**
 * Show a flash message
 * @param {"error"|"loading"|"success"} type The type of message to show
 * @param {string} message The content of the message
 */
export const showFlashMessage = (type, message) => {
  client.writeData({
    data: {
      flashMessageState: {
        isShown: true,
        message,
        type,
        __typename: "FlashMessageState",
      },
    },
  });

  // Success messages are automatically dismissed
  if (type === "success") {
    setTimeout(() => {
      hideFlashMessage();
    }, AUTO_DISMISS_TIME_MS);
  }
};
