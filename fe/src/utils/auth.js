import client from "./graphql/client";
import { AUTH_STATUSES } from "./constants";

export const logout = async () => {
  await client.cache.reset();
  client.writeData({
    data: { authStatus: AUTH_STATUSES.SIGNED_OUT },
  });
};
