import React from "react";

export const backgroundDecorator = story => (
  <div
    style={{
      background: "linear-gradient(30deg, #f5a34c, #ef7e53)",
      height: "100vh",
    }}
  >
    {story()}
  </div>
);
