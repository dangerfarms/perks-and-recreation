import { allDataWithPaginatedProposalsQuery } from "../graphql/queries";

export const AUTH_STATUSES = {
  COOKIES_DISABLED: "COOKIES_DISABLED",
  PENDING: "PENDING",
  SIGN_IN_FAILED: "SIGN_IN_FAILED",
  SIGNED_IN: "SIGNED_IN",
  SIGNED_OUT: "SIGNED_OUT",
  SIGNING_IN: "SIGNING_IN",
};

export const DEFAULT_FILTER = {
  displayName: "All",
  query: allDataWithPaginatedProposalsQuery,
  variables: { filter: "all" },
};

export const DEFAULT_SORT = {
  displayName: "Newest",
  query: allDataWithPaginatedProposalsQuery,
  variables: { sort: "DATE_DESCENDING" },
};

export const FILTERS = [
  {
    displayName: "Needs my vote",
    query: allDataWithPaginatedProposalsQuery,
    variables: { filter: "needs_my_vote" },
  },
  {
    displayName: "Mine",
    query: allDataWithPaginatedProposalsQuery,
    variables: { filter: "mine" },
  },
  {
    displayName: "Approved",
    query: allDataWithPaginatedProposalsQuery,
    variables: { filter: "approved" },
  },
  {
    displayName: "Purchased",
    query: allDataWithPaginatedProposalsQuery,
    variables: { filter: "purchased" },
  },
  DEFAULT_FILTER,
];

export const SORTS = [
  {
    displayName: "Oldest",
    query: allDataWithPaginatedProposalsQuery,
    variables: { sort: "DATE_ASCENDING" },
  },
  DEFAULT_SORT,
  {
    displayName: "Most Popular",
    query: allDataWithPaginatedProposalsQuery,
    variables: { sort: "POPULARITY_DESCENDING" },
  },
  {
    displayName: "Least Popular",
    query: allDataWithPaginatedProposalsQuery,
    variables: { sort: "POPULARITY_ASCENDING" },
  },
];
