import colors from "./colors.module.css";

export const orangeGradient = {
  angle: -50,
  startColor: colors.orangeGradientLight,
  endColor: colors.orangeGradientDark,
};
