/**
 * Taken from https://tylermcginnis.com/validate-email-address-javascript/
 *
 * This doesn't quite follow the email spec exactly, but the email spec is weird and following
 * it exactly would be complicated or require depending on a library.
 *
 * We could switch to using a library if we need it, but it's simplest to just be a bit permissive.
 *
 * @param email
 * @returns {boolean}
 */
export const emailIsValid = email => /\S+@\S+\.\S+/.test(email);

export const minimumPasswordLength = 8;

export const passwordIsValid = password =>
  password.length >= minimumPasswordLength;

export const displayFormError = (validationError, formError) => {
  if (validationError) {
    return validationError;
  } else if (formError) {
    const errorMessage = formError.toString();
    if (errorMessage.toLowerCase().includes("network error")) {
      return "Unexpected network error";
    } else if (errorMessage.includes("error: ")) {
      return errorMessage.split("error: ")[1];
    } else {
      return errorMessage;
    }
  }

  return null;
};
