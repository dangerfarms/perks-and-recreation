import { gql } from "@apollo/client";
import { AUTH_STATUSES } from "../../constants";
import { putRefreshTokenInStorage } from "../../refreshToken";
import client from "../client";
import { GetAuthStatus, GetRefreshToken } from "../localQueries";

const unauthenticatedError = {
  message: "You do not have permission to perform this action",
  locations: [
    {
      line: 2,
      column: 3,
    },
  ],
  path: ["me"],
};

const unrelatedError = {
  message: "Something went wrong",
  locations: [
    {
      line: 2,
      column: 3,
    },
  ],
  path: ["me"],
};

const mockSuccessResponse = {
  data: {
    me: {
      id: "mockId",
      __typename: "ProfileType",
    },
  },
  errors: null,
};

const mockUnrelatedErrorResponse = {
  data: null,
  errors: [unrelatedError],
};

const mockUnauthenticatedResponse = {
  data: null,
  errors: [unauthenticatedError],
};

const mockRefreshResponseResponse = {
  data: {
    refreshToken: {
      refreshToken: "mockNewRefreshToken",
      payload: null,
      __typename: "Refresh",
    },
  },
  errors: null,
};

const mockQuery = gql`
  {
    me {
      id
    }
  }
`;

describe("Custom fetch", () => {
  beforeEach(() => {
    client.cache.reset();

    fetch.resetMocks();

    localStorage.clear();
    localStorage.getItem.mockClear();
    localStorage.setItem.mockClear();
  });

  it("Should do nothing if there is no error", async () => {
    fetch.mockResponseOnce(JSON.stringify(mockSuccessResponse));

    const response = await client.query({
      query: mockQuery,
      fetchPolicy: "network-only",
    });

    expect(response.data).toEqual(mockSuccessResponse.data);
    expect(fetch).toHaveBeenCalledTimes(1);
  });

  it("Should do nothing if there is a non auth-related error", async () => {
    fetch.mockResponseOnce(JSON.stringify(mockUnrelatedErrorResponse));

    try {
      await client.query({
        query: mockQuery,
        fetchPolicy: "network-only",
      });
    } catch (e) {
      expect(e.message).toEqual(`GraphQL error: ${unrelatedError.message}`);
    }

    expect(fetch).toHaveBeenCalledTimes(1);
  });

  it("Should use the refresh token from local storage if not in the cache", async () => {
    putRefreshTokenInStorage("testTokenInStorage");

    fetch
      .once(JSON.stringify(mockUnauthenticatedResponse))
      .once(JSON.stringify(mockRefreshResponseResponse))
      .once(JSON.stringify(mockSuccessResponse));

    const response = await client.query({
      query: mockQuery,
      fetchPolicy: "network-only",
    });

    expect(fetch).toHaveBeenCalledTimes(3);
    expect(response.data).toEqual(mockSuccessResponse.data);

    // Check that the refresh token was updated
    const { refreshToken } = client.readQuery({ query: GetRefreshToken });
    expect(refreshToken).toEqual("mockNewRefreshToken");
  });

  it("Should log the user out if we don't have a refresh token", async () => {
    fetch.mockResponseOnce(JSON.stringify(mockUnauthenticatedResponse));

    try {
      await client.query({
        query: mockQuery,
        fetchPolicy: "network-only",
      });
    } catch (e) {
      expect(e.message).toEqual(
        `GraphQL error: ${unauthenticatedError.message}`,
      );
    }

    expect(fetch).toHaveBeenCalledTimes(1);

    const { authStatus } = client.readQuery({ query: GetAuthStatus });
    expect(authStatus).toEqual(AUTH_STATUSES.SIGNED_OUT);
  });

  it("Should refresh the token and try again on auth error", async () => {
    client.writeData({ data: { refreshToken: "mockRefreshToken" } });

    fetch
      .once(JSON.stringify(mockUnauthenticatedResponse))
      .once(JSON.stringify(mockRefreshResponseResponse))
      .once(JSON.stringify(mockSuccessResponse));

    const response = await client.query({
      query: mockQuery,
      fetchPolicy: "network-only",
    });

    expect(fetch).toHaveBeenCalledTimes(3);
    expect(response.data).toEqual(mockSuccessResponse.data);

    // Check that the refresh token was updated
    const { refreshToken } = client.readQuery({ query: GetRefreshToken });
    expect(refreshToken).toEqual("mockNewRefreshToken");
  });

  it("Should log the user out if the token refresh fails", async () => {
    client.writeData({ data: { refreshToken: "mockRefreshToken" } });

    fetch
      .once(JSON.stringify(mockUnauthenticatedResponse))
      .once(JSON.stringify(mockUnrelatedErrorResponse));

    try {
      await client.query({ query: mockQuery, fetchPolicy: "network-only" });
    } catch (e) {
      expect(e.message).toEqual(
        `GraphQL error: ${unauthenticatedError.message}`,
      );
    }

    expect(fetch).toHaveBeenCalledTimes(2);

    const { authStatus } = client.readQuery({ query: GetAuthStatus });
    expect(authStatus).toEqual(AUTH_STATUSES.SIGNED_OUT);
  });
});
