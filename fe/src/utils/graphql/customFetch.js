import {
  getRefreshTokenFromStorage,
  putRefreshTokenInStorage,
} from "../refreshToken";
import { logout } from "../auth";
import { refreshTokenMutation } from "./authMutations";
import client from "./client";
import { GetRefreshToken } from "./localQueries";

const UNAUTHENTICATED_ERROR_MESSAGE =
  "You do not have permission to perform this action";

let isRefreshingToken = false;

/**
 * A wrapper around `fetch` that attempts to refresh an expired auth token
 * @param {RequestInfo} input
 * @param {RequestInit} init
 */
export const customFetch = async (input, init) => {
  const initialResponse = await fetch(input, init);
  const initialResponseText = await initialResponse.text();
  const initialResponseJson = JSON.parse(initialResponseText);

  // We need to repackage the Response since you can only call `.json` once. We're about to
  // call it here and the caller of this function will need to be able to call it too.
  const initialResponseRepackaged = {
    ok: true,
    json: () => Promise.resolve(initialResponseJson),
    text: () => Promise.resolve(initialResponseText),
  };

  if (
    initialResponseJson &&
    initialResponseJson.errors &&
    initialResponseJson.errors[0] &&
    initialResponseJson.errors[0].message === UNAUTHENTICATED_ERROR_MESSAGE
  ) {
    if (isRefreshingToken) {
      isRefreshingToken = false;
      return initialResponseRepackaged;
    }

    // Try to get the refresh token we added to the cache the last time we authenticated.
    // `readQuery` will error if we don't have a token
    let getRefreshTokenResponse = null;
    try {
      getRefreshTokenResponse = client.readQuery({
        query: GetRefreshToken,
      });
    } catch (e) {
      // Nothing to do here - we may have the refresh token in local storage
    }

    // If we couldn't get the refresh token from the cache, try to get it from local storage
    const refreshToken =
      getRefreshTokenResponse && getRefreshTokenResponse.refreshToken
        ? getRefreshTokenResponse.refreshToken
        : getRefreshTokenFromStorage();

    // Sign the user out if we don't have a refresh token
    if (!refreshToken) {
      await logout();
      return initialResponseRepackaged;
    }

    // Refresh the auth token
    try {
      isRefreshingToken = true;
      const refreshTokenResponse = await client.mutate({
        mutation: refreshTokenMutation,
        variables: { refreshToken },
      });
      isRefreshingToken = false;

      client.writeData({
        data: {
          refreshToken: refreshTokenResponse.data.refreshToken.refreshToken,
        },
      });
      putRefreshTokenInStorage(
        refreshTokenResponse.data.refreshToken.refreshToken,
      );

      // Make the initial request again now that we have the new auth token
      return fetch(input, init);
    } catch (e) {
      await logout();
      isRefreshingToken = false;
      return initialResponseRepackaged;
    }
  }

  return initialResponseRepackaged;
};
