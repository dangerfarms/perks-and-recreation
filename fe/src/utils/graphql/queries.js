import { gql } from "@apollo/client";

export const proposalFragment = gql`
  fragment ProposalFragment on Proposal {
    createdAt
    estimatedCost
    description
    name
    owner
    id
    isDeleted
    isRecurring
    status
    threshold
    voteCount
    votes {
      user
      accept
    }
  }
`;

export const allDataWithPaginatedProposalsQuery = gql`
  query allDataWithPaginatedProposals(
    $filter: String
    $sort: SortOptions
    $year: String
    $cursor: Int
  ) {
    budget(year: $year) {
      remainingBudget
      totalBudget
    }
    threshold {
      threshold
    }
    users {
      email
      isAdmin
    }
    paginatedProposals(filter: $filter, cursor: $cursor, sort: $sort) {
      proposals {
        ...ProposalFragment
      }
      cursor {
        current
        final
      }
    }
  }
  ${proposalFragment}
`;

export const budgetAndThresholdQuery = gql`
  query GetBudgetAndThreshold($year: String) {
    budget(year: $year) {
      remainingBudget
      totalBudget
    }
    threshold {
      threshold
    }
  }
`;

export const adminSettingsMutation = gql`
  mutation ChangeAdminSettings($totalBudget: Int!, $threshold: Int!) {
    changeAdminSettings(totalBudget: $totalBudget, threshold: $threshold) {
      budget {
        totalBudget
        remainingBudget
      }
      threshold {
        threshold
      }
    }
  }
`;

export const purchaseProposalMutation = gql`
  mutation PurchaseProposal($cost: Int!, $proposalId: String!) {
    purchaseProposal(cost: $cost, proposalId: $proposalId) {
      proposal {
        ...ProposalFragment
      }
      budget {
        remainingBudget
        totalBudget
      }
    }
  }
  ${proposalFragment}
`;

export const deleteMutation = gql`
  mutation DeleteProposal($proposal: String!) {
    deleteProposal(proposal: $proposal) {
      ok
    }
  }
`;

export const createProposalMutation = gql`
  mutation CreateProposal(
    $name: String!
    $estimatedCost: Int!
    $description: String
    $isRecurring: Boolean
  ) {
    createProposal(
      name: $name
      estimatedCost: $estimatedCost
      description: $description
      isRecurring: $isRecurring
    ) {
      proposal {
        ...ProposalFragment
      }
      ok
    }
  }
  ${proposalFragment}
`;

export const editProposalMutation = gql`
  mutation EditProposal(
    $proposal: String!
    $name: String!
    $estimatedCost: Int!
    $description: String
  ) {
    editProposal(
      proposal: $proposal
      name: $name
      estimatedCost: $estimatedCost
      description: $description
    ) {
      proposal {
        ...ProposalFragment
      }
      ok
    }
  }
  ${proposalFragment}
`;

export const companyFragment = gql`
  fragment CompanyFragment on CompanyType {
    name
    admin {
      firstName
      lastName
      email
    }
  }
`;

export const companyQuery = gql`
  query GetCompany($id: ID!) {
    company(id: $id) {
      ...CompanyFragment
    }
  }
  ${companyFragment}
`;

export const allCompaniesQuery = gql`
  query GetCompanies {
    companies {
      ...CompanyFragment
    }
  }
  ${companyFragment}
`;
