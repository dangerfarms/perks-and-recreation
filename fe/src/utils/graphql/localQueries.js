import { gql } from "@apollo/client";

export const GetAuthStatus = gql`
  {
    authStatus @client
  }
`;

export const GetRefreshToken = gql`
  {
    refreshToken @client
  }
`;
