import { ApolloClient, HttpLink, InMemoryCache, gql } from "@apollo/client";
import { customFetch } from "./customFetch";

/** Type definitions for the app's local state */
const typeDefs = gql`
  type FlashMessageState {
    """
    (client-only) True if the flash message should be shown
    """
    isShown: Boolean
    """
    (client-only) The content of the flash message
    """
    message: String
    """
    (client-only) The type of the flash message
    """
    type: String
  }

  extend type Query {
    """
    (client-only) The current user's auth status
    """
    authStatus: String
    """
    (client-only) The state of the flash message
    """
    flashMessageState: FlashMessageState
    """
    (client-only) The current auth token
    """
    refreshToken: String
  }
`;

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    uri: process.env.REACT_APP_GRAPHQL_ENDPOINT,
    credentials: "include",
    fetch: customFetch,
  }),
  typeDefs,
  // Required for @client queries to work
  resolvers: {},
});
export default client;
