import { gql } from "@apollo/client";

export const tokenAuthMutation = gql`
  mutation TokenAuth($username: String!, $password: String!) {
    tokenAuth(username: $username, password: $password) {
      refreshToken
    }
  }
`;

export const verifyTokenMutation = gql`
  mutation VerifyToken($token: String!) {
    verifyToken(token: $token) {
      payload
    }
  }
`;

export const refreshTokenMutation = gql`
  mutation RefreshToken($refreshToken: String!) {
    refreshToken(refreshToken: $refreshToken) {
      payload
      refreshToken
    }
  }
`;

// Note that the operation name, "Logout", is important - the API uses it to determine that the
// JWT cookie should be deleted
export const logOutMutation = gql`
  mutation Logout($refreshToken: String!) {
    logOut(refreshToken: $refreshToken) {
      message
    }
  }
`;

export const createCompanyMutation = gql`
  mutation CreateCompany($admin: UserInput!, $name: String!) {
    createCompany(admin: $admin, name: $name) {
      company {
        name
        admin {
          firstName
          lastName
          email
        }
      }
    }
  }
`;
