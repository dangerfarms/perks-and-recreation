import { useMutation } from "@apollo/react-hooks";
import {
  allDataWithPaginatedProposalsQuery,
  purchaseProposalMutation,
} from "../graphql/queries";

export const usePurchaseProposal = () => {
  const [purchaseProposal] = useMutation(purchaseProposalMutation, {
    update(
      cache,
      {
        data: {
          purchaseProposal: { proposal, budget },
        },
      },
    ) {
      const beforeCache = cache.readQuery({
        query: allDataWithPaginatedProposalsQuery,
        variables: { filter: "all", sort: "DATE_DESCENDING" },
      });

      const previousPaginatedProposals = beforeCache.paginatedProposals;
      const updatedProposals = previousPaginatedProposals.proposals.map(
        cachedProposal =>
          cachedProposal.id === proposal.id ? proposal : cachedProposal,
      );
      const newPaginatedProposals = {
        ...previousPaginatedProposals,
        updatedProposals,
      };

      cache.writeQuery({
        query: allDataWithPaginatedProposalsQuery,
        data: {
          ...beforeCache,
          paginatedProposals: newPaginatedProposals,
          budget,
        },
        variables: { filter: "all", sort: "DATE_DESCENDING" },
      });
    },
  });
  return purchaseProposal;
};
