import { useMutation, useQuery } from "@apollo/client";
import {
  logOutMutation,
  refreshTokenMutation,
  tokenAuthMutation,
} from "../graphql/authMutations";
import { GetRefreshToken } from "../graphql/localQueries";
import {
  getRefreshTokenFromStorage,
  putRefreshTokenInStorage,
} from "../refreshToken";
import { logout } from "../auth";

// Check for the refresh token in local storage when the app loads
const refreshTokenFromInitialStorageCheck = getRefreshTokenFromStorage();

/** A hook for accessing auth-related gql mutations */
export const useAuthMutation = () => {
  const { data, client } = useQuery(GetRefreshToken);
  const [callTokenAuthMutation] = useMutation(tokenAuthMutation);
  const [callRefreshMutation] = useMutation(refreshTokenMutation);
  const [callLogoutMutation] = useMutation(logOutMutation);

  const refreshToken = data
    ? data.refreshToken
    : refreshTokenFromInitialStorageCheck;

  /**
   * Update the apollo cache and update the refresh token in local storage if necessary
   */
  const updateCache = updates => {
    client.writeData({ data: updates });

    if ("refreshToken" in updates)
      putRefreshTokenInStorage(updates.refreshToken);
  };

  /**
   * Authenticate a user with email and password. Sets the `refreshToken` to be used in calls
   * to `refreshAuthToken`.
   * @param {string} email
   * @param {string} password
   */
  const authenticate = async (email, password) => {
    const response = await callTokenAuthMutation({
      variables: { username: email, password },
    });

    updateCache({ refreshToken: response.data.tokenAuth.refreshToken });

    return response;
  };

  /**
   * Refresh the auth token using the `refreshToken` from the previous auth call and add
   * the new refresh token in the response to the cache and local storage
   */
  const refreshAuthToken = async () => {
    if (refreshToken === null) return undefined;

    const response = await callRefreshMutation({
      variables: { refreshToken },
    });

    updateCache({ refreshToken: response.data.refreshToken.refreshToken });

    return response;
  };

  /**
   * Call the mutation to remove the JWT cookie and set the auth status to `SIGNED_OUT`
   */
  const logOut = async () => {
    const response = await callLogoutMutation({
      variables: { refreshToken },
    });
    await logout();
    return response;
  };

  return { authenticate, logOut, refreshAuthToken };
};
