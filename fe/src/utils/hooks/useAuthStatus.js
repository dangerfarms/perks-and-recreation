import { useQuery } from "@apollo/client";
import { AUTH_STATUSES } from "../constants";
import { GetAuthStatus } from "../graphql/localQueries";

/** A hook to get and set the current user's auth status */
export const useAuthStatus = () => {
  const { data, client } = useQuery(GetAuthStatus);

  /**
   * The current user's auth status
   * @type {string}
   */
  const authStatus = data ? data.authStatus : AUTH_STATUSES.PENDING;

  /**
   * Set the current user's auth status
   * @param {string} status The status to set. See `AUTH_STATUSES` in `utils/constants`
   */
  const setAuthStatus = status =>
    client.writeData({ data: { authStatus: status } });

  return { authStatus, setAuthStatus };
};
