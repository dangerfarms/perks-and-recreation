import { useMutation } from "@apollo/react-hooks";
import {
  allDataWithPaginatedProposalsQuery,
  deleteMutation,
} from "../graphql/queries";

export const useDeleteProposal = proposalToDelete =>
  useMutation(deleteMutation, {
    update: (cache, { data }) => {
      // Don't update the cache if the operation failed
      if (!data.deleteProposal.ok) {
        return;
      }

      const beforeCache = cache.readQuery({
        query: allDataWithPaginatedProposalsQuery,
        variables: { filter: "all", sort: "DATE_DESCENDING" },
      });

      const previousPaginatedProposals = beforeCache.paginatedProposals;
      const indexToDelete = previousPaginatedProposals.proposals.findIndex(
        proposal => proposal.id === proposalToDelete,
      );

      const newPaginatedProposals = {
        ...previousPaginatedProposals,
        cursor: {
          ...previousPaginatedProposals.cursor,
          current: previousPaginatedProposals.cursor.current - 1,
          final: previousPaginatedProposals.cursor.final - 1,
        },
        proposals: [
          ...previousPaginatedProposals.proposals.slice(0, indexToDelete),
          ...previousPaginatedProposals.proposals.slice(indexToDelete + 1),
        ],
      };

      cache.writeQuery({
        query: allDataWithPaginatedProposalsQuery,
        data: {
          ...beforeCache,
          paginatedProposals: newPaginatedProposals,
        },
        variables: { filter: "all", sort: "DATE_DESCENDING" },
      });
    },
  });
