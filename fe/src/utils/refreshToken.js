const LOCAL_STORAGE_KEY = "refreshToken";

/**
 * Get the refresh token from local storage
 */
export const getRefreshTokenFromStorage = () =>
  localStorage.getItem(LOCAL_STORAGE_KEY);

/**
 * Put the refresh token in local storage. Pass `null` to remove the token
 * @param {string} token
 */
export const putRefreshTokenInStorage = token =>
  token === null
    ? localStorage.removeItem(LOCAL_STORAGE_KEY)
    : localStorage.setItem(LOCAL_STORAGE_KEY, token);
