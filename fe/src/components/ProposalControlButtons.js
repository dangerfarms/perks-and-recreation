import React from "react";
import PropTypes from "prop-types";
// import classnames from "classnames";
import { proposalShape } from "../utils/propTypes";
import Button from "./common/Button";
// import styles from "./DeleteProposalModal.module.css";

const ProposalControlButtons = ({
  isAdmin,
  isOwner,
  onEditProposal,
  currentProposal,
  remainingBudget,
  onPurchase,
  showDeleteProposalModal,
  // onDeleteError,
}) => {
  const PurchaseButton =
    isAdmin && remainingBudget && currentProposal.status === "APPROVED" ? (
      <Button onClick={() => onPurchase(currentProposal)}>Purchase</Button>
    ) : null;

  const editButton =
    isOwner &&
    currentProposal.status === "OPEN" &&
    currentProposal.voteCount === 0 ? (
      <Button
        dataRef="editProposalButton"
        onClick={event => onEditProposal(event, currentProposal)}
      >
        Edit
      </Button>
    ) : null;

  const deleteButton =
    isOwner && currentProposal.status === "OPEN" ? (
      <>
        {/* {onDeleteError && (
          <div
            className={classnames(styles.errorMessage, {
              [styles.isShown]: true,
            })}
            data-ref="deleteProposalErrorMessage"
          >
            {"Sorry, your proposal couldn't be deleted"}
          </div>
        )} */}
        <Button
          dataRef="deleteProposalButton"
          onClick={() => showDeleteProposalModal(currentProposal.id)}
        >
          Delete
        </Button>
      </>
    ) : null;

  return (
    <>
      {PurchaseButton}
      {editButton}
      {deleteButton}
    </>
  );
};

ProposalControlButtons.propTypes = {
  currentProposal: PropTypes.shape(proposalShape).isRequired,
  isAdmin: PropTypes.bool.isRequired,
  isOwner: PropTypes.bool.isRequired,
  // onDeleteError: PropTypes.string.isRequired,
  onEditProposal: PropTypes.func.isRequired,
  onPurchase: PropTypes.func.isRequired,
  remainingBudget: PropTypes.number,
  showDeleteProposalModal: PropTypes.func.isRequired,
};

ProposalControlButtons.defaultProps = {
  remainingBudget: null,
};

export default ProposalControlButtons;
