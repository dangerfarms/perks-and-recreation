import { gql } from "@apollo/client";

export const dashboardQuery = gql`
  query dashboard {
    me {
      company {
        name
        budget
      }
      user {
        email
        firstName
        lastName
      }
    }
  }
`;
