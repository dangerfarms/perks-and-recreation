import { useQuery } from "@apollo/client";
import React, { useState } from "react";
import { FaCog } from "react-icons/fa";
import { hideFlashMessage, showFlashMessage } from "../../utils/flashMessage";
import { useAuthMutation } from "../../utils/hooks/useAuthMutation";
import Budget from "../Budget";
import Button from "../common/Button";
import GradientBackground from "../common/GradientBackground";
import ConnectedSetBudgetModal from "../SetBudgetModal/ConnectedSetBudgetModal";
import { dashboardQuery } from "./queries";
import styles from "./Dashboard.module.css";

const ConnectedDashboard = () => {
  const { loading, data, refetch } = useQuery(dashboardQuery, {
    onError: error => showFlashMessage("error", error.message),
  });
  const { logOut } = useAuthMutation();
  const [showSetBudgetModal, setShowSetBudgetModal] = useState(false);

  const onLogOut = () => {
    hideFlashMessage();
    logOut();
  };

  const toggleSetBudgetModal = () => {
    hideFlashMessage();
    setShowSetBudgetModal(!showSetBudgetModal);
  };

  const handleBudgetChange = () => {
    toggleSetBudgetModal();
    showFlashMessage("success", "Budget updated successfully");
    // This use of refetch is temporary, to show the budget mutation has worked,
    // pending implementation of cache updating
    refetch();
  };

  // Loading status will also be displayed via displayMessage once other
  // issues have been resolved
  if (loading) {
    return (
      <>
        <GradientBackground />
        <p>Loading . . .</p>
      </>
    );
  }

  return (
    <div className={styles.container}>
      <GradientBackground />

      {data && (
        <div className={styles.sidebar}>
          <span className={styles.companyName}>{data.me.company.name}</span>

          <div className={styles.budget}>
            <Budget budget={data.me.company.budget}>
              {showSetBudgetModal && (
                <ConnectedSetBudgetModal
                  companyId={data.me.company.id}
                  currentBudget={data.me.company.budget.totalBudget}
                  onClickCancel={toggleSetBudgetModal}
                  onSuccess={handleBudgetChange}
                />
              )}
            </Budget>
            <Button
              className={styles.setBudgetButton}
              onClick={toggleSetBudgetModal}
            >
              <FaCog size="26px" style={{ opacity: 1 }} />
            </Button>
          </div>

          <div className={styles.flexSpacer} />

          <span className={styles.userName}>
            {`${data.me.user.firstName} ${data.me.user.lastName}`}
          </span>
          <Button className={styles.logOutButton} onClick={onLogOut}>
            Log out
          </Button>
        </div>
      )}
    </div>
  );
};

export default ConnectedDashboard;
