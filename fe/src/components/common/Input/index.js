import React from "react";
import PropTypes from "prop-types";
import styles from "./Input.module.css";

const Input = ({ className, ...props }) => {
  const combinedClassName = className
    ? `${styles.input} ${className}`
    : styles.input;

  return <input className={combinedClassName} {...props} />;
};

Input.propTypes = {
  className: PropTypes.string,
};

Input.defaultProps = {
  className: null,
};

export default Input;
