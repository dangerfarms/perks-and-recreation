import React from "react";
import styles from "./styles.module.css";

const GradientBackground = () => <div className={styles.background} />;

export default GradientBackground;
