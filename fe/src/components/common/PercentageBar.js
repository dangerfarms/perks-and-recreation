import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import styles from "./PercentageBar.module.css";

const PercentageBar = ({ className, remaining, total }) => {
  const remainingPercentage = Math.round((remaining / total) * 100);
  return (
    <div className={classnames(styles.container, className)}>
      <div
        className={styles.bar}
        style={{ width: `${remainingPercentage}%` }}
      />
    </div>
  );
};

PercentageBar.propTypes = {
  className: PropTypes.string,
  remaining: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
};

PercentageBar.defaultProps = {
  className: null,
};

export default PercentageBar;
