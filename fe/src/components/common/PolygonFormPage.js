import { Polygon } from "@dangerfarms/ui/lib/core";
import PropTypes from "prop-types";
import React from "react";
import { orangeGradient } from "../../utils/gradients";
import styles from "./PolygonFormPage.module.css";

const PolygonFormPage = ({ children, onSubmit, polygonSize }) => {
  const handleSubmit = event => {
    event.preventDefault(); // Doing this early in the chain is DRYer
    onSubmit();
  };

  return (
    <div className={styles.page}>
      <Polygon
        className={styles.polygon}
        fill={orangeGradient}
        sides={10}
        size={polygonSize}
      />
      <form className={styles.form} onSubmit={handleSubmit}>
        {children}
      </form>
    </div>
  );
};

PolygonFormPage.propTypes = {
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired,
  polygonSize: PropTypes.number,
};

PolygonFormPage.defaultProps = {
  polygonSize: 600,
};

export default PolygonFormPage;
