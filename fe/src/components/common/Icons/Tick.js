import React from "react";

const Tick = () => (
  <svg height="17px" viewBox="0 0 21 17" width="21px">
    <g fill="none" fillRule="evenodd" id="Final" stroke="none" strokeWidth="1">
      <g
        id="Filled-recurring-proposal"
        transform="translate(-618.000000, -537.000000)"
      >
        <g
          fill="#FFFFFF"
          id="Tick"
          transform="translate(618.000000, 537.000000)"
        >
          <rect
            height="3"
            id="Rectangle"
            transform="translate(4.500000, 11.622836) rotate(43.000000) translate(-4.500000, -11.622836) "
            width="7"
            x="1"
            y="10.1228362"
          />
          <rect
            height="3"
            id="Rectangle"
            transform="translate(12.881402, 8.500000) rotate(-47.000000) translate(-12.881402, -8.500000) "
            width="19"
            x="3.38140169"
            y="7"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default Tick;
