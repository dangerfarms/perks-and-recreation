import React from "react";
import { storiesOf } from "@storybook/react";
import Plus from "./Plus";
import Tick from "./Tick";

storiesOf("Icons", module)
  .add("Plus", () => <Plus />)
  .add("Tick", () => <Tick />);
