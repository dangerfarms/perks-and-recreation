import React from "react";

function Plus() {
  return (
    <svg height="9px" viewBox="0 0 9 9" width="9px">
      <g
        fill="none"
        fillRule="evenodd"
        id="Symbols"
        stroke="none"
        strokeWidth="1"
      >
        <g
          fill="#39575D"
          fillRule="nonzero"
          id="Sidebar"
          transform="translate(-16.000000, -174.000000)"
        >
          <g id="New-proposal">
            <g transform="translate(0.000000, 163.000000)">
              <g id="Group" transform="translate(16.000000, 7.000000)">
                <polygon
                  id="Path"
                  points="9 9.14285714 5.14285714 9.14285714 5.14285714 13 3.85714286 13 3.85714286 9.14285714 0 9.14285714 0 7.85714286 3.85714286 7.85714286 3.85714286 4 5.14285714 4 5.14285714 7.85714286 9 7.85714286"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
}

export default Plus;
