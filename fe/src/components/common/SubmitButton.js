import React from "react";
import PropTypes from "prop-types";
import Button from "./Button";

export default function SubmitButton({ children, form, ...props }) {
  return (
    <Button form={form} type="submit" {...props}>
      {children}
    </Button>
  );
}

SubmitButton.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  dataRef: PropTypes.string,
  disabled: PropTypes.bool,
  form: PropTypes.string,
};

SubmitButton.defaultProps = {
  className: null,
  dataRef: null,
  disabled: false,
  form: null,
};
