import React from "react";
import PropTypes from "prop-types";
import ReactDatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Input from "../Input";
import styles from "./DatePicker.module.css";

const DatePicker = ({ onChange, value, ...props }) => (
  <ReactDatePicker
    calendarClassName={styles.calendar}
    customInput={<Input />}
    dateFormat="MMMM d, yyyy"
    onChange={onChange}
    selected={value}
    {...props}
  />
);

DatePicker.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.shape(),
};

DatePicker.defaultProps = {
  onChange: () => {},
  value: null,
};

export default DatePicker;
