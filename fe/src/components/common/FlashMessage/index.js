import classnames from "classnames";
import { gql, useQuery } from "@apollo/client";
import React from "react";
import { FaWindowClose } from "react-icons/fa";
import { hideFlashMessage } from "../../../utils/flashMessage";
import Button from "../Button";
import styles from "./styles.module.css";

const GetFlashMessageState = gql`
  {
    flashMessageState @client {
      isShown
      message
      type
    }
  }
`;

const FlashMessage = () => {
  const { data } = useQuery(GetFlashMessageState);

  const { isShown = false, message = null, type = null } = data
    ? data.flashMessageState
    : {};

  return (
    <div
      className={classnames(
        styles.message,
        isShown && styles[`${type}Message`],
        {
          [styles.isShown]: isShown,
        },
      )}
    >
      <span className={styles.messageText}>{isShown && message}</span>
      <Button
        className={styles.messageCloseButton}
        onClick={() => hideFlashMessage()}
      >
        <FaWindowClose fill="#fff" />
      </Button>
    </div>
  );
};

export default FlashMessage;
