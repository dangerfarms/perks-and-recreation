import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import StoryRouter from "storybook-react-router";
import Button from "./Button";

storiesOf("Button", module)
  .add("a button!", () => (
    <Button onClick={action("clicked")}>Hello Button</Button>
  ))
  .add("a few buttons!", () => (
    <React.Fragment>
      <Button onClick={action("clicked")}>Button 1</Button>
      <Button onClick={action("clicked")}>Button 2</Button>
      <Button onClick={action("clicked")}>Button 3</Button>
    </React.Fragment>
  ));

storiesOf("Button", module)
  .addDecorator(StoryRouter())
  .add("a button behaving as a link", () => (
    <Button linkTo="/example">Hello Button</Button>
  ));
