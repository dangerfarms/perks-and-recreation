import { Spinner } from "@dangerfarms/ui";
import PropTypes from "prop-types";
import React from "react";
import colors from "../../../utils/colors.module.css";
import styles from "./styles.module.css";

const PerksSpinner = ({ shouldAnimateOut, animateOutCallback }) => {
  const spinnerProps = {
    fill: {
      angle: 30,
      endColor: colors.orangeGradientDark,
      startColor: colors.orangeGradientLight,
    },
    scale: shouldAnimateOut
      ? {
          from: 1,
          to: 100,
          duration: 1500,
          callback: animateOutCallback,
        }
      : undefined,
    size: 100,
  };

  return (
    <div className={styles.spinnerContainer}>
      <Spinner sides={10} {...spinnerProps} />
    </div>
  );
};

PerksSpinner.propTypes = {
  animateOutCallback: PropTypes.func,
  shouldAnimateOut: PropTypes.bool,
};

PerksSpinner.defaultProps = {
  animateOutCallback: () => {},
  shouldAnimateOut: false,
};

export default PerksSpinner;
