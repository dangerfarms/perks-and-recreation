import React from "react";
import PropTypes from "prop-types";
import Tick from "../Icons/Tick";
import styles from "./Checkbox.module.css";

const Checkbox = ({ checked, onChange, label, className }) => {
  const combinedClassName = className
    ? `${styles.checkbox} ${className}`
    : styles.checkbox;

  return (
    <label className={className}>
      <span className={styles.background}>{checked ? <Tick /> : null}</span>
      <input
        checked={checked}
        className={combinedClassName}
        data-ref="newProposalRecurring"
        onChange={onChange}
        type="checkbox"
      />
      {label}
    </label>
  );
};

Checkbox.propTypes = {
  checked: PropTypes.bool,
  className: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
};

Checkbox.defaultProps = {
  checked: false,
  className: null,
  label: "",
  onChange: () => {},
};

export default Checkbox;
