import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import classnames from "classnames";
import styles from "./Button.module.css";

export default function Button({
  children,
  className,
  dataRef,
  disabled,
  onClick,
  linkTo,
  secondary,
  type,
}) {
  // Render a <Link /> if the `linkTo` prop is provided, otherwise it should be
  // a plain <button />
  let ButtonComponent;
  let componentSpecificProps;

  if (linkTo !== null) {
    // Then treat the button as a Link
    ButtonComponent = Link;
    componentSpecificProps = { to: linkTo };
  } else {
    ButtonComponent = "button";
    componentSpecificProps = {
      type,
    };
  }

  return (
    <ButtonComponent
      className={classnames(
        styles.button,
        {
          [styles.secondary]: secondary,
        },
        className,
      )}
      data-ref={dataRef}
      disabled={disabled}
      onClick={onClick}
      {...componentSpecificProps}
    >
      {children}
    </ButtonComponent>
  );
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  dataRef: PropTypes.string,
  disabled: PropTypes.bool,
  linkTo: PropTypes.string,
  onClick: PropTypes.func,
  secondary: PropTypes.bool,
  type: PropTypes.string,
};

Button.defaultProps = {
  className: null,
  dataRef: null,
  disabled: false,
  linkTo: null,
  onClick: null,
  secondary: false,
  type: "button",
};
