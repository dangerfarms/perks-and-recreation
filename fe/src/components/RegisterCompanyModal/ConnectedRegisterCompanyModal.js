import loglevel from "loglevel";
import { gql, useMutation } from "@apollo/client";
import React from "react";
import { Redirect } from "react-router-dom";
import { AUTH_STATUSES } from "../../utils/constants";
import { hideFlashMessage, showFlashMessage } from "../../utils/flashMessage";
import { useAuthMutation } from "../../utils/hooks/useAuthMutation";
import { useAuthStatus } from "../../utils/hooks/useAuthStatus";
import RegisterCompany from "./RegisterCompany";

export const createCompanyMutation = gql`
  mutation CreateCompany($name: String!, $admin: UserInput!) {
    createCompany(name: $name, admin: $admin) {
      company {
        name
        companyUsers {
          user {
            firstName
            lastName
            email
          }
        }
      }
    }
  }
`;

const ConnectedRegisterCompanyModal = () => {
  const { authStatus, setAuthStatus } = useAuthStatus();
  const { authenticate } = useAuthMutation();

  const [createCompany] = useMutation(createCompanyMutation);

  const onSubmitForm = async createCompanyFormData => {
    hideFlashMessage();

    // Create the company
    try {
      await createCompany({
        variables: createCompanyFormData,
      });
    } catch (error) {
      let errorMessage = error.graphQLErrors[0].message;
      if (
        error.message.includes("A user with this email address already exists")
      ) {
        errorMessage =
          "A user with this email address already exists. Only new users can register a company";
      }

      setAuthStatus(AUTH_STATUSES.SIGN_IN_FAILED);
      showFlashMessage("error", errorMessage);
      return;
    }

    // Log in
    setAuthStatus(AUTH_STATUSES.SIGNING_IN);
    try {
      await authenticate(
        createCompanyFormData.admin.email,
        createCompanyFormData.admin.password,
      );
      setAuthStatus(AUTH_STATUSES.SIGNED_IN);
      showFlashMessage("success", "Company registered successfully");
    } catch (e) {
      // Better to log the error than swallow it, this is an unexpected case.
      loglevel.error(e);

      setAuthStatus(AUTH_STATUSES.SIGN_IN_FAILED);
      showFlashMessage(
        "error",
        "Sorry, could not create an administrator account with the details provided",
      );
    }
  };

  if (authStatus === AUTH_STATUSES.SIGNED_IN) {
    return <Redirect to="/" />;
  }

  return <RegisterCompany onClickOK={onSubmitForm} />;
};

export default ConnectedRegisterCompanyModal;
