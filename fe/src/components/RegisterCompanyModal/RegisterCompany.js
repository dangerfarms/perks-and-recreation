import PropTypes from "prop-types";
import React, { useState } from "react";
import { hideFlashMessage, showFlashMessage } from "../../utils/flashMessage";
import { emailIsValid } from "../../utils/validateInput";
import Button from "../common/Button";
import SubmitButton from "../common/SubmitButton";
import Input from "../common/Input";
import PolygonFormPage from "../common/PolygonFormPage";
import styles from "./RegisterCompany.module.css";

const RegisterCompany = ({ onClickOK }) => {
  const [name, setName] = useState("");
  const [adminFirstName, setAdminFirstName] = useState("");
  const [adminLastName, setAdminLastName] = useState("");
  const [adminEmail, setAdminEmail] = useState("");
  const [adminPassword, setAdminPassword] = useState("");

  const createCompanyVariables = {
    name,
    admin: {
      firstName: adminFirstName,
      lastName: adminLastName,
      email: adminEmail,
      password: adminPassword,
    },
  };

  const showValidationFlashMessage = message =>
    showFlashMessage("error", message);

  const validateForm = () => {
    if (!name.length) {
      showValidationFlashMessage("Company name cannot be blank.");
    } else if (!adminFirstName.length) {
      showValidationFlashMessage("Admin first name cannot be blank.");
    } else if (!adminLastName.length) {
      showValidationFlashMessage("Admin last name cannot be blank.");
    } else if (!adminEmail.length) {
      showValidationFlashMessage("Admin email cannot be blank.");
    } else if (!emailIsValid(adminEmail)) {
      showValidationFlashMessage("Admin email address is invalid.");
    } else if (adminPassword.length < 8) {
      showValidationFlashMessage("Password must be 8 characters or greater.");
    } else {
      hideFlashMessage();
      return true;
    }

    return false;
  };

  const handleSubmit = () => {
    const isFormValid = validateForm();
    if (isFormValid) {
      onClickOK(createCompanyVariables);
    }
  };

  const formID = "registerCompanyForm";

  return (
    <PolygonFormPage
      formProps={{
        id: formID,
        noValidate: true,
      }}
      onSubmit={handleSubmit}
    >
      <h2 className={styles.formHeading}>Register company</h2>
      <Input
        data-ref="registerCompanyNameInput"
        onChange={e => setName(e.target.value)}
        placeholder="Company name"
        required
        type="text"
        value={name}
      />
      <Input
        data-ref="registerCompanyAdminFirstNameInput"
        onChange={e => setAdminFirstName(e.target.value)}
        placeholder="Administrator first name"
        required
        type="text"
        value={adminFirstName}
      />
      <Input
        data-ref="registerCompanyAdminLastNameInput"
        onChange={e => setAdminLastName(e.target.value)}
        placeholder="Administrator last name"
        required
        type="text"
        value={adminLastName}
      />
      <Input
        data-ref="registerCompanyEmailInput"
        onChange={e => setAdminEmail(e.target.value)}
        placeholder="Administrator email"
        required
        type="email"
        value={adminEmail}
      />
      <Input
        data-ref="registerCompanyAdminPassword"
        onChange={e => setAdminPassword(e.target.value)}
        placeholder="Administrator password"
        required
        type="password"
        value={adminPassword}
      />
      <a
        alt="Beta notice"
        className={styles.betaNoticeLink}
        href="/beta-notice.txt"
        target="blank"
      >
        Beta notice
      </a>

      <div className={styles.buttonContainer}>
        <SubmitButton dataRef="registerCompanySubmitButton" form={formID}>
          OK
        </SubmitButton>
        <Button linkTo="/login" secondary>
          Cancel
        </Button>
      </div>
    </PolygonFormPage>
  );
};

export default RegisterCompany;

RegisterCompany.propTypes = {
  onClickOK: PropTypes.func.isRequired,
};
