import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { backgroundDecorator } from "../utils/storybook";
import Proposal from "./Proposal";

const proposalData = {
  name: "Fruit bowl for the next 3 months",
  description: "Health is good for you",
  estimatedCost: 100,
  owner: "phil@dangerfarms.com",
  voteCount: 0,
  votes: [],
  createdAt: "2019-09-20T15:25:26.473327",
  id: "PROPOSAL2019-09-20T15:25:26.473327",
  isDeleted: null,
  isRecurring: null,
  status: "OPEN",
  threshold: 50,
};

const isAdmin = true;

const remainingBudget = 1000;

const user = "phil@dangerfarms.com";

const users = [
  { __typename: "User", email: "balint@dangerfarms.com", isAdmin: "false" },
];

storiesOf("Proposal", module)
  .addDecorator(backgroundDecorator)
  .add("a proposal!", () => (
    <Proposal
      data={proposalData}
      isAdmin={isAdmin}
      onEditProposal={action("onEditProposal")}
      onVote={action("onVote")}
      remainingBudget={remainingBudget}
      showConfirmPurchasePrompt={action("showConfirmPurchasePrompt")}
      showDeleteProposalModal={action("showDeleteProposalModal")}
      user={user}
      users={users}
    />
  ));
