import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import ProposalForm from "./ProposalForm";

storiesOf("ProposalForm", module)
  .add("default", () => (
    <ProposalForm
      onClickCancel={action("Cancel clicked.")}
      onClickOK={action("OK clicked.")}
    />
  ))
  .add("with errors", () => (
    <ProposalForm
      formErrors={["Errors."]}
      onClickCancel={action("Cancel clicked.")}
      onClickOK={action("OK clicked.")}
    />
  ));
