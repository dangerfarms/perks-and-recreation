import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import PropTypes from "prop-types";
import {
  allDataWithPaginatedProposalsQuery,
  createProposalMutation,
  editProposalMutation,
} from "../../utils/graphql/queries";
import ProposalForm from "./ProposalForm";

const ConnectedProposalForm = ({
  proposalToEdit,
  onSubmitProposal,
  onClickCancel,
}) => {
  const mutation = proposalToEdit
    ? editProposalMutation
    : createProposalMutation;
  const proposalDataName = proposalToEdit ? "editProposal" : "createProposal";

  const [formError, setFormError] = useState(null);

  const [createProposal] = useMutation(mutation, {
    onCompleted: onSubmitProposal,
    onError: setFormError,
    update(
      cache,
      {
        data: {
          [proposalDataName]: { proposal },
        },
      },
    ) {
      const beforeCache = cache.readQuery({
        query: allDataWithPaginatedProposalsQuery,
        variables: { filter: "all", sort: "DATE_DESCENDING" },
      });

      const previousPaginatedProposals = beforeCache.paginatedProposals;
      let newPaginatedProposals;

      if (proposalToEdit) {
        previousPaginatedProposals.proposals = previousPaginatedProposals.proposals.map(
          prevProposal =>
            prevProposal.id === proposal.id ? proposal : prevProposal,
        );
        newPaginatedProposals = {
          ...previousPaginatedProposals,
          cursor: previousPaginatedProposals.cursor,
        };
      } else {
        newPaginatedProposals = {
          ...previousPaginatedProposals,
          cursor: {
            ...previousPaginatedProposals.cursor,
            current: previousPaginatedProposals.cursor.current + 1,
            final: previousPaginatedProposals.cursor.final + 1,
          },
          proposals: [proposal, ...previousPaginatedProposals.proposals],
        };
      }

      cache.writeQuery({
        query: allDataWithPaginatedProposalsQuery,
        data: {
          ...beforeCache,
          paginatedProposals: newPaginatedProposals,
        },
        variables: { filter: "all", sort: "DATE_DESCENDING" },
      });
    },
  });

  const onSubmitForm = proposalObject => {
    setFormError(null);

    createProposal({
      variables: {
        ...(proposalToEdit && {
          proposal: proposalToEdit.id,
        }),
        ...proposalObject,
      },
    });
  };

  return (
    <ProposalForm
      formErrors={formError}
      onClickCancel={onClickCancel}
      onClickOK={onSubmitForm}
      proposal={proposalToEdit}
    />
  );
};

export default ConnectedProposalForm;

ConnectedProposalForm.propTypes = {
  onClickCancel: PropTypes.func.isRequired,
  onSubmitProposal: PropTypes.func.isRequired,
  proposalToEdit: PropTypes.shape(),
};

ConnectedProposalForm.defaultProps = {
  proposalToEdit: null,
};
