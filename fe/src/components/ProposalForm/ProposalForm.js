import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { Modal, ModalButton, ModalSubmitButton } from "@dangerfarms/ui";
import Checkbox from "../common/Checkbox";
import DatePicker from "../common/DatePicker";
import styles from "./ProposalForm.module.css";

const ProposalForm = ({ onClickCancel, onClickOK, formErrors, proposal }) => {
  const [name, setName] = useState(proposal ? proposal.name : "");
  const [description, setDescription] = useState(
    proposal ? proposal.description : "",
  );
  const [isRecurring, setIsRecurring] = useState(
    proposal ? !!proposal.isRecurring : false,
  );
  const [startDate, setStartDate] = useState(
    proposal ? proposal.startDate : null,
  );
  const [estimatedCost, setEstimatedCost] = useState(
    proposal ? proposal.estimatedCost : "",
  );

  const proposalObject = {
    description,
    name,
    isRecurring,
    startDate,
    estimatedCost,
  };

  return (
    <Modal
      buttons={
        <>
          <ModalSubmitButton
            dataRef="newProposalSubmitButton"
            form="newProposalForm"
          >
            OK
          </ModalSubmitButton>
          <ModalButton onClick={onClickCancel}>Cancel</ModalButton>
        </>
      }
      onCancel={onClickCancel}
      onConfirm={() => onClickOK(proposalObject)}
    >
      <div className={styles.formContainer}>
        <h2>Create proposal</h2>

        <form
          id="newProposalForm"
          onSubmit={e => {
            e.preventDefault();
            onClickOK(proposalObject);
          }}
        >
          <input
            className={styles.input}
            data-ref="newProposalNameInput"
            onChange={e => setName(e.target.value)}
            placeholder="Name"
            value={name}
          />
          <textarea
            className={styles.input}
            data-ref="newProposalDescriptionInput"
            onChange={e => setDescription(e.target.value)}
            placeholder="Description"
            rows="3"
            value={description}
          />
          <Checkbox
            checked={isRecurring}
            className={styles.checkbox}
            label="This is a recurring proposal"
            onChange={() => setIsRecurring(!isRecurring)}
          />
          {isRecurring ? (
            <>
              <DatePicker
                onChange={setStartDate}
                placeholderText="Date of first bill"
                value={startDate}
              />

              <input
                className={`${styles.input} ${
                  styles.recurringEstimatedCostInput
                }`}
                data-ref="newProposalEstimatedCostInput"
                min="0"
                onChange={e => setEstimatedCost(e.target.value)}
                placeholder="Estimated monthly cost (£)"
                type="number"
                value={estimatedCost}
              />
              <div className={styles.recurringCostPredictionContainer}>
                {estimatedCost ? (
                  <p className={styles.recurringCostPrediction}>
                    {`That's about £${estimatedCost * 12} a year`}
                  </p>
                ) : null}
              </div>
            </>
          ) : (
            <input
              className={styles.input}
              data-ref="newProposalEstimatedCostInput"
              min="0"
              onChange={e => setEstimatedCost(e.target.value)}
              placeholder="Estimated cost (£)"
              type="number"
              value={estimatedCost}
            />
          )}

          <div
            className={classnames(styles.errorMessage, {
              [styles.isShown]: !!formErrors,
            })}
            data-ref="newProposalErrorMessage"
          >
            {`Sorry, your proposal couldn't be ${
              proposal ? "edited" : "created"
            }`}
          </div>
        </form>
      </div>
    </Modal>
  );
};

export default ProposalForm;

ProposalForm.propTypes = {
  formErrors: PropTypes.shape(),
  onClickCancel: PropTypes.func.isRequired,
  onClickOK: PropTypes.func.isRequired,
  proposal: PropTypes.shape(),
};

ProposalForm.defaultProps = {
  formErrors: null,
  proposal: null,
};
