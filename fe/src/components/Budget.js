import React from "react";
import PropTypes from "prop-types";
import { Spinner } from "@dangerfarms/ui/lib/core";
import styles from "./Budget.module.css";

const Budget = ({ budget, children }) => (
  <div>
    {budget ? (
      <h1 className={styles.heading}>£{budget}</h1>
    ) : (
      <Spinner fill="#fff" size={30} />
    )}
    {children}
  </div>
);

Budget.propTypes = {
  budget: PropTypes.string,
  children: PropTypes.node,
};

Budget.defaultProps = {
  budget: null,
  children: null,
};

export default Budget;
