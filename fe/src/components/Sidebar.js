import React from "react";
import classnames from "classnames";
import { PropTypes } from "prop-types";
import { budgetShape, filterShape, sortShape } from "../utils/propTypes";
import Button from "./common/Button";
import styles from "./Sidebar.module.css";
import Budget from "./Budget";
import Plus from "./common/Icons/Plus";

export default function Sidebar(props) {
  return (
    <div className={styles.sidebar}>
      <h1 className={styles.companyName}>Danger Farms</h1>
      <h2 className={styles.appName}>Perks and Recreation</h2>

      <Budget
        budget={props.budget}
        className={styles.budget}
        isAdmin={props.isAdmin}
        onOpenAdminSettings={props.onOpenAdminSettings}
      />

      <div className={styles.createProposalButtonContainer}>
        <Button
          dataRef="createProposalButton"
          onClick={props.onClickCreateProposal}
        >
          <Plus />
          <span className={styles.createProposalButtonText}>
            Create proposal
          </span>
        </Button>
      </div>

      <p className={styles.filterHeading}>Filter by</p>

      <div className={styles.filters}>
        {props.filters.map(filter => (
          <Button
            key={filter.displayName}
            className={classnames(styles.filterButton, {
              [styles.filterButton__active]: filter === props.activeFilter,
            })}
            onClick={() => props.setProposalsFilter(filter)}
          >
            {filter.displayName}
          </Button>
        ))}
      </div>

      <p className={styles.filterHeading}>Sort by</p>

      <div className={styles.filters}>
        {props.sorts.map(sort => (
          <Button
            key={sort.displayName}
            className={classnames(styles.filterButton, {
              [styles.filterButton__active]: sort === props.activeSort,
            })}
            onClick={() => props.setProposalsSort(sort)}
          >
            {sort.displayName}
          </Button>
        ))}
      </div>

      <div className={styles.spacer} />

      <div className={styles.loginInfo}>
        <p className={styles.username} data-ref="sidebarUsername">
          {props.user}
        </p>
        <Button
          className={styles.signOutButton}
          dataRef="sidebarSignOutButton"
          onClick={props.onSignout}
        >
          <h3 className={styles.signOut}>Sign out</h3>
        </Button>
      </div>
    </div>
  );
}

Sidebar.propTypes = {
  activeFilter: PropTypes.shape(filterShape),
  activeSort: PropTypes.shape(sortShape),
  budget: PropTypes.shape(budgetShape),
  filters: PropTypes.arrayOf(PropTypes.shape(filterShape)).isRequired,
  isAdmin: PropTypes.bool,
  onClickCreateProposal: PropTypes.func.isRequired,
  onOpenAdminSettings: PropTypes.func.isRequired,
  onSignout: PropTypes.func.isRequired,
  setProposalsFilter: PropTypes.func.isRequired,
  setProposalsSort: PropTypes.func.isRequired,
  sorts: PropTypes.arrayOf(PropTypes.shape(sortShape)).isRequired,
  user: PropTypes.string,
};

Sidebar.defaultProps = {
  activeFilter: null,
  activeSort: null,
  budget: null,
  isAdmin: false,
  user: null,
};
