import React from "react";
import { Redirect, Route } from "react-router-dom";
import { AUTH_STATUSES } from "../../utils/constants";
import { useAuthStatus } from "../../utils/hooks/useAuthStatus";

/**
 * A route that can only be accessed by an authenticated user. Redirects unauthenticated users
 * to the login page
 * @param {import("react-router-dom").RouteProps} props
 */
const ProtectedRoute = props => {
  const { authStatus } = useAuthStatus();

  // Note that while auth status is pending, we shouldn't redirect to login yet.
  const shouldRedirectToLogin =
    authStatus === AUTH_STATUSES.SIGNED_OUT ||
    authStatus === AUTH_STATUSES.SIGN_IN_FAILED;
  return shouldRedirectToLogin ? (
    <Redirect to="/login" />
  ) : (
    <Route {...props} />
  );
};

export default ProtectedRoute;
