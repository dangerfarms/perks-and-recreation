import React from "react";
import PropTypes from "prop-types";
import Proposal from "./Proposal";
import styles from "./ProposalList.module.css";

export default function ProposalList({
  isAdmin,
  onEditProposal,
  proposals,
  remainingBudget,
  onPurchase,
  showDeleteProposalModal,
  user,
  users,
  onVote,
  // onDeleteError,
}) {
  return (
    <main className={styles.proposalList}>
      {proposals.map(proposal => (
        <Proposal
          key={proposal.createdAt}
          data={proposal}
          isAdmin={isAdmin}
          // onDeleteError={onDeleteError}
          onEditProposal={onEditProposal}
          onPurchase={onPurchase}
          onVote={onVote}
          remainingBudget={remainingBudget}
          showDeleteProposalModal={showDeleteProposalModal}
          user={user}
          users={users}
        />
      ))}
    </main>
  );
}

ProposalList.propTypes = {
  isAdmin: PropTypes.bool.isRequired,
  onEditProposal: PropTypes.func.isRequired,
  onPurchase: PropTypes.func.isRequired,
  onVote: PropTypes.func.isRequired,
  proposals: PropTypes.arrayOf(PropTypes.shape()),
  remainingBudget: PropTypes.number,
  showDeleteProposalModal: PropTypes.func.isRequired,
  user: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

ProposalList.defaultProps = {
  proposals: null,
  remainingBudget: null,
};
