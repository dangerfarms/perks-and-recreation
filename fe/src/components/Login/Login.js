import React from "react";
import PropTypes from "prop-types";
import Button from "../common/Button";
import SubmitButton from "../common/SubmitButton";
import Input from "../common/Input";
import PolygonFormPage from "../common/PolygonFormPage";
import styles from "./Login.module.css";

/**
 * Presentation-only component for the login form. State + API interactions
 * are handled by the connected parent.
 */
const Login = ({
  email,
  password,
  onEmailChange,
  onPasswordChange,
  onSubmit,
}) => (
  <PolygonFormPage onSubmit={onSubmit}>
    <h1 className={styles.loginPageHeading}>Perks and Recreation</h1>

    <Input
      data-ref="emailInput"
      onChange={onEmailChange}
      placeholder="Email address"
      required
      type="email"
      value={email}
    />
    <Input
      data-ref="passwordInput"
      onChange={onPasswordChange}
      placeholder="Password"
      required
      type="password"
      value={password}
    />

    <SubmitButton className={styles.button}>Sign in</SubmitButton>
    <p className={styles.buttonDivider}>OR</p>
    <Button linkTo="/register">Register new company</Button>
  </PolygonFormPage>
);

Login.propTypes = {
  email: PropTypes.string.isRequired,
  onEmailChange: PropTypes.func.isRequired,
  onPasswordChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  password: PropTypes.string.isRequired,
};

export default Login;
