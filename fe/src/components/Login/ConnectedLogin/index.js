import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";
import { AUTH_STATUSES } from "../../../utils/constants";
import client from "../../../utils/graphql/client";
import { useAuthMutation } from "../../../utils/hooks/useAuthMutation";
import { useAuthStatus } from "../../../utils/hooks/useAuthStatus";
import PerksSpinner from "../../common/PerksSpinner";
import Login from "../Login";
import {
  hideFlashMessage,
  showFlashMessage,
} from "../../../utils/flashMessage";

const ConnectedLogin = () => {
  const [didMount, setDidMount] = useState(false);
  const [
    postSignInAnimationDidComplete,
    setPostSignInAnimationDidComplete,
  ] = useState(false);
  const { authStatus, setAuthStatus } = useAuthStatus();

  // The email + password state from the login form is lifted up here. This
  // allows the state to be persisted across bad login attempts, so the user
  // doesn't lose the email they entered (usually it's the password that's
  // wrong).
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const { authenticate } = useAuthMutation();

  useEffect(() => {
    setDidMount(true);
  }, []);

  // TODO: manage the form inputs in a DRYer way if more are added
  const onEmailChange = event => {
    const { value } = event.target;

    setFormData(prevFormData => ({
      ...prevFormData,
      email: value,
    }));
  };

  // TODO: manage the form inputs in a DRYer way if more are added
  const onPasswordChange = event => {
    const { value } = event.target;

    setFormData(prevFormData => ({
      ...prevFormData,
      password: value,
    }));
  };

  const onUserSignInAttempt = async () => {
    hideFlashMessage();
    setAuthStatus(AUTH_STATUSES.SIGNING_IN);

    try {
      const response = await authenticate(formData.email, formData.password);

      client.writeData({
        data: { refreshToken: response.data.tokenAuth.refreshToken },
      });
      setAuthStatus(AUTH_STATUSES.SIGNED_IN);
    } catch (error) {
      // TODO: assumes only a single API error at a time, maybe should handle multiple?
      showFlashMessage("error", error.graphQLErrors[0].message);
      setAuthStatus(AUTH_STATUSES.SIGN_IN_FAILED);

      // Clear the password state on a bad login attempt: since the input
      // is masked, editing/verifying the password is impossible. Users will
      // always just want to clear it and start again anyway.
      setFormData(prevFormData => ({
        ...prevFormData,
        password: "",
      }));
    }
  };

  // Redirect already authenticated users to the dashboard during the initial mount
  if (authStatus === AUTH_STATUSES.SIGNED_IN && !didMount) {
    return <Redirect to="/" />;
  }

  // Render a loading spinner while signing in
  if (authStatus === AUTH_STATUSES.SIGNING_IN) {
    return <PerksSpinner />;
  }

  // Animate the spinner on sign in before redirecting
  if (
    authStatus === AUTH_STATUSES.SIGNED_IN &&
    !postSignInAnimationDidComplete
  ) {
    return (
      <PerksSpinner
        animateOutCallback={() => setPostSignInAnimationDidComplete(true)}
        shouldAnimateOut
      />
    );
  }

  // Redirect to the dashboard after the animation completes
  if (
    authStatus === AUTH_STATUSES.SIGNED_IN &&
    postSignInAnimationDidComplete
  ) {
    return <Redirect to="/" />;
  }

  return (
    <Login
      email={formData.email}
      onEmailChange={onEmailChange}
      onPasswordChange={onPasswordChange}
      onSubmit={onUserSignInAttempt}
      password={formData.password}
    />
  );
};

export default ConnectedLogin;
