import React from "react";
import PropTypes from "prop-types";
import { Modal } from "@dangerfarms/ui/lib/core";

const DeleteProposalModal = ({
  onCancel,
  useDeleteProposal,
  proposalToDelete,
  hideDeleteProposalModal,
}) => {
  const [doDelete] = useDeleteProposal(proposalToDelete);

  const onDeleteProposalConfirm = () => {
    doDelete({
      variables: { proposal: proposalToDelete },
    });
    hideDeleteProposalModal();
  };

  return (
    <Modal
      dataRef="deleteProposalModal"
      onCancel={onCancel}
      onConfirm={onDeleteProposalConfirm}
      size={400}
    >
      <p>Are you sure you want to delete this proposal?</p>
    </Modal>
  );
};

DeleteProposalModal.propTypes = {
  hideDeleteProposalModal: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  proposalToDelete: PropTypes.number.isRequired,
  useDeleteProposal: PropTypes.func.isRequired,
};

DeleteProposalModal.defaultProps = {
  onCancel: null,
};

export default DeleteProposalModal;
