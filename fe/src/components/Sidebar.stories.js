import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { backgroundDecorator } from "../utils/storybook";
import { SORTS, FILTERS } from "../utils/constants";
import Sidebar from "./Sidebar";

const activeFilter = {
  displayName: "All",
  query: {},
  variables: {},
};

const budget = {
  remainingBudget: 1000,
  totalBudget: 5000,
};

const user = "craig@dangerfarms.com";

storiesOf("Sidebar", module)
  .addDecorator(backgroundDecorator)
  .add("default", () => (
    <Sidebar
      activeFilter={activeFilter}
      budget={budget}
      filters={FILTERS}
      onClickCreateProposal={action("onClickCreateProposal")}
      onSignout={action("onSignout")}
      setProposalsFilter={action("setProposalsFilter")}
      setProposalsSort={action("setActiveSort")}
      sorts={SORTS}
      user={user}
    />
  ));
