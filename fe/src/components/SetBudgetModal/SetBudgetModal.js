import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal, ModalButton, ModalSubmitButton } from "@dangerfarms/ui";
import { hideFlashMessage, showFlashMessage } from "../../utils/flashMessage";
import styles from "./SetBudgetModal.module.css";

const polygonFill = {
  startColor: "#EA5E48",
  endColor: "#663CAB",
};

const SetBudgetModal = ({ currentBudget, onClickCancel, onClickOK }) => {
  const [totalBudget, setTotalBudget] = useState(currentBudget);

  const displayValidationMessage = message =>
    showFlashMessage("error", message);

  const validateForm = () => {
    if (Number.isNaN(totalBudget)) {
      displayValidationMessage("Budget must be a number");
    } else if (
      totalBudget.includes(".") &&
      totalBudget.split(".")[1].length > 2
    ) {
      displayValidationMessage("Budget must have a valid number of pence");
    } else {
      hideFlashMessage();
      return true;
    }

    return false;
  };

  const handleSubmit = event => {
    event.preventDefault();

    const isFormValid = validateForm();
    if (isFormValid) {
      onClickOK(totalBudget);
    }
  };

  return (
    <Modal
      buttons={
        <>
          <ModalSubmitButton
            dataRef="setBudgetSubmitButton"
            form="setBudgetForm"
          >
            OK
          </ModalSubmitButton>
          <ModalButton onClick={onClickCancel}>Cancel</ModalButton>
        </>
      }
      dataRef="setBudgetModal"
      fill={polygonFill}
      onCancel={onClickCancel}
      onConfirm={() => null}
    >
      <div className={styles.formContainer}>
        <h2 className={styles.formHeading}>Set budget</h2>

        <form id="setBudgetForm" noValidate onSubmit={handleSubmit}>
          <div className={styles.budgetInput}>
            <span className={styles.currencySymbol}>£</span>
            <input
              className={styles.input}
              data-ref="setBudgetNameInput"
              onChange={e => setTotalBudget(e.target.value)}
              placeholder={currentBudget}
              required
              type="number"
              value={totalBudget}
            />
          </div>
        </form>
      </div>
    </Modal>
  );
};

export default SetBudgetModal;

SetBudgetModal.propTypes = {
  currentBudget: PropTypes.string.isRequired,
  onClickCancel: PropTypes.func.isRequired,
  onClickOK: PropTypes.func.isRequired,
};
