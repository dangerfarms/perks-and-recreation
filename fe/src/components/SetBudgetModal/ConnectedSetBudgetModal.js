import React from "react";
import PropTypes from "prop-types";
import { gql, useMutation } from "@apollo/client";
import { hideFlashMessage, showFlashMessage } from "../../utils/flashMessage";
import SetBudgetModal from "./SetBudgetModal";

export const setBudgetMutation = gql`
  mutation SetBudget($budget: MoneyField!) {
    setBudget(budget: $budget) {
      budget
    }
  }
`;

const ConnectedSetBudgetModal = ({
  currentBudget,
  onClickCancel,
  onSuccess,
}) => {
  const [setBudget] = useMutation(setBudgetMutation, {
    onError: () =>
      showFlashMessage("error", "Sorry, your budget couldn't be changed"),
    onCompleted: onSuccess,
  });

  const onSubmitForm = async budget => {
    hideFlashMessage();

    await setBudget({
      variables: { budget },
    });
  };

  return (
    <SetBudgetModal
      currentBudget={currentBudget}
      onClickCancel={onClickCancel}
      onClickOK={onSubmitForm}
    />
  );
};

export default ConnectedSetBudgetModal;

ConnectedSetBudgetModal.propTypes = {
  currentBudget: PropTypes.string.isRequired,
  onClickCancel: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
};
