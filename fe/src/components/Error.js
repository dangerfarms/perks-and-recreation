import React from "react";
import PropTypes from "prop-types";
import { Polygon } from "@dangerfarms/ui/lib/core";
import styles from "./Error.module.css";

const Error = ({ children, withoutSidebar }) => (
  <div
    className={
      withoutSidebar ? styles.errorMessage : styles.errorMessage__withSidebar
    }
  >
    <Polygon className={styles.polygon} fill="#F4A56B" sides={10} size={400} />
    <p className={styles.polygonContent} data-ref="errorMessage">
      {children}
    </p>
  </div>
);

Error.propTypes = {
  children: PropTypes.node.isRequired,
  withoutSidebar: PropTypes.bool,
};

Error.defaultProps = {
  withoutSidebar: false,
};

export default Error;
