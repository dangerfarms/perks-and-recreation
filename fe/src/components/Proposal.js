import React from "react";
import PropTypes from "prop-types";
import ProposalStatus from "./ProposalStatus";
import ProposalControlButtons from "./ProposalControlButtons";
import styles from "./Proposal.module.css";

/* eslint-disable react/prop-types */
function ProposalHeading({ name, estimatedCost, mobile = false, status }) {
  let className =
    status === "OPEN"
      ? styles.proposalHeading
      : styles.proposalHeading__closedProposal;
  className = mobile ? styles.proposalHeading__mobile : className;
  return (
    <div className={className}>
      <h1 className={styles.name} data-ref="proposalName">
        {name}
      </h1>
      <h2 className={styles.estimatedCost} data-ref="proposalEstimatedCost">
        £{estimatedCost}
      </h2>
    </div>
  );
}
/* eslint-enable react/prop-types */

export default function Proposal({
  data,
  isAdmin,
  onEditProposal,
  remainingBudget,
  onPurchase,
  showDeleteProposalModal,
  user,
  users,
  onVote,
  // onDeleteError,
}) {
  const proposalId = data.id;

  return (
    <div
      className={
        data.status === "OPEN" ? styles.proposal : styles.proposal__closed
      }
      data-id={data.id}
      data-ref="proposalItem"
    >
      <div className={styles.content}>
        <ProposalHeading
          estimatedCost={data.estimatedCost}
          name={data.name}
          status={data.status}
        />
        <p className={styles.description} data-ref="proposalDescription">
          {data.description}
        </p>
        <ProposalControlButtons
          currentProposal={data}
          isAdmin={isAdmin}
          isOwner={data.owner === user}
          // onDeleteError={onDeleteError}
          onEditProposal={onEditProposal}
          onPurchase={onPurchase}
          remainingBudget={remainingBudget}
          showDeleteProposalModal={showDeleteProposalModal}
        />
      </div>
      <div className={styles.status}>
        <ProposalStatus
          onVote={onVote}
          proposalId={proposalId}
          status={data.status}
          user={user}
          users={users}
          voteCount={data.voteCount}
          votes={data.votes}
        >
          <ProposalHeading
            estimatedCost={data.estimatedCost}
            mobile
            name={data.name}
          />
        </ProposalStatus>
      </div>
    </div>
  );
}

Proposal.propTypes = {
  data: PropTypes.shape().isRequired,
  isAdmin: PropTypes.bool.isRequired,
  onEditProposal: PropTypes.func.isRequired,
  onPurchase: PropTypes.func.isRequired,
  onVote: PropTypes.func.isRequired,
  remainingBudget: PropTypes.number,
  showDeleteProposalModal: PropTypes.func.isRequired,
  user: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

Proposal.defaultProps = {
  remainingBudget: null,
};
