import React from "react";
import { storiesOf } from "@storybook/react";
import { backgroundDecorator } from "../utils/storybook";
import Budget from "./Budget";

storiesOf("Budget", module)
  .addDecorator(backgroundDecorator)
  .add("arbitrary budget", () => (
    <Budget budget={{ remainingBudget: 500, totalBudget: 2000 }} />
  ))
  .add("zero budget", () => (
    <Budget budget={{ remainingBudget: 0, totalBudget: 2000 }} />
  ))
  .add("full budget", () => (
    <Budget budget={{ remainingBudget: 2000, totalBudget: 2000 }} />
  ));
