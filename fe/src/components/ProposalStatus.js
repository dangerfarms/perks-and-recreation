import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Button from "./common/Button";
import styles from "./ProposalStatus.module.css";
import PercentageBar from "./common/PercentageBar";

/* eslint-disable react/prop-types */
function OpenProposal({
  votes,
  user,
  children,
  proposalId,
  onVote,
  users,
  voteCount,
}) {
  const doVote = onVote(proposalId, user);

  const userVote = votes ? votes.find(vote => vote.user === user) : null;

  return (
    <React.Fragment>
      {children}
      <div className={styles.voteButtons}>
        <Button
          className={classnames(styles.voteNo, {
            [styles.vote__chosen]: userVote && userVote.accept === false,
          })}
          disabled={userVote && userVote.accept === false}
          onClick={() =>
            doVote({
              variables: {
                proposalId,
                accept: false,
              },
            })
          }
        >
          No{" "}
          {userVote && userVote.accept === false && (
            <React.Fragment>&#10003;</React.Fragment>
          )}
        </Button>
        <Button
          className={classnames(styles.voteYes, {
            [styles.vote__chosen]: userVote && userVote.accept,
          })}
          disabled={userVote && userVote.accept}
          onClick={() =>
            doVote({
              variables: {
                proposalId,
                accept: true,
              },
            })
          }
        >
          Yes{" "}
          {userVote && userVote.accept && (
            <React.Fragment>&#10003;</React.Fragment>
          )}
        </Button>
      </div>
      <p className={styles.votesText}>
        {`Waiting for ${users.length - voteCount} vote${
          users.length - voteCount > 1 ? "s" : ""
        }...`}
      </p>
    </React.Fragment>
  );
}

function ClosedProposal({ status, votes }) {
  const totalVotesFor = votes.reduce(
    (votesFor, vote) => votesFor + (vote.accept ? 1 : 0),
    0,
  );
  const votesForPercentage = Math.round((totalVotesFor / votes.length) * 100);
  const votesForPercentageText =
    votesForPercentage > 0 ? `${votesForPercentage}%` : "None";
  return (
    <React.Fragment>
      <div
        className={
          status === "REJECTED"
            ? styles.redStatusBadge
            : styles.greenStatusBadge
        }
      >
        {status.toLowerCase()}
      </div>
      <PercentageBar
        className={styles.bar}
        remaining={totalVotesFor}
        total={votes.length}
      />
      <p className={styles.votesText}>
        {`${votesForPercentageText} of us voted for this idea`}
      </p>
    </React.Fragment>
  );
}
/* eslint-enable react/prop-types */

export default function ProposalStatus({
  onVote,
  proposalId,
  user,
  users,
  voteCount,
  votes,
  status,
  children,
}) {
  return !status || status === "OPEN" ? (
    <OpenProposal
      onVote={onVote}
      proposalId={proposalId}
      user={user}
      users={users}
      voteCount={voteCount}
      votes={votes}
    >
      {children}
    </OpenProposal>
  ) : (
    <ClosedProposal status={status} votes={votes} />
  );
}

ProposalStatus.propTypes = {
  children: PropTypes.node.isRequired,
  onVote: PropTypes.func.isRequired,
  proposalId: PropTypes.string.isRequired,
  status: PropTypes.string,
  user: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  voteCount: PropTypes.number,
  votes: PropTypes.arrayOf(PropTypes.shape()),
};

ProposalStatus.defaultProps = {
  status: "OPEN",
  voteCount: null,
  votes: null,
};
