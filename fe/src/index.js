import { ApolloProvider } from "@apollo/client";
import * as Sentry from "@sentry/browser";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import client from "./utils/graphql/client";
import "./utils/typography";

if (process.env.REACT_APP_SENTRY_RELEASE) {
  Sentry.init({
    dsn: "https://96c8908831904b6fb2c5e56a4259c216@sentry.io/1490008",
    release: process.env.REACT_APP_SENTRY_RELEASE,
  });
}

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root"),
);
