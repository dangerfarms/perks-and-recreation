import { useQuery } from "@apollo/client";
import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import styles from "./App.module.css";
import PerksSpinner from "./components/common/PerksSpinner";
import ConnectedDashboard from "./components/Dashboard/ConnectedDashboard";
import { dashboardQuery } from "./components/Dashboard/queries";
import Error from "./components/Error";
import ConnectedLogin from "./components/Login/ConnectedLogin";
import ProtectedRoute from "./components/ProtectedRoute";
import { AUTH_STATUSES } from "./utils/constants";
import { useAuthStatus } from "./utils/hooks/useAuthStatus";
import FlashMessage from "./components/common/FlashMessage";
import ConnectedRegisterCompanyModal from "./components/RegisterCompanyModal/ConnectedRegisterCompanyModal";

const App = () => {
  const { authStatus, setAuthStatus } = useAuthStatus();

  /*
   * This query is made purely to check whether we have a valid auth token cookie. We're using
   * the dashboard query since it's most likely the page we'll be redirecting to and it'll
   * benefit from the query being cached.
   *
   * Set `skip` to true and refetch in `useEffect` so that this only happens once when the app loads.
   */
  const { refetch: refetchDashboardQuery } = useQuery(dashboardQuery, {
    fetchPolicy: "network-only",
    skip: true,
  });

  const [didInitialAuthCheck, setDidInitialAuthCheck] = useState(false);

  // TODO: this is another place that suffers from poor ability to tell API errors
  //       apart, imo. If the backend is down, the query also fails, so a logged-in user
  //       is (misleadingly) redirected to log in again (which will then fail,
  //       wasting their time and confusing them). We'd probably instead want to
  //       show an error toast on the dashboard.
  useEffect(() => {
    setDidInitialAuthCheck(true);
    if (didInitialAuthCheck) return;

    // The callback passed to `useEffect` can't be async, so we have to define it here
    const refetchQueryAsync = async () => {
      try {
        await refetchDashboardQuery();
        setAuthStatus(AUTH_STATUSES.SIGNED_IN);
      } catch (e) {
        setAuthStatus(AUTH_STATUSES.SIGNED_OUT);
      }
    };
    refetchQueryAsync();
  }, [
    didInitialAuthCheck,
    refetchDashboardQuery,
    setAuthStatus,
    setDidInitialAuthCheck,
  ]);

  if (authStatus === AUTH_STATUSES.PENDING) return <PerksSpinner />;

  return (
    <Router>
      <FlashMessage />

      <Switch>
        <Route component={ConnectedLogin} exact path="/login" />
        <Route
          component={ConnectedRegisterCompanyModal}
          exact
          path="/register"
        />
        <ProtectedRoute component={ConnectedDashboard} exact path="/" />
        <Route
          render={() => (
            <Error withoutSidebar>
              <a
                className={styles.notFoundLink}
                href="https://www.youtube.com/watch?v=veUUnLwhaEc"
              >
                404 not found!
              </a>
              <a className={styles.notFoundLink} href="/">
                Come home...
              </a>
            </Error>
          )}
        />
      </Switch>
    </Router>
  );
};

export default App;
