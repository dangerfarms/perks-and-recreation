import React from "react";
import PropTypes from "prop-types";
import { Modal } from "@dangerfarms/ui/lib/core";
import styles from "./ConfirmPurchasePrompt.module.css";

const ConfirmPurchasePrompt = ({
  onCancel,
  onConfirm,
  proposal,
  remainingBudget,
  purchaseCost,
  onChange,
}) => (
  <Modal
    fill={{
      startColor: "#663CAB",
      endColor: "#EA5E48",
    }}
    onCancel={onCancel}
    onConfirm={onConfirm}
    sides={10}
    size={400}
    spinDuration={80000}
  >
    <h2 className={styles.heading}>Confirm purchase</h2>
    <p>{`You're confirming the purchase of "${proposal.name}"`}</p>
    <input
      className={styles.input}
      max={remainingBudget}
      min="0"
      onChange={e => onChange(e.target.value)}
      placeholder="Actual price (£)"
      step={1}
      type="number"
      value={purchaseCost}
    />
    <p>{`Remaining budget: £${remainingBudget - (purchaseCost || 0)}`}</p>
  </Modal>
);

ConfirmPurchasePrompt.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  proposal: PropTypes.shape({ name: PropTypes.string.isRequired }).isRequired,
  purchaseCost: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    .isRequired,
  remainingBudget: PropTypes.number.isRequired,
};

export default ConfirmPurchasePrompt;
