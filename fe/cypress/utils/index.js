export const getByRef = ref => cy.get(`[data-ref="${ref}"]`);

export const getProposalById = id => cy.get(`[data-id="${id}"]`);
