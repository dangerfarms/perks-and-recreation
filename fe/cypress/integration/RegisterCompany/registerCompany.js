import { Then, When } from "cypress-cucumber-preprocessor/steps";
import { getByRef } from "../../utils";
import { CompanyFactory } from "../../fixtures/companyFactory";

When("I click the Register new company button", () => {
  cy.get("a")
    .contains("Register new company")
    .click();
});

Then("I see the Register company form", () => {
  cy.contains("Register company");
});

When("I fill out the Register company form", () => {
  const {
    administratorEmail,
    administratorFirstName,
    administratorLastName,
    administratorPassword,
    companyName,
  } = CompanyFactory.create();

  getByRef("registerCompanyNameInput").type(companyName, { delay: 0 });
  getByRef("registerCompanyAdminFirstNameInput").type(administratorFirstName, {
    delay: 0,
  });
  getByRef("registerCompanyAdminLastNameInput").type(administratorLastName, {
    delay: 0,
  });
  getByRef("registerCompanyEmailInput").type(administratorEmail, { delay: 0 });
  getByRef("registerCompanyAdminPassword").type(administratorPassword, {
    delay: 0,
  });
});

When("I click the OK button", () => {
  getByRef("registerCompanySubmitButton").click();
});
