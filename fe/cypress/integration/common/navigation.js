import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

Given("I am an unauthenticated user", () => {
  // TODO: Implement
  console.error("Not implemented!"); // eslint-disable-line no-console
});

Given("I am an authenticated user", () => {
  // TODO: Implement
  console.error("Not implemented!"); // eslint-disable-line no-console
});

When("I navigate to the app", () => {
  cy.visit("/");
});

Then("I see the dashboard", () => {
  // Assumes that the dashboard URL is the root one.
  cy.location().should(location => {
    expect(location.pathname).to.eq("/");
  });
});
