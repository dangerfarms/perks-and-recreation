Feature: Register company

  As a Company Admin, I want to register my Company through username / password so that my Company can start using the app

  Scenario: Create a new company
    Given I am an unauthenticated user
    When I navigate to the app
    And I click the Register new company button
    Then I see the Register company form

    When I fill out the Register company form
    And I click the OK button
    Then I see the dashboard

  Scenario: Visit the app as an existing user
    Given I am an authenticated user
    When I navigate to the app
    Then I see the dashboard
