import faker from "faker";

export class CompanyFactory {
  static create() {
    const firstName = faker.name.firstName(undefined);
    const lastName = faker.name.lastName(undefined);
    const email = `test+${faker.internet.email(
      firstName,
      lastName,
      "dangerfarms.com",
    )}`;
    this.data = {
      administratorEmail: email,
      administratorFirstName: firstName,
      administratorLastName: lastName,
      administratorPassword: faker.internet.password(),
      companyName: faker.company.companyName(),
    };
    return this.data;
  }
}
