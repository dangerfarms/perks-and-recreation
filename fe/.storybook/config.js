import { addParameters, configure } from "@storybook/react";
import perksTheme from "./perksTheme";
import "../src/index.css";
import "../src/utils/typography";

addParameters({
  options: {
    theme: perksTheme,
  },
});

function loadStories() {
  const req = require.context("../src", true, /\.stories\.js$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
