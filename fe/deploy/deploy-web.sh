#!/usr/bin/env sh
# Usage
# ./deploy-web.sh <STAGE>

# https://www.peterbe.com/plog/set-ex
set -ex

STAGE=$1
if [ ! "$STAGE" ]; then
  echo "You must provide a stage name e.g. './deploy-web.sh dev' for the stage name 'dev'"
  exit 1
fi

cd fe/deploy

# Load build-time env vars
. .env

# Authenticate with gcloud using service account
# Note that it's necessary to be authenticated before pushing/pulling from Google Container Registry
gcloud auth activate-service-account --project="$PROJECT" --key-file=perks-service-account.json
gcloud auth configure-docker --quiet

# Build and push the frontend image, including some tricks to allow CI to cache intermediate image layers.
#
# Without caching, the `yarn install` step of the build (ie. populating node_modules) is executed regardless
# of changes to package.json. It's the longest step by a long way, so can end up being an unnecessary bottleneck in the
# build time.
#
# 1. Pull first. Needed so that CI has the existing image layers to use as cache
docker pull $IMAGE || true
# 2. Build the image. The BUILDKIT env vars enable Docker's new build engine. In our case this
#    is the easiest way to cache the intermediate images of the multistage build (see Dockerfile).
#    https://github.com/moby/moby/issues/34715#issuecomment-550567523
#
#    Note that at the time of writing, docker-compose for Mac isn't at v1.25.0 yet, that's the first version that
#    supports buildkit. So devs may not experience cached builds for manual deploys.
DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 web
# 3. Push the image back to GCR so that Cloud Run can access it
docker push $IMAGE

# Deploy a new revision of the service to Cloud Run
gcloud run deploy "$CLOUD_RUN_SERVICE_NAME" \
  --add-cloudsql-instances="$INSTANCE_CONNECTION_NAME" \
  --allow-unauthenticated \
  --image="$IMAGE" \
  --platform="managed" \
  --project="$PROJECT" \
  --region="$REGION" \
  --set-env-vars="$(sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/,/g' .secrets)"
