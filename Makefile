# use .deploy.env as Make variables for deployment commands
EXTRA_INCLUDES:=$(wildcard makefile.*.mk)
ifneq (,$(wildcard ./.deploy.env))
    include .deploy.env
    GOOGLE_APPLICATION_CREDENTIALS=$(shell pwd)/${GOOGLE_APPLICATION_CREDENTIALS_RELATIVE_PATH}
    # set all make variables as env vars
    export
endif


# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

## Development commands
setup-backend: ## Set up the backend environment for development
	./bin/setup-backend.sh

setup-frontend: ## Set up the frontend environment for development
	./bin/setup-frontend.sh

setup-project: ## Set up the generic project environment, githooks, etc.
	./bin/setup-project.sh

setup-infra: ## Set up the infra environment
	./bin/setup-infra.sh

setup: setup-backend setup-frontend setup-project ## Set up development environment
	@echo "🎉 Setup done!"
	@echo ""
	@echo "Please make sure that you update the following env files before you dig in:"
	@echo "./fe/.env"
	@echo "./fe/cypress.json"
	@echo "./fe/deploy/.secrets (If you are planning to deploy manually.)"
	@echo "./be/src/.env"
	@echo ".deploy.env"
	@echo "./infra/secrets.tfvars (If you are planning to deploy manually.)"

start-fe: ## Start the local frontend
	cd fe ; yarn start

start-db: ## Start the local development database
	cd be ; docker-compose up -d;

restart-db: ## Restart the local development database
	cd be; \
    docker-compose stop db ; \
    docker-compose up -d

start-be: ## Start the local backend services
	cd be ; \
	docker-compose up -d ; \
	venv/bin/python src/manage.py runserver

be-requirements: ## Install dev dependencies for the backend.
	cd be ; \
	venv/bin/pip install -r requirements.local.txt

migrate: ## Run Django migrations
	cd be ; \
	docker-compose up -d ; \
	venv/bin/python src/manage.py migrate

e2e-headless: ## Run e2e tests locally, in a headless browser. Must have both the backend running locally.
	cd fe ; yarn cy:run

e2e: ## Open Cypress to run tests in GUI.
	cd fe ; yarn cypress open

be-test: ## Run the backend tests.
	cd be ; \
	docker-compose up -d ; \
	venv/bin/python src/manage.py test

be-lint: ## Lint the entire backend code.
	cd be ; \
	. venv/bin/activate ; \
	black src ; \
	flake8 src

fe-lint: ## Lint the entire frontend code.
	cd fe ; \
	yarn lint

drop-db: ## Drop the database
	cd be ; \
	docker-compose stop && docker-compose rm -f && docker-compose down -v

## Repo commands
checkout-branch: ## Check out a branch with the convention format
	@bin/ch-checkout-new-branch.sh

## Deployment commands
check-service-account: ## Check if a service account is available and has the correct roles
	cd bin ; ./check-service-account.sh

check-cloudflare-access: ## Check if the CloudFlare setup is correct.
	cd bin ; ./check-cloudflare-access.sh

check-secret-values: ## Check that the secret files are set locally
	cd bin ; ./check-secret-values.sh

deploy-api: ## Build and deploy the API to the cloud
	./be/deploy/deploy-api.sh ${STAGE}

deploy-web: ## Build and deploy the web UI to the cloud
	./fe/deploy/deploy-web.sh ${STAGE}

infra-select-workspace: ## Select workspace for terraform
	cd infra && terraform workspace select ${STAGE}

infra-create-new-workspace:
	cd infra && terraform workspace new ${STAGE}

infra-init: check-secret-values check-service-account check-cloudflare-access ## Initialise terraform.
	cd infra && \
	terraform init

infra-plan: check-secret-values check-service-account check-cloudflare-access infra-select-workspace ## Run terraform plan with the appropriate variables. Must specify `STAGE=<stage-name>`
	cd infra && terraform plan -var-file="default.tfvars" -var-file="secrets.tfvars" -var="credentials_file=${GOOGLE_APPLICATION_CREDENTIALS}"

infra-output: check-service-account infra-select-workspace ## Run terraform ouput (JSON).
	cd infra && terraform output -json

infra-apply: check-secret-values check-service-account check-cloudflare-access infra-select-workspace ## Set up local env for infra deployment
	cd infra && terraform apply -var-file="default.tfvars" -var-file="secrets.tfvars" -var="credentials_file=${GOOGLE_APPLICATION_CREDENTIALS}"
