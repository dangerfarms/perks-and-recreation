# Legacy README notes

Perks has pivoted significantly in its lifetime. The old README notes are here for reference. Eventually we should just drop the legacy code, along with this README.

## Getting started: design

The sketch file is in the `design` folder. That is the source of truth, accept no alternatives :D

Fonts are the same as those used in the marketing site project. You can find them there if you don't have them installed already!

## Getting started: development

### Backend

Prerequisites:

* Docker
* Python 3.6
* Node
* Yarn

**Note:** You'll need Docker running to be able to deploy or run the tests.

1. First, create a `secrets.yml` file based on `secrets.template.yml`.

   The `STAGE` variable is used by Serverless to provide isolated deployments between developers. So you should use a unique (amongst the team) identifier for your default `STAGE`. We tend to use our first names.

   The `GOOGLE_OAUTH_CLIENT_ID` value should exactly match `REACT_APP_GOOGLE_LOGIN_CLIENT_ID` in the frontend.

   The `SLACK_BOT_USER_TOKEN` can be found in the "Install App" section of the slack API application found in the Danger Farms workplace. You can leave it unset in your `secrets.yml` and it will simply not post to the configured slack channel.
   The `SLACK_BOT_CHANNEL_ID` can be found by using the Slack API to find the channel ID. Ask another team member for this if it seems too complicated or time consuming.

2. Create a virtualenv and install dependencies. The `yarn` command installs the serverless plugins.

   ```bash
   python -m venv venv
   . venv/bin/activate
   pip install -r requirements.dev.txt
   yarn
   ```

3. Deploy to your stage like this:

   ```bash
   yarn sls deploy
   ```

#### Running backend integration tests

Tests are run using `pytest`. They need a DB instance to execute against. This is handled behind-the-scenes by spinning up a [DynamoDB Local container](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.Docker.html).

If Docker is running, you can simply run:

```bash
pytest perks
```

**Warning:** the first run will take a long time if you have never pulled the DynamoDB image before. Be patient 🙏

**Warning:** if the test suite quits unexpectedly due to e.g. an import
error, it's likely that the local DynamoDB instance has not shut down
correctly. This may cause all of the tests in the next suite run to fail.
If this happens, run `docker ps` to assert that the container did not
shut down, and run `docker kill <id>` with the container ID of the local
DynamoDB instance.

#### Populating dummy data

    ```bash
    serverless invoke --function addExampleItems
    ```

### Development with remote backend

Before performing any development work that requires the backend, or to run the E2E test suite, it's required to configure the AWS CLI to point to the correct AWS account.

To do this, you'll need to create a file at `~/.aws/credentials` which contains:
```
[default]
aws_access_key_id = <your key id>
aws_secret_access_key = <your key value>
```
If you do not want this AWS account to be your default, you can change `[default]` to be anything else e.g. `[df]`.
In that case you'll need to set the `AWS_PROFILE` environment variable to the profile you want to use e.g. `AWS_PROFILE=df`.

## Getting started: deployment

> Note: only relevant for maintainers.

There are two fixed deployment stages: dev + production. Both backend and frontend are deployed and versioned together.

Automated deployments to `dev` are triggered by CI when a new commit is pushed to `master`. Production deployments are automatically triggered by creating a tag with the pattern `v2`.

### Infrastructure

Frontend infrastructure is managed by Terraform, and leverages multiple cloud services. You'll need credentials for AWS, GCP and CloudFlare to deploy. You can explore the templates in `infra` for the most up-to-date description of the infrastructure used.

Backend infrastructure is managed by Serverless Framework (ie. CloudFront). You can inspect `be/serverless.yml` to see what's used.

### Infrastructure that needs to be managed manually (not via TF/CFN)

**Google OAuth Client**: this is used to allow Google login. You need to add all frontend app domain names used for deployments in the Google Cloud Console (APIs and Services -> Credentials -> OAuth 2.0 Client IDs).
**ACM certificate validation**: CloudFront uses a shared SSL certificate for our domain name `*.dangerfarms.com`. The validation process when creating the certificate can't be done via Terraform. You shouldn't need to worry about it unless the certificate needs replacing.

#### Slack configuration

Aside from the secrets mentioned in the `secrets.yml` instructions, the "Slash command" must be configured to hit the right API endpoint. This API endpoint is created by serverless as the `slackProposal` handler. You can set this value by editing the command in the "Slash Commands" section of the Slack App.
