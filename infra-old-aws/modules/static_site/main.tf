locals {
  s3_origin_id = "s3_origin_${var.url}"
}

# Pull in outputs from core-infrastructure/cloudfront to use shared origin access identity
# (see CloudFront distribution below)
data "terraform_remote_state" "cloudfront" {
  count   = var.enabled == "true" ? 1 : 0
  backend = "gcs"

  config = {
    bucket = "danger-farms-tf-state"
    prefix = "cloudfront"
  }
}

# Enable Cloudfront to read the objects from the private bucket by creating an access policy.
# This is applied using the aws_s3_bucket_policy.bucket_policy resource.
# https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-restricting-access-to-s3.html
data "aws_iam_policy_document" "policy_document" {
  count = var.enabled == "true" ? 1 : 0
  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.bucket[0].arn]

    principals {
      # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
      # force an interpolation expression to be interpreted as a list by wrapping it
      # in an extra set of list brackets. That form was supported for compatibilty in
      # v0.11, but is no longer supported in Terraform v0.12.
      #
      # If the expression in the following list itself returns a list, remove the
      # brackets to avoid interpretation as a list of lists. If the expression
      # returns a single list item then leave it as-is and remove this TODO comment.
      identifiers = [data.terraform_remote_state.cloudfront[0].outputs.origin_access_identity_iam_arn]
      type        = "AWS"
    }
  }

  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.bucket[0].arn}/*"]

    principals {
      # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
      # force an interpolation expression to be interpreted as a list by wrapping it
      # in an extra set of list brackets. That form was supported for compatibilty in
      # v0.11, but is no longer supported in Terraform v0.12.
      #
      # If the expression in the following list itself returns a list, remove the
      # brackets to avoid interpretation as a list of lists. If the expression
      # returns a single list item then leave it as-is and remove this TODO comment.
      identifiers = [data.terraform_remote_state.cloudfront[0].outputs.origin_access_identity_iam_arn]
      type        = "AWS"
    }
  }
}

# A bucket holds the static files. The bucket is intentionally not configured as a static website
# itself: instead the files will be served securely via CloudFront.
resource "aws_s3_bucket" "bucket" {
  count  = var.enabled == "true" ? 1 : 0
  acl    = "private"
  bucket = var.url
  tags   = var.tags
}

resource "aws_cloudfront_distribution" "cloudfront" {
  count               = var.enabled == "true" ? 1 : 0
  aliases             = [var.url]
  default_root_object = "index.html"
  enabled             = true
  is_ipv6_enabled     = true
  tags                = var.tags

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    # Redirecting the viewer to HTTPS also forces an HTTPS connection between CloudFront + S3,
    # thus ensuring full SSL
    # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-https-cloudfront-to-s3-origin.html
    viewer_protocol_policy = "redirect-to-https"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  origin {
    domain_name = aws_s3_bucket.bucket[0].bucket_domain_name
    origin_id   = local.s3_origin_id

    s3_origin_config {
      origin_access_identity = data.terraform_remote_state.cloudfront[0].outputs.origin_access_identity_path
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    # TODO: remove hardcoded ARN by defining the certificate in core-infrastructure.
    acm_certificate_arn      = "arn:aws:acm:us-east-1:875959276663:certificate/abd97b2c-db33-4179-8c77-6f707804a0e7"
    minimum_protocol_version = "TLSv1"
    ssl_support_method       = "sni-only"
  }

  custom_error_response {
    error_caching_min_ttl = 0
    error_code            = 404
    response_code         = 200
    response_page_path    = "/index.html"
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  count  = var.enabled == "true" ? 1 : 0
  bucket = aws_s3_bucket.bucket[0].id
  policy = data.aws_iam_policy_document.policy_document[0].json
}

# Create a DNS record pointing the desired URL to the CloudFront distribution
resource "cloudflare_record" "dns" {
  count  = var.enabled == "true" ? 1 : 0
  domain = var.root_domain
  name   = var.url
  type   = "CNAME"
  value  = aws_cloudfront_distribution.cloudfront[0].domain_name
}
