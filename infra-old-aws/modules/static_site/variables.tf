variable "tags" {
  type        = map(string)
  description = "Set of AWS tags applied to all resources."
}

variable "root_domain" {
  type        = string
  description = "The root domain for the static site. For example: dangerfarms.com"
}

variable "url" {
  type        = string
  description = "The full URL of the static site including subdomain. For example: perks.dangerfarms.com"
}

variable "enabled" {
  type        = string
  description = "'true' if the site should be included in the configuration"
  default     = "true"
}
