output "app_bucket_name" {
  value = module.app.bucket_name
}

output "storybook_bucket_name" {
  value = module.storybook.bucket_name
}
