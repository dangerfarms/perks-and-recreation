variable "api_url" {
  type  = string
  description = "The URL for the API backend"
}

variable "frontend_url" {
  type        = string
  description = "The URL for the frontend app (ie. the entry point from a user perspective)."
}

variable "root_domain" {
  type        = string
  description = "The root domain for all URLs. For example: dangerfarms.com"
}

variable "storybook_url" {
  type        = string
  description = "The URL to deploy Storybook at."
}
