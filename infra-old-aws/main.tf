provider "google" {
  project = "danger-farms"
  region  = "europe-west1"
  zone    = "europe-west1"
}

provider "aws" {
  region = "us-east-1"
}

provider "cloudflare" {
  # Credentials pulled from CLOUDFLARE_EMAIL and CLOUDFLARE_TOKEN environment variables
}

terraform {
  backend "gcs" {
    bucket = "danger-farms-tf-state"
    prefix = "perks"
  }
}

locals {
  default_tags = {
    df-project          = "perks"
    df-deployment-stage = terraform.workspace == "default" ? "prod" : terraform.workspace
  }
}

module "app" {
  source = "./modules/static_site"

  tags        = local.default_tags
  root_domain = var.root_domain
  url         = var.frontend_url
}

module "storybook" {
  source = "./modules/static_site"

  tags        = local.default_tags
  root_domain = var.root_domain
  url         = var.storybook_url
  enabled     = terraform.workspace == "e2etest" ? "false" : "true"
}

resource "aws_api_gateway_domain_name" "api_gateway_custom_domain" {
  # TODO: remove hardcoded ARN by defining the certificate in core-infrastructure.
  certificate_arn = "arn:aws:acm:us-east-1:875959276663:certificate/abd97b2c-db33-4179-8c77-6f707804a0e7"
  domain_name     = var.api_url
}

resource "cloudflare_record" "api_dns" {
  domain  = "dangerfarms.com"
  name    = var.api_url
  type    = "CNAME"
  value   = aws_api_gateway_domain_name.api_gateway_custom_domain.cloudfront_domain_name
}
