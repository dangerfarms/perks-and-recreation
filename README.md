## Getting started: development

Prerequisites:

* Docker (to run postgres)
* docker-compose
* python 3.7.5
* Node
* yarn
* make

### Local development

Start by reading through the `Makefile`. 

All targets have comments to explain what each target does.

If you want a reminder later, you can run `make` or `make help` to list all targets.

You can run `make setup` to execute all setup required for local development. 

> **Note:** even after running `make setup-dev`, you'll need to populate environment variables + secrets manually.

### Frontend environment variables

We use [Create React App's out-of-the-box management of environment variables](https://create-react-app.dev/docs/adding-custom-environment-variables/). Use an `.env.local` file for local development (since env files with `.local` are gitignored).

These environment variables are required:

* `REACT_APP_GRAPHQL_ENDPOINT` is the URL to the GraphQL endpoint in the backend (ie. `/graphql`). **You probably want to use `http://localhost:8000/graphql` for local dev.**

### Backend environment variables

Create these in `be/src/.env`, based on the `.env.template` file. The default values are mostly fine for local dev, with these exceptions:

* `MAILGUN_API_KEY`: retrieve this from company Mailgun account, or steal from CI variables.
* `MAILGUN_SENDER_DOMAIN`: as above (in thoery, should be able to use sandbox domain, but you may have to add your email as an authorized recipient).

### E2E tests

#### BDD

We're using `cypress-cucumber-preprocessor` to allow E2E tests to be written using Cucumber.

Tests should be added to the `cypress/integration` folder and consist of
* A `<FeatureName>.feature` file containing the tests in Cucumber syntax
* A `<FeatureName>` folder containing a JS file (or multiple files) where the steps in `<FeatureName>.feature` are defined

Shared step definitions can be added to `cypress/integration/common`.

You can find more detailed explanations of the above in the preprocessor's [documentation](https://github.com/TheBrainFamily/cypress-cucumber-preprocessor#readme).

#### Running the tests

Before running E2E tests locally, you'll need to create a `cypress.json` file in the `fe` folder based on `cypress.template.json`.

* `baseUrl` is the url of the site, so `http://localhost:3000` to run against your development server.
* `env.E2E_JWT` is the JWT used in requests to the backend to let it know that the request is from Cypress. It can be found in the settings for CI in GitLab.

Make sure that `blacklistHosts` keeps its value from the template file. We need to block the download of the Google auth API so that we can mock it to bypass auth during tests.

In `fe`, run `yarn cypress open` to open the Cypress UI or `yarn cy:run` to run all tests in a headless browser.

## Deployment

### Prerequisites

You will need to save a service account key locally in the project root with the name `perks-service-account.json`.
This is a git-ignored file, make sure you get the name right, and you don't commit your secret key to the repo.

You also need the following software:
- gcloud (272.0.0)
- terraform (v0.12.16 or above)
- python 3.7.5
- jq (https://stedolan.github.io/jq/ mainly for infra stuff, so if you don't touch that, you won't need it)

You will need to create and download a key.json file from the Google Cloud Console IAM section, from a service account which has the following roles:
- (Project /) Editor
- Cloud Run / Admin

The service account address must also be added to the domain owners manually for now:
```shell script
gcloud domains verify "*.dangerfarms.com"
# manually add the service account email after you verified as an owner yourself.
```

If you're starting from scratch (not using the existing remote state for terraform in bucket `danger-farms-tf-state`, prefix `perks`), you will need to create two workspaces:
```
cd infra
terraform workspace new dev
terraform workspace new prod
```
