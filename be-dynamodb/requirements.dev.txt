-r requirements.txt

black
docker
flake8
freezegun
mock
pre-commit-hooks
pytest
