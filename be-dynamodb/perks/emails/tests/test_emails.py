import os
from unittest import TestCase

from mock import patch, Mock

from perks.emails import send_created_proposal_email, send_approved_proposal_email, send_rejected_proposal_email
from perks.mocks import mock_environment, MockMailgunResponse
from perks.utils.factories import ProposalFactory, ApprovedProposalFactory, RejectedProposalFactory


class EmailsBaseTestCase(TestCase):
    def setUp(self):
        self.env_patch = patch.dict(os.environ, mock_environment)
        self.env_patch.start()
        self.get_users_patcher = patch("perks.emails.get_users")
        self.mock_get_users = self.get_users_patcher.start()
        self.email_patcher = patch("perks.emails.requests.post", return_value=MockMailgunResponse())
        self.mock_send_email = self.email_patcher.start()

    def tearDown(self):
        self.env_patch.stop()
        self.get_users_patcher.stop()
        self.mock_send_email.stop()


class SendNewProposalEmailTestCase(EmailsBaseTestCase):
    @patch(
        "perks.emails.get_users",
        Mock(
            return_value=[
                {"email": "tom@dangerfarms.com", "is_admin": False},
                {"email": "max@dangerfarms.com", "is_admin": False},
            ]
        ),
    )
    @patch.dict(os.environ, {**mock_environment, "STAGE": "prod"})
    def test_should_send_created_proposal_email_to_all_users_in_production(self):
        proposal = ProposalFactory.build(name="Fruit bowl")

        send_created_proposal_email(proposal)

        self.mock_send_email.assert_called_once_with(
            "https://api.mailgun.net/v3/mailgun.dangerfarms.com/messages",
            auth=("api", "test-mailgun-key"),
            data={
                "from": "perks@dangerfarms.com",
                "to": "tom@dangerfarms.com,max@dangerfarms.com",
                "subject": "There's a new proposal to vote on!",
                "text": "A colleague has proposed a new perk: Fruit bowl.\n\nEnsure your opinion is heard at perks.dangerfarms.com",
            },
        )

    @patch(
        "perks.emails.get_users",
        Mock(
            return_value=[
                {"email": "tom@dangerfarms.com", "is_admin": False},
                {"email": "max@dangerfarms.com", "is_admin": False},
            ]
        ),
    )
    @patch.dict(os.environ, {**mock_environment, "STAGE": "dev"})
    def test_should_send_created_proposal_email_to_developers_in_non_production(self):
        proposal = ProposalFactory.build(name="Fruit bowl")

        send_created_proposal_email(proposal)

        self.mock_send_email.assert_called_once_with(
            "https://api.mailgun.net/v3/mailgun.dangerfarms.com/messages",
            auth=("api", "test-mailgun-key"),
            data={
                "from": "perks@dangerfarms.com",
                "to": "perks-developers@dangerfarms.com",
                "subject": "There's a new proposal to vote on!",
                "text": "A colleague has proposed a new perk: Fruit bowl.\n\nEnsure your opinion is heard at perks.dangerfarms.com",
            },
        )

    def test_should_not_send_if_no_proposal(self):
        send_created_proposal_email(None)
        self.mock_send_email.assert_not_called()


class SendApprovedProposalEmailTestCase(EmailsBaseTestCase):
    def test_should_not_send_approval_email_if_no_proposal(self):
        send_created_proposal_email(None)
        self.mock_send_email.assert_not_called()

    @patch(
        "perks.emails.get_users",
        Mock(
            return_value=[
                {"email": "tom@dangerfarms.com", "is_admin": False},
                {"email": "max@dangerfarms.com", "is_admin": False},
            ]
        ),
    )
    @patch.dict(os.environ, {**mock_environment, "STAGE": "dev"})
    def test_should_send_approval_email_to_developers_in_non_production(self):
        proposal = ApprovedProposalFactory.build(name="Fruit bowl", owner="tom@dangerfarms.com")

        send_approved_proposal_email(proposal)

        self.mock_send_email.assert_called_once_with(
            "https://api.mailgun.net/v3/mailgun.dangerfarms.com/messages",
            auth=("api", "test-mailgun-key"),
            data={
                "from": "perks@dangerfarms.com",
                "to": "perks-developers@dangerfarms.com",
                "subject": "A proposal has been approved!",
                "text": "A proposal from tom@dangerfarms.com has been approved: Fruit bowl.\n\nCongratulations! Your proposal shall be purchased soon, check at perks.dangerfarms.com",
            },
        )

    @patch(
        "perks.emails.get_users",
        Mock(
            return_value=[
                {"email": "owen@dangerfarms.com", "is_admin": False},
                {"email": "lewis@dangerfarms.com", "is_admin": True},
            ]
        ),
    )
    @patch.dict(os.environ, {**mock_environment, "STAGE": "prod"})
    def test_should_send_approval_email_to_all_users_in_production(self):
        proposal = ApprovedProposalFactory.build(name="A test proposal", owner="craig@dangerfarms.com")

        send_approved_proposal_email(proposal)

        self.mock_send_email.assert_called_once_with(
            "https://api.mailgun.net/v3/mailgun.dangerfarms.com/messages",
            auth=("api", "test-mailgun-key"),
            data={
                "from": "perks@dangerfarms.com",
                "to": "owen@dangerfarms.com,lewis@dangerfarms.com",
                "subject": "A proposal has been approved!",
                "text": "A proposal from craig@dangerfarms.com has been approved: A test proposal.\n\nCongratulations! Your proposal shall be purchased soon, check at perks.dangerfarms.com",
            },
        )


class SendRejectedProposalEmailTestCase(EmailsBaseTestCase):
    def test_should_not_send_rejection_email_if_no_proposal(self):
        send_rejected_proposal_email(None)
        self.mock_send_email.assert_not_called()

    @patch(
        "perks.emails.get_users",
        Mock(
            return_value=[
                {"email": "tom@dangerfarms.com", "is_admin": False},
                {"email": "max@dangerfarms.com", "is_admin": False},
            ]
        ),
    )
    @patch.dict(os.environ, {**mock_environment, "STAGE": "dev"})
    def test_should_send_rejection_email_to_developers_in_non_production(self):
        proposal = RejectedProposalFactory.build(name="Fruit bowl", owner="tom@dangerfarms.com")

        send_rejected_proposal_email(proposal)

        self.mock_send_email.assert_called_once_with(
            "https://api.mailgun.net/v3/mailgun.dangerfarms.com/messages",
            auth=("api", "test-mailgun-key"),
            data={
                "from": "perks@dangerfarms.com",
                "to": "perks-developers@dangerfarms.com",
                "subject": "Your proposal has been rejected",
                "text": "A proposal from tom@dangerfarms.com has been rejected: Fruit bowl.\n\nMaybe next time...",
            },
        )

    @patch(
        "perks.emails.get_users",
        Mock(
            return_value=[
                {"email": "tom@dangerfarms.com", "is_admin": True},
                {"email": "lewis@dangerfarms.com", "is_admin": True},
                {"email": "max@dangerfarms.com", "is_admin": False},
                {"email": "daniel@dangerfarms.com", "is_admin": False},
                {"email": "balint@dangerfarms.com", "is_admin": False},
            ]
        ),
    )
    @patch.dict(os.environ, {**mock_environment, "STAGE": "prod"})
    def test_should_only_send_rejection_email_to_owner_and_admins_in_production(self):
        proposal = RejectedProposalFactory.build(name="Fruit bowl", owner="max@dangerfarms.com")

        send_rejected_proposal_email(proposal)

        self.mock_send_email.assert_called_once_with(
            "https://api.mailgun.net/v3/mailgun.dangerfarms.com/messages",
            auth=("api", "test-mailgun-key"),
            data={
                "from": "perks@dangerfarms.com",
                "to": "tom@dangerfarms.com,lewis@dangerfarms.com,max@dangerfarms.com",
                "subject": "Your proposal has been rejected",
                "text": "A proposal from max@dangerfarms.com has been rejected: Fruit bowl.\n\nMaybe next time...",
            },
        )
