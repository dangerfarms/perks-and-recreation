from perks.utils.constants import days_until_proposal_expiry


def proposal_creation_template(proposal):
    return f"A colleague has proposed a new perk: {proposal['name']}.\n\nEnsure your opinion is heard at perks.dangerfarms.com"


def proposal_approval_template(proposal):
    return f"A proposal from {proposal['owner']} has been approved: {proposal['name']}.\n\nCongratulations! Your proposal shall be purchased soon, check at perks.dangerfarms.com"


def proposal_rejection_template(proposal):
    return f"A proposal from {proposal['owner']} has been rejected: {proposal['name']}.\n\nMaybe next time..."


def proposal_expiry_template(proposal, num_of_users):
    return f"A proposal from {proposal['owner']} has expired: {proposal['name']}.\n\nThe expiry period of {days_until_proposal_expiry} days elapsed with only {proposal['vote_count']} out of {num_of_users} users having voted."
