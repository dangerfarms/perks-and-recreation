import os

import requests

from perks.data.users import get_users
from perks.emails.templates import (
    proposal_creation_template,
    proposal_approval_template,
    proposal_rejection_template,
    proposal_expiry_template,
)
from perks.utils.logging import get_logger

logger = get_logger(__name__)

FROM_ADDRESS = "perks@dangerfarms.com"


def send_created_proposal_email(proposal):
    if not proposal:
        return

    subject = "There's a new proposal to vote on!"
    body = proposal_creation_template(proposal)
    recipients = [user["email"] for user in get_users()]

    return send_email(recipients, subject, body)


def send_approved_proposal_email(proposal):
    if not proposal:
        return

    subject = "A proposal has been approved!"
    body = proposal_approval_template(proposal)
    recipients = [user["email"] for user in get_users()]

    return send_email(recipients, subject, body)


def send_rejected_proposal_email(proposal):
    if not proposal:
        return

    subject = "Your proposal has been rejected"
    body = proposal_rejection_template(proposal)
    recipients = [user["email"] for user in get_users() if user["is_admin"] or user["email"] == proposal["owner"]]

    return send_email(recipients, subject, body)


def send_expired_proposal_email(proposal):
    if not proposal:
        return

    users = get_users()
    subject = "Your proposal has expired"
    body = proposal_expiry_template(proposal, len(users))
    recipients = [user["email"] for user in users if user["is_admin"] or user["email"] == proposal["owner"]]

    return send_email(recipients, subject, body)


def send_email(recipients, subject, body):
    api_key = os.getenv("MAILGUN_API_KEY")
    domain = "mailgun.dangerfarms.com"
    request_url = f"https://api.mailgun.net/v3/{domain}/messages"

    if not api_key:
        raise ValueError("API key not provided")

    if os.getenv("STAGE") != "prod":
        send_to = "perks-developers@dangerfarms.com"
    else:
        send_to = ",".join([email for email in recipients])

    logger.debug("About to send email", {"to": send_to, "subject": subject, "body": body})

    response = requests.post(
        request_url, auth=("api", api_key), data={"from": FROM_ADDRESS, "to": send_to, "subject": subject, "text": body}
    )

    logger.debug(
        "Mailgun response", {"mailgun_status_code": response.status_code, "mailgun_response_body": response.text}
    )

    return response
