import graphene
from graphene.types.resolver import dict_resolver

from perks.data.budget import get_budget, set_budget
from perks.data.threshold import get_threshold, set_threshold
from perks.data.users import get_users, create_user, get_user
from perks.data.proposals import (
    process_proposals,
    get_proposal,
    add_vote_to_proposal,
    get_proposals_user_needs_to_vote_on,
    list_proposals,
    create_proposal,
    edit_proposal,
    delete_proposal,
    purchase_proposal,
    get_proposals_by_owner,
    get_proposals_by_status,
)
from perks.data.votes import remove_pending_vote
from perks.utils import get_current_year
from perks.utils.logging import get_logger
from perks.utils.constants import ProposalStatuses as ProposalStatusConstants, SortOptions as SortOptionsConstants

logger = get_logger(__name__)


class Budget(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    partition_key = graphene.String()
    sort_key = graphene.String()
    total_budget = graphene.Int()
    remaining_budget = graphene.Int()


class Threshold(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    partition_key = graphene.String()
    sort_key = graphene.String()
    threshold = graphene.Int()


class ProposalStatuses(graphene.Enum):
    APPROVED = "APPROVED"
    OPEN = "OPEN"
    PURCHASED = "PURCHASED"
    REJECTED = "REJECTED"


class ProposalVote(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    user = graphene.String()
    accept = graphene.Boolean()


class Proposal(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    partition_key = graphene.String()
    id = graphene.String()
    created_at = graphene.String()
    name = graphene.String()
    description = graphene.String()
    estimated_cost = graphene.Int()
    is_deleted = graphene.Boolean()
    first_billing_date = graphene.String()
    is_recurring = graphene.Boolean()
    owner = graphene.String()
    status = ProposalStatuses()
    threshold = graphene.Int()
    vote_count = graphene.Int()
    votes = graphene.List(ProposalVote)


class Cursor(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    current = graphene.Int()
    final = graphene.Int()


class PaginatedProposals(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    proposals = graphene.List(Proposal)
    cursor = graphene.Field(Cursor)
    previousFilter = graphene.String()


class User(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    email = graphene.String()
    is_admin = graphene.Boolean()


class Vote(graphene.Mutation):
    class Arguments:
        proposal = graphene.String(required=True)
        accept = graphene.Boolean(required=True)

    accept = graphene.Boolean()
    status = ProposalStatuses()
    vote_count = graphene.Int()
    votes = graphene.List(ProposalVote)

    @staticmethod
    def mutate(self, info, proposal, accept):
        proposal_data = get_proposal(proposal)
        if not proposal_data:
            raise ValueError("Cannot vote on a proposal that does not exist")

        user = info.context["authenticated_user"]["email"]
        remove_pending_vote(proposal, user)
        result = add_vote_to_proposal(proposal, user, accept)
        return Vote(
            accept=result["accept"], status=result["status"], vote_count=result["vote_count"], votes=result["votes"]
        )


SortOptions = graphene.Enum.from_enum(SortOptionsConstants)


class Query(graphene.ObjectType):
    me = graphene.Field(User)
    budget = graphene.Field(
        Budget, year=graphene.Argument(type=graphene.String, default_value=get_current_year(), required=False)
    )
    threshold = graphene.Field(Threshold)
    proposals = graphene.List(
        Proposal,
        filter=graphene.Argument(type=graphene.String, default_value="all", required=False),
        sort=graphene.Argument(type=SortOptions, default_value=SortOptions.DATE_ASCENDING, required=False),
    )
    paginated_proposals = graphene.Field(
        PaginatedProposals,
        filter=graphene.Argument(type=graphene.String, default_value="all", required=False),
        cursor=graphene.Argument(type=graphene.Int, default_value=0, required=False),
        sort=graphene.Argument(type=SortOptions, default_value=SortOptions.DATE_ASCENDING, required=False),
    )
    users = graphene.List(User)

    @staticmethod
    def resolve_me(self, info):
        user_email = info.context.get("authenticated_user", {}).get("email")
        return get_user(user_email)

    @staticmethod
    def resolve_budget(self, info, year):
        return get_budget(year)

    @staticmethod
    def resolve_threshold(self, info):
        return get_threshold()

    @staticmethod
    def resolve_users(self, info):
        return get_users()

    @staticmethod
    def resolve_proposals(self, info, filter, sort=SortOptions.DATE_ASCENDING):
        user_id = info.context["authenticated_user"]["email"]
        if filter == "needs_my_vote":
            proposals = get_proposals_user_needs_to_vote_on(user_id, sort)
        elif filter == "approved":
            proposals = get_proposals_by_status(ProposalStatusConstants.APPROVED, sort)
        elif filter == "mine":
            proposals = get_proposals_by_owner(user_id, sort)
        elif filter == "purchased":
            proposals = get_proposals_by_status(ProposalStatusConstants.PURCHASED, sort)
        else:
            proposals = list_proposals(user_id, sort)
        return process_proposals(proposals, user_id)

    @staticmethod
    def resolve_paginated_proposals(self, info, filter, sort, cursor):
        proposals = Query.resolve_proposals(self, info, filter, sort)
        pagination_limit = 10
        final_cursor = len(proposals)
        paginated_proposals = []
        next_cursor = cursor + pagination_limit if cursor + pagination_limit <= final_cursor else final_cursor
        if cursor < next_cursor:
            try:
                paginated_proposals = proposals[cursor:next_cursor]
            except IndexError as err:
                raise Exception(f"The pagination cursor is at an invalid position: {err}")
        return {"proposals": paginated_proposals, "cursor": {"current": next_cursor, "final": final_cursor}}


class ChangeAdminSettings(graphene.Mutation):
    class Arguments:
        total_budget = graphene.Int()
        threshold = graphene.Int()

    budget = graphene.Field(Budget)
    threshold = graphene.Field(Threshold)

    @staticmethod
    def mutate(self, info, **kwargs):
        user_id = info.context["authenticated_user"]["email"]
        user = get_user(user_id)

        if not user["is_admin"]:
            raise Exception("Only admins may set a new budget or alter an existing one.")

        year = get_current_year()
        current_budget = get_budget(year)
        new_total_budget = kwargs.get("total_budget", None)

        if not new_total_budget or (current_budget and current_budget.get("total_budget") == new_total_budget):
            budget = current_budget or set_budget(0, 0, year)
        else:
            if new_total_budget < 0:
                raise Exception("Budget must be a whole number greater than or equal to zero.")

            remaining_budget = new_total_budget
            if current_budget:
                current_remaining_budget = current_budget.get("remaining_budget", remaining_budget)
                if current_remaining_budget <= remaining_budget:
                    remaining_budget = current_remaining_budget

            budget = set_budget(new_total_budget, remaining_budget, year)
            budget["year"] = budget.pop("sort_key")  # TODO This can be done better.

        current_threshold = get_threshold()
        new_threshold = kwargs.get("threshold", None)

        if not new_threshold or (current_threshold and current_threshold.get("threshold") == new_threshold):
            threshold = current_threshold or set_threshold(50)
        else:
            if new_threshold < 0:
                raise Exception("Approval threshold percentage must be a whole number greater than or equal to zero.")

            threshold = set_threshold(new_threshold)

        return ChangeAdminSettings(budget=budget, threshold=threshold)


class CreateUser(graphene.Mutation):
    user = graphene.Field(User)

    @staticmethod
    def mutate(self, info):
        user_id = info.context["authenticated_user"]["email"]
        existing_user = get_user(user_id)
        if existing_user:
            raise Exception("This email address is already associated with a user account.")

        is_admin = False
        existing_users = get_users()
        if not len(existing_users):
            is_admin = True

        user = create_user(user_id, is_admin)
        return CreateUser(user=user)


IS_RECURRING_DEFAULT = False
FIRST_BILLING_DATE_DEFAULT = None


class CreateProposal(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)
        estimated_cost = graphene.Int(required=True)
        description = graphene.String(default_value="")
        is_recurring = graphene.Boolean(default_value=IS_RECURRING_DEFAULT)
        first_billing_date = graphene.String(default_value=FIRST_BILLING_DATE_DEFAULT)

    ok = graphene.Boolean()
    proposal = graphene.Field(Proposal)

    @staticmethod
    def mutate(
        self,
        info,
        name,
        estimated_cost,
        description,
        is_recurring=IS_RECURRING_DEFAULT,
        first_billing_date=FIRST_BILLING_DATE_DEFAULT,
    ):
        if estimated_cost < 0:
            raise Exception("Estimated cost must be a whole number greater than zero.")

        user_id = info.context["authenticated_user"]["email"]
        proposal = create_proposal(user_id, name, estimated_cost, description, is_recurring, first_billing_date)
        proposal["id"] = proposal.pop("sort_key")  # TODO This can be done better.
        return CreateProposal(ok=True, proposal=proposal)


class EditProposal(graphene.Mutation):
    class Arguments:
        proposal = graphene.String(required=True)
        name = graphene.String()
        estimated_cost = graphene.Int()
        description = graphene.String()

    ok = graphene.Boolean()
    proposal = graphene.Field(Proposal)

    @staticmethod
    def mutate(self, info, proposal, name=None, estimated_cost=None, description=None):
        user_id = info.context["authenticated_user"]["email"]
        edited_proposal = edit_proposal(user_id, proposal, name, estimated_cost, description)
        edited_proposal["id"] = edited_proposal.pop("sort_key")
        return EditProposal(ok=True, proposal=edited_proposal)


class DeleteProposal(graphene.Mutation):
    class Arguments:
        proposal = graphene.String(required=True)

    ok = graphene.Boolean()

    @staticmethod
    def mutate(self, info, proposal):
        user_id = info.context["authenticated_user"]["email"]
        delete_proposal(user_id, proposal)
        return DeleteProposal(ok=True)


class PurchaseProposal(graphene.Mutation):
    class Arguments:
        cost = graphene.Int(required=True)
        proposal_id = graphene.String(required=True)

    proposal = graphene.Field(Proposal)
    budget = graphene.Field(Budget)

    @staticmethod
    def mutate(self, info, proposal_id, cost):
        proposal = get_proposal(proposal_id)

        if proposal["status"] != ProposalStatusConstants.APPROVED:
            raise Exception("Only proposals with 'APPROVED' status can be changed to 'PURCHASED' status")

        year = get_current_year()
        budget = get_budget(year)

        if cost > budget["remaining_budget"]:
            print("Cost : " + cost)
            print("Remaining Budget : " + budget["remaining_budget"])
            raise Exception(
                "Estimated cost of this proposal exceeds the remaining budget, so the proposal cannot be purchased this year unless the budget is raised."
            )

        updated_budget = set_budget(budget["total_budget"], budget["remaining_budget"] - cost, year)

        updated_proposal = purchase_proposal(proposal)
        updated_proposal["id"] = updated_proposal.pop("sort_key")  # TODO This can be done better.

        return PurchaseProposal(proposal=updated_proposal, budget=updated_budget)


class Mutations(graphene.ObjectType):
    change_admin_settings = ChangeAdminSettings.Field()
    create_user = CreateUser.Field()
    create_proposal = CreateProposal.Field()
    edit_proposal = EditProposal.Field()
    delete_proposal = DeleteProposal.Field()
    purchase_proposal = PurchaseProposal.Field()
    vote = Vote.Field()


schema = graphene.Schema(query=Query, mutation=Mutations)
