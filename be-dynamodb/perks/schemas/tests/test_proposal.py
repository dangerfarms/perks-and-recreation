from unittest import TestCase

from graphene.test import Client
from mock import patch

from perks.schemas import schema
from perks.utils.factories import ProposalFactory


class PutProposalTestCase(TestCase):
    def setUp(self):
        self.client = Client(schema=schema)

    @patch("perks.schemas.create_proposal")
    def test_should_create_proposal_with_provided_variables(self, mock_create_proposal):
        resulting_proposal = ProposalFactory.build(
            name="New Proposal", description="New Proposal Description", estimated_cost=1_000
        )

        mock_create_proposal.return_value = resulting_proposal

        result = self.client.execute(
            """
            mutation CreateProposal($name: String!, $estimated_cost: Int!, $description: String) {
                createProposal(name: $name, estimatedCost: $estimated_cost, description: $description) {
                    proposal {
                        name
                        description
                        estimatedCost
                    }
                    ok
                }
            }
            """,
            variables={"name": "New Proposal", "description": "New Proposal Description", "estimated_cost": 1_000},
            context={"authenticated_user": {"email": "craig@dangerfarms.com"}},
        )

        mock_create_proposal.assert_called_once_with(
            "craig@dangerfarms.com", "New Proposal", 1_000, "New Proposal Description", False, None
        )

        self.assertTrue(result["data"]["createProposal"]["ok"])
        proposal_result = result["data"]["createProposal"]["proposal"]
        self.assertEqual(proposal_result["name"], "New Proposal")
        self.assertEqual(proposal_result["description"], "New Proposal Description")
        self.assertEqual(proposal_result["estimatedCost"], 1_000)

    @patch("perks.schemas.create_proposal")
    def test_should_not_create_proposal_when_estimated_cost_is_negative(self, mock_create_proposal):
        resulting_proposal = ProposalFactory.build(
            name="New Proposal", description="New Proposal Description", estimated_cost=-1_000
        )

        mock_create_proposal.return_value = resulting_proposal

        result = self.client.execute(
            """
            mutation CreateProposal($name: String!, $estimated_cost: Int!, $description: String) {
                createProposal(name: $name, estimatedCost: $estimated_cost, description: $description) {
                    proposal {
                        name
                    }
                    ok
                }
            }
            """,
            variables={"name": "New Proposal", "description": "New Proposal Description", "estimated_cost": -1_000},
            context={"authenticated_user": {"email": "craig@dangerfarms.com"}},
        )

        mock_create_proposal.assert_not_called()
        self.assertIsNone(result["data"]["createProposal"])
        self.assertEqual(result["errors"][0]["message"], "Estimated cost must be a whole number greater than zero.")
