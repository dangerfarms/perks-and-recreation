from unittest import TestCase

from graphene.test import Client
from mock import patch, Mock

from perks.schemas import schema
from perks.utils.factories import ProposalFactory, VoteFactory


class PutVoteTestCase(TestCase):
    def setUp(self):
        self.client = Client(schema=schema)

    @patch("perks.schemas.remove_pending_vote", Mock())
    @patch("perks.schemas.get_proposal")
    @patch("perks.schemas.add_vote_to_proposal")
    def test_should_return_false_accept_value_when_voting_against(self, mock_add_vote, mock_get_proposal):
        vote = VoteFactory(accept=False)
        proposal = ProposalFactory(votes=[vote])
        mock_add_vote.return_value = {
            "accept": False,
            "status": proposal["status"],
            "vote_count": proposal["vote_count"],
            "votes": proposal["votes"],
        }

        mock_get_proposal.return_value = proposal

        result = self.client.execute(
            """
                mutation Vote($proposal: String!, $accept: Boolean!) {
                    vote(proposal: $proposal, accept: $accept) {
                        accept
                    }
                }
            """,
            variables={"proposal": proposal["sort_key"], "accept": False},
            context={"authenticated_user": {"email": "craig@dangerfarms.com"}},
        )

        self.assertFalse(result["data"]["vote"]["accept"])

    @patch("perks.schemas.remove_pending_vote", Mock())
    @patch("perks.schemas.get_proposal")
    @patch("perks.schemas.add_vote_to_proposal")
    def test_should_return_true_accept_value_when_voting_for(self, mock_add_vote, mock_get_proposal):
        vote = VoteFactory(accept=True)
        proposal = ProposalFactory(votes=[vote])
        mock_add_vote.return_value = {
            "accept": True,
            "status": proposal["status"],
            "vote_count": proposal["vote_count"],
            "votes": proposal["votes"],
        }
        mock_get_proposal.return_value = proposal

        result = self.client.execute(
            """
                mutation Vote($proposal: String!, $accept: Boolean!) {
                    vote(proposal: $proposal, accept: $accept) {
                        accept
                    }
                }
            """,
            variables={"proposal": proposal["sort_key"], "accept": True},
            context={"authenticated_user": {"email": "craig@dangerfarms.com"}},
        )

        self.assertTrue(result["data"]["vote"]["accept"])

    @patch("perks.schemas.remove_pending_vote", Mock())
    @patch("perks.schemas.get_proposal")
    @patch("perks.schemas.add_vote_to_proposal")
    def test_should_return_accept_value_of_newest_vote(self, mock_add_vote, mock_get_proposal):
        existing_vote = VoteFactory(accept=True)
        new_vote = VoteFactory(accept=False)
        proposal = ProposalFactory(votes=[existing_vote, new_vote])
        mock_add_vote.return_value = {
            "accept": False,
            "status": proposal["status"],
            "vote_count": proposal["vote_count"],
            "votes": proposal["votes"],
        }
        mock_get_proposal.return_value = proposal

        result = self.client.execute(
            """
                mutation Vote($proposal: String!, $accept: Boolean!) {
                    vote(proposal: $proposal, accept: $accept) {
                        accept
                    }
                }
            """,
            variables={"proposal": proposal["sort_key"], "accept": False},
            context={"authenticated_user": {"email": "craig@dangerfarms.com"}},
        )

        self.assertFalse(result["data"]["vote"]["accept"])
