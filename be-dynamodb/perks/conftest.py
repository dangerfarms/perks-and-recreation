import os

import docker
import pytest


@pytest.fixture(scope="session", autouse=True)
def dynamodb_local(request):
    # Set dummy AWS creds to allow boto3 to initialize, without granting access to real AWS provisioned services.
    #
    # Note that the dummy creds intentionally override any real creds set in the current shell, including any
    # AWS_PROFILE that's been set. We don't want "integration" tests to make calls to AWS. Any credentials errors
    # you come across while writing integration tests should be solved be either a) providing a locally runnable
    # instance of the service (like what we do with DynamoDB), or b) mocking the 3rd party call (eg. using moto).
    os.environ["AWS_ACCESS_KEY_ID"] = "test"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "test"

    # Only run if not in CI, as in that case a service is already provisioned
    if not os.getenv("CI_JOB_ID"):
        print("\nStarting DynamoDB Local container...")

        # Use DynamoDB Local container (set up below)
        os.environ["DYNAMODB_ENDPOINT_URL"] = "http://localhost:8000"

        docker_client = docker.from_env()
        db_container = docker_client.containers.run("amazon/dynamodb-local", ports={"8000/tcp": 8000}, detach=True)

        def tear_down():
            print("\nDestroying DynamoDB Local container...")
            db_container.kill()
            db_container.remove()

        request.addfinalizer(tear_down)
