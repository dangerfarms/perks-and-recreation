from datetime import datetime


def get_current_year():
    return str(datetime.now().year)
