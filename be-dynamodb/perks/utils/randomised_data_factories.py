import factory
import random
import datetime

from perks.utils.factories import ProposalFactory
from perks.utils.constants import ProposalStatuses, list_proposal_statuses, example_users


def populate_proposal_votes(proposal):
    if proposal.status == ProposalStatuses.OPEN:
        return [{"user": example_users[i], "accept": random.choice([True, False])} for i in range(proposal.vote_count)]
    if proposal.status == ProposalStatuses.APPROVED or proposal.status == ProposalStatuses.PURCHASED:
        return [{"user": example_users[i], "accept": False if i % 3 == 0 else True} for i in range(len(example_users))]
    if proposal.status == ProposalStatuses.REJECTED:
        return [{"user": example_users[i], "accept": True if i % 3 == 0 else False} for i in range(len(example_users))]


class RandomisedProposalFactory(ProposalFactory):
    class Meta:
        model = dict

    sort_key = factory.LazyAttribute(lambda proposal: f"PROPOSAL-{proposal.created_at}")
    created_at = factory.LazyFunction(lambda: datetime.datetime.now().isoformat())
    name = factory.Faker("sentence", nb_words=random.randint(2, 7))
    description = factory.Faker("sentence", nb_words=random.randint(10, 30))
    estimated_cost = factory.LazyFunction(lambda: random.randint(10, 1001))
    owner = factory.LazyFunction(lambda: random.choice(example_users))
    status = factory.LazyFunction(lambda: random.choice(list_proposal_statuses()))
    vote_count = factory.LazyAttribute(
        lambda proposal: random.randint(0, len(example_users) - 1) if proposal.status == "OPEN" else len(example_users)
    )
    votes = factory.LazyAttribute(populate_proposal_votes)


class RandomisedOpenProposalFactory(RandomisedProposalFactory):
    class Meta:
        model = dict

    status = ProposalStatuses.OPEN
    vote_count = 0
    votes = []
