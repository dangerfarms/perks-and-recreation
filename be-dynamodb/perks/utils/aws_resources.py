import os

import boto3


def get_dynamodb_client():
    ddb_kwargs = {}
    if os.getenv("DYNAMODB_ENDPOINT_URL"):
        ddb_kwargs = {"endpoint_url": os.getenv("DYNAMODB_ENDPOINT_URL")}
    return boto3.resource("dynamodb", region_name=os.getenv("REGION"), **ddb_kwargs)


def create_proposals_table():
    client = get_dynamodb_client()
    return client.create_table(
        TableName=os.getenv("PROPOSALS_TABLE"),
        KeySchema=[
            {"AttributeName": "partition_key", "KeyType": "HASH"},
            {"AttributeName": "sort_key", "KeyType": "RANGE"},
        ],
        AttributeDefinitions=[
            {"AttributeName": "partition_key", "AttributeType": "S"},
            {"AttributeName": "sort_key", "AttributeType": "S"},
            {"AttributeName": "status", "AttributeType": "S"},
        ],
        ProvisionedThroughput={"ReadCapacityUnits": 1, "WriteCapacityUnits": 1},
        GlobalSecondaryIndexes=[
            {
                "IndexName": "status_index",
                "KeySchema": [
                    {"AttributeName": "status", "KeyType": "HASH"},
                    {"AttributeName": "sort_key", "KeyType": "RANGE"},
                ],
                "Projection": {"ProjectionType": "ALL"},
                "ProvisionedThroughput": {"ReadCapacityUnits": 1, "WriteCapacityUnits": 1},
            }
        ],
    )


def get_proposals_table():
    client = get_dynamodb_client()
    return client.Table(os.getenv("PROPOSALS_TABLE"))
