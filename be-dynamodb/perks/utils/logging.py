from structured_logger import Logger
import os


def get_logger(name):
    log_level = os.getenv("LOG_LEVEL", "DEBUG").upper()
    logger = Logger(name)
    logger.set_level(log_level)
    return logger
