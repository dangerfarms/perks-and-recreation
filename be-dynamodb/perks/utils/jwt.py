import os

import cachecontrol
import google.auth.transport.requests
from google.oauth2 import id_token
import requests

from perks.utils.logging import get_logger

logger = get_logger(__name__)


class JwtError(ValueError):
    pass


def verify_jwt(b64_jwt):
    # Cache requests to Google for their public key
    # https://google-auth.readthedocs.io/en/latest/reference/google.oauth2.id_token.html#module-google.oauth2.id_token
    session = requests.session()
    cached_session = cachecontrol.CacheControl(session)
    request = google.auth.transport.requests.Request(session=cached_session)

    try:
        jwt_claims = id_token.verify_oauth2_token(b64_jwt, request, os.environ["GOOGLE_OAUTH_CLIENT_ID"])
    except ValueError:
        raise JwtError("Invalid authentication")

    if (
        jwt_claims["hd"] != "dangerfarms.com"
        or jwt_claims["aud"] != os.environ["GOOGLE_OAUTH_CLIENT_ID"]
        or jwt_claims["iss"] != "accounts.google.com"
    ):
        raise JwtError("Invalid authentication")

    return jwt_claims


def get_email_address_from_claims(claims):
    return claims["email"]


def verify_e2e_test_token(b64_jwt):
    if os.getenv("STAGE") in ["prod", "dev"]:
        return None
    elif b64_jwt == os.getenv("E2E_JWT"):
        return {"email": "e2e@test.com"}
    elif b64_jwt == os.getenv("REVIEW_APP_JWT"):
        return {"email": "perks-developers@dangerfarms.com"}


def authenticate(api_gateway_event):
    # All requests must be authenticated. If the Authorization header is missing, that's already a 401.
    try:
        authorization_header = api_gateway_event["headers"]["Authorization"]
    except KeyError:
        raise JwtError("Unauthenticated")

    b64_jwt = authorization_header.replace("Bearer ", "")

    # Verify the JWT against the the token string in the env vars in local deployments
    e2e_token = verify_e2e_test_token(b64_jwt)
    if e2e_token is not None:
        logger.info("JWT verified as e2e test")
        return e2e_token

    # Verify the JWT against Google's server. If it's good, trust the claims.
    return verify_jwt(b64_jwt)
