import inspect
from enum import Enum


class ProposalStatuses:
    APPROVED = "APPROVED"
    OPEN = "OPEN"
    PURCHASED = "PURCHASED"
    REJECTED = "REJECTED"


class SortOptions(Enum):
    DATE_ASCENDING = 1
    DATE_DESCENDING = 2
    POPULARITY_ASCENDING = 3
    POPULARITY_DESCENDING = 4


def list_proposal_statuses():
    attributes = inspect.getmembers(ProposalStatuses, lambda a: not (inspect.isroutine(a)))
    return [a[0] for a in attributes if not (a[0].startswith("__") and a[0].endswith("__"))]


example_users = [
    "owen@dangerfarms.com",
    "tom@dangerfarms.com",
    "max@dangerfarms.com",
    "craig@dangerfarms.com",
    "lewis@dangerfarms.com",
    "balint@dangerfarms.com",
    "daniel@dangerfarms.com",
    "phil@dangerfarms.com",
]

admin_user = "lewis@dangerfarms.com"


days_until_proposal_expiry = 365
