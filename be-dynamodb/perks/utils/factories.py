import factory
import datetime
from perks.utils import get_current_year
from perks.utils.constants import ProposalStatuses, example_users


class ProposalFactory(factory.DictFactory):
    partition_key = "PROPOSAL"
    sort_key = factory.LazyAttribute(lambda proposal: f"PROPOSAL-{proposal.created_at}")
    created_at = factory.LazyFunction(lambda: datetime.datetime.now().isoformat())
    name = "Fruit bowl"
    owner = example_users[0]
    description = "Fruit bowls make excellent subjects for still-life painting."
    estimated_cost = 100
    status = ProposalStatuses.OPEN
    threshold = 50
    vote_count = factory.LazyAttribute(lambda proposal: len(proposal.votes))
    votes = []
    is_recurring = False


class ApprovedProposalFactory(ProposalFactory):
    status = ProposalStatuses.APPROVED
    vote_count = len(example_users)
    votes = [{"user": user, "accept": True} for user in example_users]


class RejectedProposalFactory(ProposalFactory):
    status = ProposalStatuses.REJECTED
    vote_count = len(example_users)
    votes = [{"user": user, "accept": False} for user in example_users]


class RecurringProposalFactory(ProposalFactory):
    is_recurring = True
    first_billing_date = factory.LazyFunction(lambda: datetime.datetime.now().isoformat())


class BudgetFactory(factory.DictFactory):
    partition_key = "BUDGET"
    sort_key = get_current_year()
    remaining_budget = 1000
    total_budget = 5000


class ThresholdFactory(factory.DictFactory):
    partition_key = "THRESHOLD"
    sort_key = "THRESHOLD"
    threshold = 50


class UserFactory(factory.DictFactory):
    partition_key = "USER"
    sort_key = f'{str(factory.Faker("first_name")).lower()}@dangerfarms.com'
    is_admin = False


class VoteFactory(factory.DictFactory):
    user = "craig@dangerfarms.com"
    accept = True
