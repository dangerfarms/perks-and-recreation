from perks.schemas import schema
from perks.utils.jwt import get_email_address_from_claims


def execute_query(query, jwt_claims, variables=None):
    authenticated_user = {"email": get_email_address_from_claims(jwt_claims)}
    return schema.execute(query, context={"authenticated_user": authenticated_user}, variables=variables).to_dict()
