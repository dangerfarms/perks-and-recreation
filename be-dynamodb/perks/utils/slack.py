import os
import requests

API_BASE_URL = "https://slack.com/api"


def get_ron_swan_token():
    return os.getenv("SLACK_BOT_USER_TOKEN", None)


def get_perks_channel_id():
    return os.getenv("SLACK_BOT_CHANNEL_ID")


def post_message_to_perks_channel(message_text):
    """Posts to slack as Ron Swan."""
    token = get_ron_swan_token()

    requests.post(
        f"{API_BASE_URL}/chat.postMessage",
        json={"channel": get_perks_channel_id(), "text": message_text},
        headers={"Authorization": f"Bearer {token}"},
    )


def is_slack_configured():
    return get_ron_swan_token() is not None


def get_user_email_from_user_id(user_id):
    token = get_ron_swan_token()

    response = requests.get(f"{API_BASE_URL}/users.info?token={token}&user={user_id}")

    return response.json()["user"]["profile"]["email"]
