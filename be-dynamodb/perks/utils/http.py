import json


def create_response(status_code, data):
    return create_raw_response(status_code, json.dumps(data))


def create_raw_response(status_code, data):
    return {"statusCode": status_code, "headers": {"Access-Control-Allow-Origin": "*"}, "body": data}
