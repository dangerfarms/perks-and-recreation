from perks.utils.aws_resources import get_proposals_table
from perks.utils.factories import BudgetFactory, ThresholdFactory
from perks.utils import get_current_year
from perks.data.budget import get_budget
from perks.data.threshold import get_threshold
from perks.data.users import get_users


def initialise_data(event, context):
    table = get_proposals_table()

    budget = get_budget(get_current_year())
    if not budget:
        table.put_item(Item=BudgetFactory.build(remaining_budget=0, total_budget=0))

    threshold = get_threshold()
    if not threshold:
        table.put_item(Item=ThresholdFactory.build())

    users = get_users()
    admin_exists = len([user for user in users if user["is_admin"]])
    if not admin_exists:
        admin = {"partition_key": "USER", "sort_key": "lewis@dangerfarms.com", "is_admin": True}
        table.put_item(Item=admin)
