from perks.utils.playground_html import html


def serve_playground(event, context):
    processed_html = html.replace("$$STAGE$$", event["requestContext"]["stage"])

    return {"statusCode": 200, "headers": {"Content-Type": "text/html"}, "body": processed_html}
