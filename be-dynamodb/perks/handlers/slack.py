import re

from perks.utils.http import create_raw_response, create_response
from urllib.parse import parse_qs

from perks.data.proposals import create_proposal as perks_create_proposal
from perks.utils.slack import get_user_email_from_user_id

EXAMPLE_COMMAND = "Play pétanque (We've always said we'd play it) for £200"


def create_proposal(event, context):
    args = parse_qs(event["body"])

    slack_user_id = args["user_id"][0]
    user_email = get_user_email_from_user_id(slack_user_id)

    try:
        (title, description, estimated_cost) = _parse_command(args["text"][0])
    except ParseFailedException:
        return create_response(
            200,
            {
                "response_type": "ephemeral",
                "text": "I couldn't understand that command. Make sure you formatted it properly.\n\n"
                f"*Example*: `{EXAMPLE_COMMAND}`",
            },
        )

    perks_create_proposal(user_email, title, estimated_cost, description)

    return create_raw_response(200, "Successfully created the proposal. Enjoy!")


class ParseFailedException(BaseException):
    pass


def _parse_command(command):
    """Given some command, return a tuple containing:
    (title, description, estimated_cost)

    Example command: 'Play pétanque (We've always said we'd play it) for £200'
    """
    matcher = re.compile("([^\(]*) \(([^\)]*)\) for £*([0-9]*)")
    matches = matcher.match(command)

    if matches is None:
        raise ParseFailedException()

    return matches.group(1), matches.group(2), matches.group(3)
