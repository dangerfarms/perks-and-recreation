from datetime import datetime
import random

from perks.data.users import create_user
from perks.utils.aws_resources import get_proposals_table
from perks.utils.factories import BudgetFactory, ThresholdFactory
from perks.utils.randomised_data_factories import RandomisedProposalFactory, RandomisedOpenProposalFactory
from perks.utils.constants import ProposalStatuses, example_users


proposals_table = get_proposals_table()


def add_proposal_owner(event, context, proposal=None):
    if not proposal:
        proposal = RandomisedProposalFactory.build()
        proposals_table.put_item(Item=proposal)
    owner = {"partition_key": f"OWNER-{proposal['owner']}", "sort_key": proposal["sort_key"]}
    proposals_table.put_item(Item=owner)


def add_example_items(event, context):
    # For now it's okay to do this every time, as the primary keys are the same so will overwrite
    last_year = str(datetime.now().year - 1)
    proposals_table.put_item(Item=BudgetFactory.build())
    proposals_table.put_item(Item=BudgetFactory.build(sort_key=last_year, remaining_budget=0, total_budget=2000))
    proposals_table.put_item(Item=ThresholdFactory.build())

    # Again, this is fine to keep doing for the same reason as above
    for user in example_users:
        is_admin = False
        if user == "lewis@dangerfarms.com":
            is_admin = True
        create_user(user, is_admin)

    for i in range(10):
        proposal = RandomisedProposalFactory.build()
        proposals_table.put_item(Item=proposal)
        add_proposal_owner(event, context, proposal=proposal)

        if proposal["status"] == ProposalStatuses.OPEN:
            voters_so_far = [vote["user"] for vote in proposal["votes"]]
            users_to_vote = [user for user in example_users if user not in voters_so_far]
            add_pending_votes(event, context, proposal=proposal, users_to_vote=users_to_vote)


def add_pending_votes(event, context, proposal=None, users_to_vote=example_users):
    if not proposal:
        proposal = RandomisedOpenProposalFactory.build()
        proposals_table.put_item(Item=proposal)
        add_proposal_owner(event, context, proposal=proposal)
    pending_vote = {"sort_key": proposal["sort_key"]}

    for user in users_to_vote:
        pending_vote["partition_key"] = f"PENDING_VOTE-{user}"
        proposals_table.put_item(Item=pending_vote)


def add_proposals_with_vote_pending(event, context):
    for i, user in enumerate(example_users):
        voters = example_users[:]
        pending_vote_user = voters.pop(i)
        proposal = RandomisedOpenProposalFactory.build(
            vote_count=len(voters), votes=[{"user": voter, "accept": random.choice([True, False])} for voter in voters]
        )
        proposals_table.put_item(Item=proposal)
        add_proposal_owner(event, context, proposal=proposal)
        pending_vote = {"sort_key": proposal["sort_key"]}

        pending_vote["partition_key"] = f"PENDING_VOTE-{pending_vote_user}"
        proposals_table.put_item(Item=pending_vote)
