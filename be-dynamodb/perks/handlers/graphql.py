import json

from perks.data.proposals import add_pending_votes_to_all_proposals_for_user
from perks.data.users import get_user, create_user
from perks.utils.graphql import execute_query
from perks.utils.http import create_response
from perks.utils.jwt import JwtError, authenticate, get_email_address_from_claims
from perks.utils.logging import get_logger

logger = get_logger(__name__)


def graphql(event, context):
    try:
        jwt_claims = authenticate(event)
    except JwtError as error:
        return create_response(401, {"errors": [{"message": str(error)}]})

    my_email_address = get_email_address_from_claims(jwt_claims)
    me = get_user(my_email_address)
    if not me:
        logger.debug(f"Adding new user. email_address: {my_email_address}")
        create_user(my_email_address, False)
        add_pending_votes_to_all_proposals_for_user(my_email_address)

    # Extract the GraphQL from the GET params or POST data
    query = None
    variables = None

    if event["httpMethod"] == "GET":
        query_params = event.get("queryStringParameters")
        if query_params is not None:
            query = query_params.get("query")
            variables = json.loads(query_params.get("variables"))
    elif event["httpMethod"] == "POST":
        query = json.loads(event.get("body", {})).get("query")
        variables = json.loads(event.get("body", {})).get("variables")

    logger.debug("Parsed query", {"query": query})
    logger.debug("Parsed variables", {"variables": variables})

    result = execute_query(query, jwt_claims, variables)
    return create_response(200, result)
