import json
from unittest import TestCase

from mock import patch, Mock

from perks.handlers.slack import create_proposal, EXAMPLE_COMMAND


def create_event(user_id, text):
    return {"body": f"user_id={user_id}&text={text}"}


class CreateProposalTestCase(TestCase):
    @patch("perks.handlers.slack.get_user_email_from_user_id", Mock(return_value="craig@dangerfarms.com"))
    @patch("perks.handlers.slack.perks_create_proposal")
    def test_should_return_a_helpful_response_when_message_is_not_expected_format(self, mock_save_proposal):
        response = create_proposal(create_event("user_id", "This doesn't match the format"), None)

        self.assertEqual(response["statusCode"], 200)

        self.assertEqual(
            response["body"],
            json.dumps(
                {
                    "response_type": "ephemeral",
                    "text": "I couldn't understand that command. Make sure you formatted it properly.\n\n"
                    f"*Example*: `{EXAMPLE_COMMAND}`",
                }
            ),
        )

        mock_save_proposal.assert_not_called()

    @patch("perks.handlers.slack.perks_create_proposal")
    @patch("perks.handlers.slack.get_user_email_from_user_id", Mock(return_value="craig@dangerfarms.com"))
    def test_should_create_a_proposal_and_return_success_message_when_well_formed(self, mock_save_proposal):
        response = create_proposal(create_event("user_id", "A new proposal (with a description) for £2"), None)

        mock_save_proposal.assert_called_once_with("craig@dangerfarms.com", "A new proposal", "2", "with a description")

        self.assertEqual(response["statusCode"], 200)

        self.assertEqual(response["body"], "Successfully created the proposal. Enjoy!")

    @patch("perks.handlers.slack.perks_create_proposal")
    @patch("perks.handlers.slack.get_user_email_from_user_id", Mock(return_value="craig@dangerfarms.com"))
    def test_should_create_a_proposal_and_return_success_message_without_pound_sign(self, mock_save_proposal):
        response = create_proposal(create_event("user_id", "A new proposal (with a description) for 2"), None)

        mock_save_proposal.assert_called_once_with("craig@dangerfarms.com", "A new proposal", "2", "with a description")

        self.assertEqual(response["statusCode"], 200)

        self.assertEqual(response["body"], "Successfully created the proposal. Enjoy!")
