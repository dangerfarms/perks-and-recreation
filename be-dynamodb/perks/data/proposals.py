import datetime
import os

from boto3.dynamodb.conditions import Key, Attr

from perks.emails import (
    send_created_proposal_email,
    send_approved_proposal_email,
    send_rejected_proposal_email,
    send_expired_proposal_email,
)
from perks.utils.aws_resources import get_dynamodb_client, get_proposals_table
from perks.utils.constants import ProposalStatuses, SortOptions, days_until_proposal_expiry
from perks.data.users import get_users
from perks.data.threshold import get_threshold
from perks.utils.logging import get_logger
from perks.utils.slack import post_message_to_perks_channel, is_slack_configured

logger = get_logger(__name__)


def hide_owner_if_not_user(item, user_id):
    if item["owner"] != user_id:
        item["owner"] = None
    return item


def hide_vote_values_of_other_users(item, user_id):
    if item["status"] == ProposalStatuses.OPEN:
        for vote in item["votes"]:
            if vote["user"] != user_id:
                vote["accept"] = None
    return item


def delete_proposal_if_expired(proposal):
    if proposal["status"] == ProposalStatuses.OPEN:
        time_proposal_has_been_open = None
        try:
            time_proposal_has_been_open = datetime.datetime.now() - datetime.datetime.strptime(
                proposal["created_at"], "%Y-%m-%dT%H:%M:%S.%f"
            )
        except Exception as err:
            logger.debug(
                f"Item appears to have non-iso timestamp (should occur only in testing); see error: {err}",
                {"proposal": proposal},
            )
        if time_proposal_has_been_open and time_proposal_has_been_open.days >= days_until_proposal_expiry:
            proposal["is_deleted"] = True
            table = get_proposals_table()
            table.put_item(Item=proposal)
            send_expired_proposal_email(proposal)
            logger.debug("Item after expiry", {"proposal": proposal})
            return True
    return False


def process_proposals(proposals, user_id):
    processed_proposals = []
    for proposal in proposals:
        proposal_has_expired = delete_proposal_if_expired(proposal)
        if not proposal_has_expired:
            proposal = hide_owner_if_not_user(proposal, user_id)
            proposal = hide_vote_values_of_other_users(proposal, user_id)
            if proposal.get("sort_key"):
                proposal["id"] = proposal.pop("sort_key")
            processed_proposals.append(proposal)
    logger.debug(f"Proposals after processing for front-end", {"Processed proposals": processed_proposals})
    return processed_proposals


def get_proposal(proposal_id):
    table = get_proposals_table()
    item = table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id}).get("Item")

    if not item:
        logger.debug("Proposal not found with this ID", {"proposal_id": proposal_id})
        return None

    logger.debug(f"Proposal item with ID {proposal_id}", {"proposal_item": item})

    return item


def _sort_proposals(proposals, sort):
    if sort in (SortOptions.POPULARITY_ASCENDING.value, SortOptions.POPULARITY_DESCENDING.value):
        return _sort_proposals_by_popularity(proposals, sort)
    else:
        return proposals


def _sort_proposals_by_popularity(proposals, sort):
    should_reverse = sort == SortOptions.POPULARITY_DESCENDING.value
    sorted_proposals = sorted(
        proposals,
        key=lambda proposal: len(([vote for vote in proposal["votes"] if vote["accept"]])),
        reverse=should_reverse,
    )
    return sorted_proposals


def list_proposals(user_id, sort=SortOptions.DATE_ASCENDING.value):
    table = get_proposals_table()
    response = table.query(
        KeyConditionExpression=Key("partition_key").eq("PROPOSAL"),
        ScanIndexForward=sort == SortOptions.DATE_ASCENDING.value,
        FilterExpression=Attr("is_deleted").not_exists() | Attr("is_deleted").eq(False),
    )
    proposals = response["Items"]
    logger.debug("All proposals from DynamoDB", {"proposal_items": proposals})
    return _sort_proposals(proposals, sort)


def get_proposals_by_owner(owner_id, sort):
    table = get_proposals_table()

    response = table.query(
        KeyConditionExpression=Key("partition_key").eq("PROPOSAL"),
        FilterExpression=Key("owner").eq(f"{owner_id}") & Attr("is_deleted").not_exists()
        | Attr("is_deleted").eq(False),
        ScanIndexForward=sort == SortOptions.DATE_ASCENDING.value,
    )
    proposals = response["Items"]
    return _sort_proposals(proposals, sort)


def get_pending_vote_items(owner_id):
    table = get_proposals_table()

    response = table.query(
        KeyConditionExpression=Key("partition_key").eq(f"PENDING_VOTE-{owner_id}"),
        FilterExpression=Attr("is_deleted").not_exists() | Attr("is_deleted").eq(False),
    )
    proposals = response["Items"]
    logger.debug("Pending vote items", {"owner_id": owner_id, "items": proposals})
    return proposals


def get_proposals_user_needs_to_vote_on(user_id, sort):
    client = get_dynamodb_client()
    table_name = os.getenv("PROPOSALS_TABLE")

    pending_vote_items = get_pending_vote_items(user_id)
    if not pending_vote_items:
        return []

    response = client.batch_get_item(
        RequestItems={
            table_name: {
                "Keys": [
                    {"partition_key": "PROPOSAL", "sort_key": pending_vote_item["sort_key"]}
                    for pending_vote_item in pending_vote_items
                ],
                "ProjectionExpression": "created_at, sort_key, #n, #o, #s, description, estimated_cost, vote_count, votes",
                "ExpressionAttributeNames": {"#n": "name", "#o": "owner", "#s": "status"},
            }
        }
    )
    proposals = response["Responses"][table_name]

    should_reverse = sort == SortOptions.DATE_DESCENDING.value
    date_sorted_proposals = sorted(proposals, key=lambda item: item["created_at"], reverse=should_reverse)

    return _sort_proposals(date_sorted_proposals, sort)


def get_proposals_by_status(status, sort):
    table = get_proposals_table()

    print(f"status: {status}")

    response = table.query(
        IndexName="status_index",
        KeyConditionExpression=Key("status").eq(status),
        ScanIndexForward=sort == SortOptions.DATE_ASCENDING.value,
    )
    proposals = response["Items"]
    logger.debug("Proposals from DynamoDB, filtered by status", {"proposal_items": proposals, "status": status})

    return _sort_proposals(proposals, sort)


def create_proposal(owner_id, name, estimated_cost, description="", is_recurring=False, first_billing_date=None):
    logger.debug("create_proposals", {"is_recurring": is_recurring})

    table = get_proposals_table()

    created_at = datetime.datetime.now().isoformat()

    try:
        voting_threshold = get_threshold()["threshold"]
    except Exception as err:
        logger.error(
            f"Failed to get threshold during proposal creation with: {err}. Threshold will be defaulted to 50%"
        )
        voting_threshold = 50

    new_item = {
        "partition_key": "PROPOSAL",
        "sort_key": f"PROPOSAL-{created_at}",
        "created_at": created_at,
        "name": name,
        "description": description,
        "estimated_cost": estimated_cost,
        "owner": owner_id,
        "status": ProposalStatuses.OPEN,
        "threshold": voting_threshold,
        "vote_count": 0,
        "votes": [],
        "is_recurring": is_recurring,
    }

    if is_recurring:
        new_item["first_billing_date"] = first_billing_date

    table.put_item(Item=new_item)

    # Ideally I'd prefer to have this at the resolver level, as this is a side effect of creating a proposal
    # rather than part of the logic required to make it happen. Reason it isn't is sadly about where and how
    # it's tested.
    send_created_proposal_email(new_item)

    if is_slack_configured():
        post_message_to_perks_channel(
            f"A new proposal was created by `{owner_id}`:\n\n"
            f"*{name}*\n"
            f"{description}\n\n"
            f"*Estimated cost*: £{estimated_cost}"
        )
    else:
        logger.debug(f"Skipping posting message to slack channel. Slack is not configured.")

    add_pending_vote_to_proposal_for_all_users(new_item)

    return new_item


def edit_proposal(user_id, proposal_id, name=None, estimated_cost=None, description=None):
    table = get_proposals_table()

    logger.debug("About to edit proposal", {"proposalId": proposal_id})

    item = table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id}).get("Item")
    if not item:
        raise ValueError(f"Cannot edit proposal {proposal_id}: it does not exist")

    logger.debug("Proposal item to be edited", {"proposal": item})

    if item["owner"] != user_id:
        raise ValueError("Cannot edit someone else's proposal")

    if item["status"] != ProposalStatuses.OPEN:
        raise ValueError("Proposal can only be edited if it is still open")

    if item["vote_count"] != 0:
        raise ValueError("Proposal can only be edited if no one has voted on it yet")

    item["last_edit_at"] = datetime.datetime.now().isoformat()
    item["name"] = name or item["name"]
    item["estimated_cost"] = estimated_cost or item["estimated_cost"]
    item["description"] = description or item["description"]

    table.put_item(Item=item)
    logger.debug("Item after editing", {"proposal": item})
    return item


def delete_proposal(user_id, proposal_id):
    table = get_proposals_table()

    logger.debug("About to delete proposal", {"proposalId": proposal_id})
    item = table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id}).get("Item")
    if not item:
        raise ValueError(f"Cannot delete proposal {proposal_id}, it does not exist")

    logger.debug("Proposal item to be deleted", {"proposal": item})

    if item["owner"] != user_id:
        raise ValueError("Cannot delete someone else's proposal")

    if item["status"] != ProposalStatuses.OPEN:
        raise ValueError("Proposal can only be deleted if it is still open")
    else:
        item["is_deleted"] = True
        table.put_item(Item=item)
        logger.debug("Item after deletion", {"proposal": item})


def add_vote_to_proposal(proposal_id, user, accept):
    table = get_proposals_table()

    item = table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id})["Item"]

    try:
        user_vote_index = next(i for i, vote in enumerate(item["votes"]) if vote["user"] == user)
    except StopIteration:
        user_vote_index = -1

    if user_vote_index == -1:
        item["vote_count"] += 1
        item["votes"].append({"user": user, "accept": accept})

        logger.debug("Users", {"users": get_users()})

        number_of_users = len(get_users())
        voting_threshold = item["threshold"]

        if number_of_users == item["vote_count"]:
            votes_in_favour = sum(1 for vote in item["votes"] if vote["accept"])
            if votes_in_favour and votes_in_favour / number_of_users * 100 >= voting_threshold:
                item["status"] = ProposalStatuses.APPROVED
                send_approved_proposal_email(item)
            else:
                item["status"] = ProposalStatuses.REJECTED
                send_rejected_proposal_email(item)
    elif item["votes"][user_vote_index]["accept"] == accept:
        raise ValueError(f"User has already voted {'for' if accept else 'against'} this proposal")
    else:
        item["votes"][user_vote_index]["accept"] = accept

    table.put_item(Item=item)

    votes = item["votes"]
    if item["status"] == ProposalStatuses.OPEN:
        # Hide identities of other voters
        for vote in votes:
            if vote["user"] != user:
                vote["user"] = None

    return {
        "accept": item["votes"][user_vote_index]["accept"],
        "status": item["status"],
        "vote_count": item["vote_count"],
        "votes": votes,
    }


def purchase_proposal(proposal):
    table = get_proposals_table()

    proposal["status"] = ProposalStatuses.PURCHASED

    table.put_item(Item=proposal)

    logger.debug("Proposal purchased", {"proposal": proposal})

    return proposal


def add_pending_vote(proposal, user_id):
    table = get_proposals_table()

    pending_vote = {"sort_key": proposal["sort_key"], "partition_key": f"PENDING_VOTE-{user_id}"}

    table.put_item(Item=pending_vote)
    return pending_vote


def add_pending_votes_to_all_proposals_for_user(user_id):
    open_proposals = [proposal for proposal in list_proposals(user_id) if proposal["status"] == ProposalStatuses.OPEN]
    for proposal in open_proposals:
        add_pending_vote(proposal, user_id)


def add_pending_vote_to_proposal_for_all_users(proposal):
    users = get_users()

    for user in users:
        add_pending_vote(proposal, user["email"])
