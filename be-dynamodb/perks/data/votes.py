from perks.utils.aws_resources import get_proposals_table


def remove_pending_vote(proposal, user):
    proposals_table = get_proposals_table()
    return proposals_table.delete_item(Key={"partition_key": f"PENDING_VOTE-{user}", "sort_key": proposal})
