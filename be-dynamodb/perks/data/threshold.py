from boto3.dynamodb.conditions import Key

from perks.utils.aws_resources import get_proposals_table
from perks.utils.logging import get_logger

logger = get_logger(__name__)


def get_threshold():
    table = get_proposals_table()

    response = table.query(
        KeyConditionExpression=Key("partition_key").eq("THRESHOLD") & Key("sort_key").eq("THRESHOLD")
    )
    items = response["Items"]
    logger.debug("Voting threshold items", {"threshold_items": items})
    if items:
        return items[0]


def set_threshold(threshold):
    table = get_proposals_table()

    new_item = {"partition_key": "THRESHOLD", "sort_key": "THRESHOLD", "threshold": threshold}

    table.put_item(Item=new_item)
    logger.debug("New threshold set", {"threshold_item": new_item})
    return new_item
