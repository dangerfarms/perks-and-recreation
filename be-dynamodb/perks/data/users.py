from boto3.dynamodb.conditions import Key

from perks.utils.aws_resources import get_proposals_table
from perks.utils.logging import get_logger

logger = get_logger(__name__)


def get_users():
    table = get_proposals_table()

    response = table.query(KeyConditionExpression=Key("partition_key").eq("USER"))
    items = response["Items"]
    logger.debug("Retrieved user items from DynamoDB", {"user_items": items})
    if items:
        return [{"email": item["sort_key"], "is_admin": item["is_admin"]} for item in items]
    return []


def get_user(user_email):
    proposals_table = get_proposals_table()
    result = proposals_table.get_item(Key={"partition_key": "USER", "sort_key": user_email})
    item = result.get("Item")
    logger.debug("Retrieved user item from DynamoDB", {"user_item": item})

    if item:
        return {"email": item["sort_key"], "is_admin": item["is_admin"]}
    return None


def create_user(user_id, is_admin):
    table = get_proposals_table()

    new_item = {"partition_key": "USER", "sort_key": user_id, "is_admin": is_admin}

    table.put_item(Item=new_item)
    logger.debug("New user created", {"user_item": new_item})
    return new_item
