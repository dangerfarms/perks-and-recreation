from unittest import TestCase

from freezegun import freeze_time
from mock import patch, MagicMock

from perks.data.proposals import (
    process_proposals,
    list_proposals,
    create_proposal,
    add_pending_votes_to_all_proposals_for_user,
)
from perks.utils.constants import ProposalStatuses
from perks.utils.factories import ProposalFactory, ApprovedProposalFactory, RejectedProposalFactory


class ProposalsBaseTestCase(TestCase):
    def setUp(self):
        self.get_proposals_table_patcher = patch("perks.data.proposals.get_proposals_table")
        self.mock_get_table = self.get_proposals_table_patcher.start()
        self.send_mock_email_patcher = patch("perks.data.proposals.send_created_proposal_email")
        self.mock_send_email = self.send_mock_email_patcher.start()
        self.send_slack_notification_patcher = patch("perks.data.proposals.post_message_to_perks_channel")
        self.mock_send_slack_notification = self.send_slack_notification_patcher.start()
        self.add_pending_votes_for_all_users_patcher = patch(
            "perks.data.proposals.add_pending_vote_to_proposal_for_all_users"
        )
        self.mock_add_pending_votes_for_all_users = self.add_pending_votes_for_all_users_patcher.start()
        self.is_slack_configured_patcher = patch("perks.data.proposals.is_slack_configured")
        self.mock_is_slack_configured = self.is_slack_configured_patcher.start()
        self.mock_is_slack_configured.return_value = True

        self.mock_table = MagicMock()
        self.mock_get_table.return_value = self.mock_table

    def tearDown(self):
        self.get_proposals_table_patcher.stop()
        self.send_mock_email_patcher.stop()
        self.send_slack_notification_patcher.stop()
        self.add_pending_votes_for_all_users_patcher.stop()
        self.is_slack_configured_patcher.stop()


class ListProposalsTestCase(ProposalsBaseTestCase):
    def test_should_hide_owner_when_proposal_owner_doesnt_match_supplied_user_id(self):
        someone_elses_proposal = ProposalFactory.build(owner="other@dangerfarms.com")
        self.mock_table.query.return_value = {"Items": [someone_elses_proposal]}

        proposals = process_proposals(list_proposals("me@dangerfarms.com"), "me@dangerfarms.com")
        self.assertIsNone(proposals[0]["owner"])

    def test_should_show_owner_when_proposal_owner_matches_supplied_user_id(self):
        my_proposal = ProposalFactory.build(owner="me@dangerfarms.com")
        self.mock_table.query.return_value = {"Items": [my_proposal]}

        proposals = process_proposals(list_proposals("me@dangerfarms.com"), "me@dangerfarms.com")
        self.assertEqual(proposals[0]["owner"], "me@dangerfarms.com")


class CreateProposalTestCase(ProposalsBaseTestCase):
    @freeze_time("2019-01-01T12:00:00")
    def test_should_call_put_item_with_expected_params(self):
        create_proposal("craig@dangerfarms.com", "New Proposal", 1_000, "Description of new proposal")

        self.mock_table.put_item.assert_called_once_with(
            Item={
                "partition_key": "PROPOSAL",
                "sort_key": "PROPOSAL-2019-01-01T12:00:00",
                "created_at": "2019-01-01T12:00:00",
                "name": "New Proposal",
                "description": "Description of new proposal",
                "estimated_cost": 1_000,
                "owner": "craig@dangerfarms.com",
                "status": ProposalStatuses.OPEN,
                "threshold": 50,
                "vote_count": 0,
                "votes": [],
                "is_recurring": False,
            }
        )

    @freeze_time("2019-01-01T12:00:00")
    def test_should_call_put_item_without_description(self):
        create_proposal("craig@dangerfarms.com", "New Proposal", 1_000)

        self.mock_table.put_item.assert_called_once_with(
            Item={
                "partition_key": "PROPOSAL",
                "sort_key": "PROPOSAL-2019-01-01T12:00:00",
                "created_at": "2019-01-01T12:00:00",
                "name": "New Proposal",
                "description": "",
                "estimated_cost": 1_000,
                "owner": "craig@dangerfarms.com",
                "status": ProposalStatuses.OPEN,
                "threshold": 50,
                "vote_count": 0,
                "votes": [],
                "is_recurring": False,
            }
        )

    @freeze_time("2019-01-01T12:00:00")
    def test_should_send_email_on_creation(self):
        new_item = create_proposal("craig@dangerfarms.com", "New Proposal", 1_000)
        self.mock_send_email.assert_called_once_with(new_item)

    @freeze_time("2019-01-01T12:00:00")
    def test_should_add_pending_votes_to_all_users(self):
        new_item = create_proposal("craig@dangerfarms.com", "New Proposal", 1_000)
        self.mock_add_pending_votes_for_all_users.assert_called_once_with(new_item)

    def test_should_send_slack_message(self):
        create_proposal("craig@dangerfarms.com", "New Proposal", 1_000)
        self.mock_send_slack_notification.assert_called_once_with(
            "A new proposal was created by `craig@dangerfarms.com`:\n\n"
            "*New Proposal*\n\n\n"
            "*Estimated cost*: £1000"
        )

    def test_should_not_send_slack_message_if_not_configured(self):
        self.mock_is_slack_configured.return_value = False

        create_proposal("craig@dangerfarms.com", "New Proposal", 1_000)

        self.mock_send_slack_notification.assert_not_called()

    @freeze_time("2019-01-01T12:00:00")
    def test_should_not_set_first_billing_date_if_not_recurring(self):
        create_proposal("craig@dangerfarms.com", "New Proposal", 1_000, "", False, "2019-01-01")

        self.mock_table.put_item.assert_called_once_with(
            Item={
                "partition_key": "PROPOSAL",
                "sort_key": "PROPOSAL-2019-01-01T12:00:00",
                "created_at": "2019-01-01T12:00:00",
                "name": "New Proposal",
                "description": "",
                "estimated_cost": 1_000,
                "owner": "craig@dangerfarms.com",
                "status": ProposalStatuses.OPEN,
                "threshold": 50,
                "vote_count": 0,
                "votes": [],
                "is_recurring": False,
            }
        )

    @freeze_time("2019-01-01T12:00:00")
    def test_should_set_first_billing_date_to_none_if_not_provided(self):
        create_proposal("craig@dangerfarms.com", "New Proposal", 1_000, "", True, None)

        self.mock_table.put_item.assert_called_once_with(
            Item={
                "partition_key": "PROPOSAL",
                "sort_key": "PROPOSAL-2019-01-01T12:00:00",
                "created_at": "2019-01-01T12:00:00",
                "name": "New Proposal",
                "description": "",
                "estimated_cost": 1_000,
                "owner": "craig@dangerfarms.com",
                "status": ProposalStatuses.OPEN,
                "threshold": 50,
                "vote_count": 0,
                "votes": [],
                "is_recurring": True,
                "first_billing_date": None,
            }
        )

    @freeze_time("2019-01-01T12:00:00")
    def test_should_set_first_billing_date(self):
        create_proposal("craig@dangerfarms.com", "New Proposal", 1_000, "", True, "2019-01-01")

        self.mock_table.put_item.assert_called_once_with(
            Item={
                "partition_key": "PROPOSAL",
                "sort_key": "PROPOSAL-2019-01-01T12:00:00",
                "created_at": "2019-01-01T12:00:00",
                "name": "New Proposal",
                "description": "",
                "estimated_cost": 1_000,
                "owner": "craig@dangerfarms.com",
                "status": ProposalStatuses.OPEN,
                "threshold": 50,
                "vote_count": 0,
                "votes": [],
                "is_recurring": True,
                "first_billing_date": "2019-01-01",
            }
        )


class AddPendingVotesToAllProposalsForUserTestCase(ProposalsBaseTestCase):
    @patch("perks.data.proposals.list_proposals")
    def test_should_add_pending_votes_to_open_proposals(self, mock_list_proposals):
        open_proposal = ProposalFactory()

        mock_list_proposals.return_value = [ApprovedProposalFactory(), RejectedProposalFactory(), open_proposal]

        add_pending_votes_to_all_proposals_for_user("craig@dangerfarms.com")

        self.mock_table.put_item.assert_called_once_with(
            Item={"sort_key": open_proposal["sort_key"], "partition_key": "PENDING_VOTE-craig@dangerfarms.com"}
        )
