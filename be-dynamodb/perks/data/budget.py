from boto3.dynamodb.conditions import Key

from perks.utils.aws_resources import get_proposals_table
from perks.utils.logging import get_logger

logger = get_logger(__name__)


def get_budget(year):
    table = get_proposals_table()

    response = table.query(KeyConditionExpression=Key("partition_key").eq("BUDGET") & Key("sort_key").eq(year))
    items = response["Items"]
    logger.debug("Retrieved budget items from DynamoDB", {"budget_items": items})
    if items:
        return items[0]
    return None


def set_budget(total_budget, remaining_budget, year):
    table = get_proposals_table()

    new_item = {
        "partition_key": "BUDGET",
        "sort_key": year,
        "total_budget": total_budget,
        "remaining_budget": remaining_budget,
    }

    table.put_item(Item=new_item)
    logger.debug("New budget set" if remaining_budget == total_budget else "Budget updated", {"budget_item": new_item})
    return new_item
