import json
import os

from mock import patch, Mock

from perks.handlers.graphql import graphql
from perks.integration_tests import LocalDynamoDBTestCase
from perks.mocks import mock_environment


class AuthenticateTest(LocalDynamoDBTestCase):
    def test_should_return_401_if_no_auth_header(self):
        event = {"headers": {}}

        response = graphql(event, None)

        self.assertEqual(401, response["statusCode"])

    @patch("perks.utils.jwt.id_token.verify_oauth2_token", Mock(side_effect=ValueError))
    def test_should_return_401_if_token_validation_fails(self):
        event = {"headers": {"Authorization": "this-is-a-bad-jwt-token"}}

        response = graphql(event, None)

        self.assertEqual(401, response["statusCode"])

    @patch(
        "perks.utils.jwt.id_token.verify_oauth2_token",
        Mock(
            return_value={
                "email": "tom@dangerfarms.com",
                "hd": "dangerfarms.com",
                "aud": "test-client-id",
                "iss": "accounts.google.com",
            }
        ),
    )
    def test_should_return_200_if_token_validation_succeeds(self):
        query = """
            query {
                proposals {
                    name
                }
            }
        """
        event = {
            "headers": {"Authorization": "this-is-a-good-jwt-token"},
            "httpMethod": "POST",
            "body": json.dumps({"query": query}),
        }

        response = graphql(event, None)

        self.assertEqual(200, response["statusCode"])

    @patch.dict(os.environ, {**mock_environment, "E2E_JWT": "mock_e2e_jwt"})
    def test_should_return_200_if_jwt_and_stage_indicate_e2e_testing(self):
        query = """
            query {
                proposals {
                    name
                }
            }
        """
        event = {
            "headers": {"Authorization": "mock_e2e_jwt"},
            "httpMethod": "POST",
            "body": json.dumps({"query": query}),
        }

        response = graphql(event, None)

        self.assertEqual(200, response["statusCode"])

    @patch.dict(os.environ, {**mock_environment, "REVIEW_APP_JWT": "mock_review_app_jwt"})
    def test_should_return_200_if_jwt_and_stage_indicate_request_is_from_review_app(self):
        query = """
                query {
                    proposals {
                        name
                    }
                }
            """
        event = {
            "headers": {"Authorization": "mock_review_app_jwt"},
            "httpMethod": "POST",
            "body": json.dumps({"query": query}),
        }

        response = graphql(event, None)

        self.assertEqual(200, response["statusCode"])

    @patch.dict(os.environ, {**mock_environment, "E2E_JWT": "mock_e2e_jwt", "STAGE": "prod"})
    def test_should_not_return_200_if_stage_does_not_indicate_e2e_testing(self):
        query = """
                query {
                    proposals {
                        name
                    }
                }
            """
        event = {
            "headers": {"Authorization": "mock_e2e_jwt"},
            "httpMethod": "POST",
            "body": json.dumps({"query": query}),
        }

        response = graphql(event, None)

        self.assertEqual(401, response["statusCode"])

    @patch.dict(os.environ, {**mock_environment, "E2E_JWT": "mock_e2e_jwt"})
    def test_should_not_return_200_if_e2e_jwt_is_present_in_env_but_not_in_request(self):
        query = """
                    query {
                        proposals {
                            name
                        }
                    }
                """
        event = {
            "headers": {"Authorization": "not_mock_e2e_jwt"},
            "httpMethod": "POST",
            "body": json.dumps({"query": query}),
        }

        response = graphql(event, None)

        self.assertEqual(401, response["statusCode"])
