from perks.utils.graphql import execute_query
from perks.integration_tests import LocalDynamoDBTestCase


class UserGraphQLTest(LocalDynamoDBTestCase):
    def test_should_create_first_user_as_admin_subsequent_not(self):
        mutation = """
            mutation CreateUser {
                createUser {
                    user {
                      email
                      isAdmin
                    }
                }
            }
        """

        execute_query(mutation, jwt_claims={"email": "lewis@dangerfarms.com"})
        execute_query(mutation, jwt_claims={"email": "max@dangerfarms.com"})

        first_item = self.proposals_table.get_item(
            Key={"partition_key": "USER", "sort_key": "lewis@dangerfarms.com"}
        ).get("Item")

        second_item = self.proposals_table.get_item(
            Key={"partition_key": "USER", "sort_key": "max@dangerfarms.com"}
        ).get("Item")

        self.assertIsNotNone(first_item)
        self.assertIsNotNone(second_item)

        # Lewis is the first user to be created, so should have admin status
        self.assertTrue(first_item["is_admin"])

        # Max is not the first user to be created, so should not have admin status
        self.assertFalse(second_item["is_admin"])

    def test_should_return_user_information(self):
        self.proposals_table.put_item(
            Item={"partition_key": "USER", "sort_key": "tom@dangerfarms.com", "is_admin": False}
        )

        query = """
            query {
                me {
                    email
                    isAdmin
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"})

        self.assertEqual(result, {"data": {"me": {"email": "tom@dangerfarms.com", "isAdmin": False}}})
