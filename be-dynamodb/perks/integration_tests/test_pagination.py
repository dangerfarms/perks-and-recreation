from perks.utils.graphql import execute_query
from perks.utils.factories import ProposalFactory
from perks.integration_tests import LocalDynamoDBTestCase


class ProposalGraphQLTest(LocalDynamoDBTestCase):
    def test_should_return_next_ten_proposals_after_cursor_when_supplied(self):
        proposals = ProposalFactory.build_batch(20)

        with self.proposals_table.batch_writer() as batch:
            for proposal in proposals:
                batch.put_item(Item=proposal)

        proposals = sorted(proposals, key=lambda p: p["sort_key"])
        expected_proposals = [{"id": proposal["sort_key"]} for proposal in proposals[::-1]]

        query = """
            query PaginatedProposals($filter: String!, $cursor: Int) {
              paginatedProposals(filter: $filter, cursor: $cursor) {
                proposals {
                  id
                }
                cursor {
                  current
                  final
                }
              }
            }
        """

        result = execute_query(
            query, jwt_claims={"email": "owen@dangerfarms.com"}, variables={"filter": "all", "cursor": 5}
        )["data"]["paginatedProposals"]

        # Pagination limit is 10, so 10 proposals should have been returned
        self.assertTrue(len(result["proposals"]) == 10)
        # Cursor position was 5, so the corresponding subset of the proposals list should have been returned
        self.assertEqual(expected_proposals[5:15], result["proposals"])
        # The cursor position should no be at 15, having advanced 10 places
        self.assertTrue(result["cursor"]["current"] == 15)
        # There are a total of 20 proposals, so the final cursor position should be 20
        self.assertTrue(result["cursor"]["final"] == 20)

    def test_should_return_first_ten_proposals_when_cursor_not_supplied(self):
        proposals = ProposalFactory.build_batch(20)

        with self.proposals_table.batch_writer() as batch:
            for proposal in proposals:
                batch.put_item(Item=proposal)

        proposals = sorted(proposals, key=lambda p: p["sort_key"])
        expected_proposals = [{"id": proposal["sort_key"]} for proposal in proposals[::-1]]

        query = """
            query PaginatedProposals($filter: String!, $cursor: Int) {
              paginatedProposals(filter: $filter, cursor: $cursor) {
                proposals {
                  id
                }
                cursor {
                  current
                  final
                }
              }
            }
        """

        result = execute_query(query, jwt_claims={"email": "owen@dangerfarms.com"}, variables={"filter": "all"})
        paginated_proposals = result["data"]["paginatedProposals"]

        # Pagination limit is 10, so 10 proposals should have been returned
        self.assertTrue(len(paginated_proposals["proposals"]) == 10)
        # Cursor position was 0, so the corresponding subset of the proposals list should have been returned
        self.assertEqual(expected_proposals[0:10], paginated_proposals["proposals"])
        # The cursor position should no be at 10, having advanced 10 places
        self.assertTrue(paginated_proposals["cursor"]["current"] == 10)
        # There are a total of 20 proposals, so the final cursor position should be 20
        self.assertTrue(paginated_proposals["cursor"]["final"] == 20)
