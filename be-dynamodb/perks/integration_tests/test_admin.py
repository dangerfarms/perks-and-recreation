from perks.utils import get_current_year
from perks.utils.graphql import execute_query
from perks.integration_tests import LocalDynamoDBTestCase


class AdminGraphQLTest(LocalDynamoDBTestCase):
    def test_should_create_budget_and_threshold(self):
        self.proposals_table.put_item(
            Item={"partition_key": "USER", "sort_key": "lewis@dangerfarms.com", "is_admin": True}
        )

        mutation = """
          mutation ChangeAdminSettings($totalBudget: Int!, $threshold: Int!) {
            changeAdminSettings(totalBudget: $totalBudget, threshold: $threshold) {
              budget {
                totalBudget
                remainingBudget
              }
              threshold {
                threshold
              }
            }
          }
        """

        execute_query(
            mutation, jwt_claims={"email": "lewis@dangerfarms.com"}, variables={"totalBudget": 5000, "threshold": 50}
        )

        budget_item = self.proposals_table.get_item(
            Key={"partition_key": "BUDGET", "sort_key": get_current_year()}
        ).get("Item")
        threshold_item = self.proposals_table.get_item(Key={"partition_key": "THRESHOLD", "sort_key": "THRESHOLD"}).get(
            "Item"
        )

        self.assertIsNotNone(budget_item)
        self.assertIsNotNone(threshold_item)
        self.assertEqual(5000, budget_item["total_budget"])

        # Remaining budget equal to total should have been assigned automatically
        self.assertEqual(budget_item["remaining_budget"], budget_item["total_budget"])

        self.assertEqual(50, threshold_item["threshold"])

    def test_should_not_allow_remaining_budget_to_exceed_total(self):
        year = get_current_year()
        self.proposals_table.put_item(
            Item={"partition_key": "USER", "sort_key": "lewis@dangerfarms.com", "is_admin": True}
        )
        self.proposals_table.put_item(
            Item={"partition_key": "BUDGET", "sort_key": year, "total_value": 5000, "remaining_value": 5000}
        )

        mutation = """
          mutation ChangeAdminSettings($totalBudget: Int!) {
            changeAdminSettings(totalBudget: $totalBudget) {
              budget {
                totalBudget
                remainingBudget
              }
            }
          }
        """

        execute_query(mutation, jwt_claims={"email": "lewis@dangerfarms.com"}, variables={"totalBudget": 4000})

        item = self.proposals_table.get_item(Key={"partition_key": "BUDGET", "sort_key": year}).get("Item")

        self.assertIsNotNone(item)

        self.assertEqual(4000, item["total_budget"])
        self.assertEqual(item["remaining_budget"], item["total_budget"])

    def test_should_not_change_settings_if_not_admin(self):
        year = get_current_year()
        self.proposals_table.put_item(
            Item={"partition_key": "BUDGET", "sort_key": year, "total_budget": 5000, "remaining_budget": 5000}
        )
        self.proposals_table.put_item(Item={"partition_key": "THRESHOLD", "sort_key": "THRESHOLD", "threshold": 50})
        self.proposals_table.put_item(
            Item={"partition_key": "USER", "sort_key": "max@dangerfarms.com", "is_admin": False}
        )

        mutation = """
          mutation ChangeAdminSettings($totalBudget: Int!, $threshold: Int!) {
            changeAdminSettings(totalBudget: $totalBudget, threshold: $threshold) {
              budget {
                totalBudget
                remainingBudget
              }
              threshold {
                threshold
              }
            }
          }
        """

        execute_query(
            mutation, jwt_claims={"email": "max@dangerfarms.com"}, variables={"budget": 10000, "threshold": 1}
        )

        # Max is not an admin, so neither the budget nor the threshold should have been changed
        budget_item = self.proposals_table.get_item(Key={"partition_key": "BUDGET", "sort_key": year}).get("Item")
        threshold_item = self.proposals_table.get_item(Key={"partition_key": "THRESHOLD", "sort_key": "THRESHOLD"}).get(
            "Item"
        )

        self.assertEqual(5000, budget_item["total_budget"])
        self.assertEqual(50, threshold_item["threshold"])
