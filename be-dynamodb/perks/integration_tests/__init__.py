import os
from unittest import TestCase

from mock import patch

from perks.mocks import mock_environment
from perks.utils.aws_resources import create_proposals_table


class LocalDynamoDBTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.env_patch = patch.dict(os.environ, mock_environment)
        cls.env_patch.start()

    def setUp(self):
        self.proposals_table = create_proposals_table()

    @classmethod
    def tearDownClass(cls):
        cls.env_patch.stop()

    def tearDown(self):
        self.proposals_table.delete()
