from mock import patch

from perks.utils.factories import ProposalFactory, UserFactory, ThresholdFactory
from perks.utils.graphql import execute_query
from perks.utils.constants import ProposalStatuses, example_users
from perks.integration_tests import LocalDynamoDBTestCase


class VoteGraphQLTest(LocalDynamoDBTestCase):
    def test_should_remove_pending_vote(self):
        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"
        self.proposals_table.put_item(Item=UserFactory.build(sort_key="craig@dangerfarms.com"))
        self.proposals_table.put_item(Item=UserFactory.build(sort_key="tom@dangerfarms.com"))
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-craig@dangerfarms.com", "sort_key": proposal_id}
        )
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-tom@dangerfarms.com", "sort_key": proposal_id}
        )
        self.proposals_table.put_item(Item=ProposalFactory.build(sort_key=proposal_id))
        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """
        variables = {"proposal": proposal_id, "accept": True}

        execute_query(mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables=variables)

        # Tom has voted now, so assert that his pending vote has disappeared
        item = self.proposals_table.get_item(
            Key={"partition_key": "PENDING_VOTE-craig@dangerfarms.com", "sort_key": proposal_id}
        ).get("Item")
        self.assertIsNotNone(item)
        item = self.proposals_table.get_item(
            Key={"partition_key": "PENDING_VOTE-tom@dangerfarms.com", "sort_key": proposal_id}
        ).get("Item")
        self.assertIsNone(item)

    def test_should_add_user_vote_to_proposal(self):
        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"
        self.proposals_table.put_item(Item=UserFactory.build(sort_key="tom@dangerfarms.com"))
        self.proposals_table.put_item(Item=ProposalFactory.build(sort_key=proposal_id))
        self.proposals_table.put_item(Item=ThresholdFactory.build())
        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """
        variables = {"proposal": proposal_id, "accept": True}

        execute_query(mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables=variables)

        item = self.proposals_table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id})["Item"]
        self.assertListEqual([{"user": "tom@dangerfarms.com", "accept": True}], item["votes"])
        self.assertEqual(1, item["vote_count"])

    def test_proposal_status_should_update_on_final_vote(self):
        succeeding_proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"
        failing_proposal_id = "PROPOSAL-2019-02-03T12:00:00Z"

        self.proposals_table.put_item(Item=ThresholdFactory.build())
        for user in example_users:
            self.proposals_table.put_item(Item=UserFactory.build(sort_key=user))

        voters = example_users[:]
        pending_vote_user = voters.pop(0)
        succeeding_proposal = ProposalFactory.build(
            sort_key=succeeding_proposal_id,
            vote_count=len(voters),
            votes=[{"user": voter, "accept": True} for voter in voters],
        )
        failing_proposal = ProposalFactory.build(
            sort_key=failing_proposal_id,
            vote_count=len(voters),
            votes=[{"user": voter, "accept": False} for voter in voters],
        )
        self.proposals_table.put_item(Item=succeeding_proposal)
        self.proposals_table.put_item(Item=failing_proposal)

        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """

        # Final vote goes against the majority, but status should update according to majority
        execute_query(
            mutation,
            jwt_claims={"email": pending_vote_user},
            variables={"proposal": succeeding_proposal_id, "accept": False},
        )
        execute_query(
            mutation,
            jwt_claims={"email": pending_vote_user},
            variables={"proposal": failing_proposal_id, "accept": True},
        )

        succeeding_item = self.proposals_table.get_item(
            Key={"partition_key": "PROPOSAL", "sort_key": succeeding_proposal_id}
        )["Item"]
        failing_item = self.proposals_table.get_item(
            Key={"partition_key": "PROPOSAL", "sort_key": failing_proposal_id}
        )["Item"]

        self.assertEqual(ProposalStatuses.APPROVED, succeeding_item["status"])
        self.assertEqual(ProposalStatuses.REJECTED, failing_item["status"])

    def test_voting_threshold_determines_outcome(self):
        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"

        for user in example_users:
            self.proposals_table.put_item(Item=UserFactory.build(sort_key=user))

        voters = example_users[:]
        final_vote_user = voters.pop(0)
        proposal = ProposalFactory.build(
            sort_key=proposal_id,
            threshold=100,
            vote_count=len(voters),
            votes=[{"user": voter, "accept": True} for voter in voters],
        )
        self.proposals_table.put_item(Item=proposal)

        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """

        # Even though every other vote is in favour, a single vote against should result
        # in the proposal being rejected because the threshold has been set to 100%
        execute_query(
            mutation, jwt_claims={"email": final_vote_user}, variables={"proposal": proposal_id, "accept": False}
        )

        item = self.proposals_table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id})["Item"]

        self.assertEqual(ProposalStatuses.REJECTED, item["status"])

    # Very much up for discussion, a placeholder as a mutation needs to return something
    def test_should_return_accept_value(self):
        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"
        self.proposals_table.put_item(Item=UserFactory.build(sort_key="tom@dangerfarms.com"))
        self.proposals_table.put_item(Item=ProposalFactory.build(sort_key=proposal_id))
        self.proposals_table.put_item(Item=ThresholdFactory.build())
        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """
        variables = {"proposal": proposal_id, "accept": True}

        result = execute_query(mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables=variables)

        self.assertEqual(result, {"data": {"vote": {"accept": True}}})

    def test_user_should_be_able_to_change_vote_on_proposal(self):
        for user in example_users:
            self.proposals_table.put_item(Item=UserFactory.build(sort_key=user))

        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                sort_key=proposal_id,
                votes=[
                    {"user": "owen@dangerfarms.com", "accept": True},
                    {"user": "tom@dangerfarms.com", "accept": False},
                    {"user": "max@dangerfarms.com", "accept": True},
                ],
            )
        )
        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """
        variables = {"proposal": proposal_id, "accept": True}

        result = execute_query(mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables=variables)
        proposal = self.proposals_table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id})["Item"]

        self.assertEqual(
            [
                {"user": "owen@dangerfarms.com", "accept": True},
                {"user": "tom@dangerfarms.com", "accept": True},
                {"user": "max@dangerfarms.com", "accept": True},
            ],
            proposal["votes"],
        )
        self.assertEqual(3, proposal["vote_count"])
        self.assertEqual({"vote": {"accept": True}}, result["data"])

    def test_user_should_not_be_able_to_vote_same_way_twice_on_proposal(self):
        for user in example_users:
            self.proposals_table.put_item(Item=UserFactory.build(sort_key=user))

        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"
        self.proposals_table.put_item(
            Item=ProposalFactory.build(sort_key=proposal_id, votes=[{"user": "tom@dangerfarms.com", "accept": False}])
        )
        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """
        variables = {"proposal": proposal_id, "accept": False}

        result = execute_query(mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables=variables)

        proposal = self.proposals_table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id})["Item"]
        self.assertEqual([{"user": "tom@dangerfarms.com", "accept": False}], proposal["votes"])
        self.assertEqual({"vote": None}, result["data"])
        self.assertEqual("User has already voted against this proposal", result["errors"][0]["message"])

    def test_should_not_vote_on_a_proposal_that_does_not_exist(self):
        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"
        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """
        variables = {"proposal": proposal_id, "accept": True}

        result = execute_query(mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables=variables)

        self.assertEqual({"vote": None}, result["data"])
        self.assertEqual("Cannot vote on a proposal that does not exist", result["errors"][0]["message"])

    @patch("perks.data.proposals.send_approved_proposal_email")
    def test_should_send_approval_email_when_proposal_is_approved(self, mock_send_email):
        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"

        self.proposals_table.put_item(Item=ThresholdFactory.build())
        for user in example_users:
            self.proposals_table.put_item(Item=UserFactory.build(sort_key=user))

        voters = example_users[:]
        pending_vote_user = voters.pop(0)
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                sort_key=proposal_id,
                vote_count=len(voters),
                votes=[{"user": voter, "accept": True} for voter in voters],
            )
        )

        mutation = """
            mutation Vote($proposal: String!, $accept: Boolean!) {
                vote(proposal: $proposal, accept: $accept) {
                    accept
                }
            }
        """

        execute_query(
            mutation, jwt_claims={"email": pending_vote_user}, variables={"proposal": proposal_id, "accept": True}
        )

        proposal = self.proposals_table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id})["Item"]

        mock_send_email.assert_called_once_with(proposal)

    @patch("perks.data.proposals.send_rejected_proposal_email")
    def test_should_send_rejected_proposal_email_when_proposal_is_rejected(self, mock_send_email):
        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"

        self.proposals_table.put_item(Item=ThresholdFactory.build())
        for user in example_users:
            self.proposals_table.put_item(Item=UserFactory.build(sort_key=user))

        voters = example_users[:]
        pending_vote_user = voters.pop(0)
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                sort_key=proposal_id,
                vote_count=len(voters),
                votes=[{"user": voter, "accept": False} for voter in voters],
            )
        )

        mutation = """
                mutation Vote($proposal: String!, $accept: Boolean!) {
                    vote(proposal: $proposal, accept: $accept) {
                        accept
                    }
                }
            """

        execute_query(
            mutation, jwt_claims={"email": pending_vote_user}, variables={"proposal": proposal_id, "accept": False}
        )

        proposal = self.proposals_table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id})["Item"]

        mock_send_email.assert_called_once_with(proposal)
