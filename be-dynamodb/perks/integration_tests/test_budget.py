from freezegun import freeze_time

from perks.utils.factories import BudgetFactory
from perks.utils.graphql import execute_query
from perks.integration_tests import LocalDynamoDBTestCase


@freeze_time("2019-01-01T12:00:00")
class BudgetGraphQLTest(LocalDynamoDBTestCase):
    def test_should_return_budget_of_current_year_by_default(self):
        self.proposals_table.put_item(Item=BudgetFactory.build(sort_key="2018", remaining_budget=0, total_budget=2000))
        self.proposals_table.put_item(
            Item=BudgetFactory.build(sort_key="2019", remaining_budget=500, total_budget=2500)
        )
        query = """
            query {
                budget {
                    remainingBudget
                    totalBudget
                }
            }
        """
        result = execute_query(query, jwt_claims={"email": "owen@dangerfarms.com"})

        self.assertEqual(result, {"data": {"budget": {"remainingBudget": 500, "totalBudget": 2500}}})

    def test_should_return_budget_of_year_specified_by_argument(self):
        self.proposals_table.put_item(Item=BudgetFactory.build(sort_key="2018", remaining_budget=0, total_budget=2000))
        self.proposals_table.put_item(
            Item=BudgetFactory.build(sort_key="2019", remaining_budget=500, total_budget=2500)
        )
        query = """
            query ($year: String){
                budget(year: $year) {
                    remainingBudget
                    totalBudget
                }
            }
        """
        variables = {"year": "2018"}
        result = execute_query(query, jwt_claims={"email": "owen@dangerfarms.com"}, variables=variables)

        self.assertEqual(result, {"data": {"budget": {"remainingBudget": 0, "totalBudget": 2000}}})
