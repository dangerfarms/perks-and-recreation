import datetime

from perks.data.proposals import get_proposal
from perks.utils.graphql import execute_query
from perks.utils.factories import ProposalFactory, BudgetFactory, ApprovedProposalFactory
from perks.utils.constants import ProposalStatuses
from perks.utils import get_current_year
from perks.integration_tests import LocalDynamoDBTestCase


purchaseMutation = """
  mutation PurchaseProposal($cost: Int!, $proposalId: String!) {
    purchaseProposal(cost: $cost, proposalId: $proposalId) {
      proposal {
        status
      }
      budget {
        remainingBudget
      }
    }
  }
"""


class ProposalGraphQLTest(LocalDynamoDBTestCase):
    def test_should_return_all_proposals(self):
        proposals = ProposalFactory.build_batch(3, status=ProposalStatuses.APPROVED)

        proposals.append(
            ProposalFactory.build(
                status=ProposalStatuses.OPEN,
                vote_count=3,
                # For now, intentionally leave logged in user out of this, the testing logic is quite complex and
                # requires refactoring. This does make the assertion weaker, however I feel it's a reasonable stopgap
                # solution as long as it's documented, and we can revisit once we have totally figured out the
                # factory/fixture situation regarding tests.
                votes=[
                    {"user": "balint@dangerfarms.com", "accept": True},
                    {"user": "daniel@dangerfarms.com", "accept": False},
                    {"user": "lewis@dangerfarms.com", "accept": True},
                ],
            )
        )

        with self.proposals_table.batch_writer() as batch:
            for proposal in proposals:
                batch.put_item(Item=proposal)

        proposals = sorted(proposals, key=lambda p: p["sort_key"])
        expected_proposals = [
            {
                "createdAt": proposal["created_at"],
                "estimatedCost": proposal["estimated_cost"],
                "description": proposal["description"],
                "name": proposal["name"],
                "owner": proposal["owner"] if proposal["owner"] == "owen@dangerfarms.com" else None,
                "status": proposal["status"],
                "voteCount": proposal["vote_count"],
                "votes": proposal["votes"]
                if proposal["status"] != ProposalStatuses.OPEN
                else [{"user": vote["user"], "accept": None} for vote in proposal["votes"]],
            }
            for proposal in proposals[::-1]
        ]

        query = """
            query {
                proposals {
                    createdAt
                    estimatedCost
                    description
                    name
                    owner
                    status
                    voteCount
                    votes {
                        user
                        accept
                    }
                }
            }
        """
        result = execute_query(query, jwt_claims={"email": "owen@dangerfarms.com"})

        self.assertEqual({"data": {"proposals": expected_proposals}}, result)

    def test_should_not_return_deleted_proposals_in_list(self):
        proposals = [
            ProposalFactory.build(created_at="2019-01-02T12:00:00Z", owner="tom@dangerfarms.com"),
            ProposalFactory.build(is_deleted=True),
        ]

        with self.proposals_table.batch_writer() as batch:
            for proposal in proposals:
                batch.put_item(Item=proposal)

        query = """
            query {
                proposals {
                    createdAt
                    estimatedCost
                    description
                    isDeleted
                    name
                    owner
                    status
                    voteCount
                    votes {
                        user
                        accept
                    }
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"})

        self.assertEqual(
            {
                "data": {
                    "proposals": [
                        {
                            "createdAt": "2019-01-02T12:00:00Z",
                            "isDeleted": None,
                            "name": "Fruit bowl",
                            "owner": "tom@dangerfarms.com",
                            "description": "Fruit bowls make excellent subjects for still-life painting.",
                            "estimatedCost": 100,
                            "status": ProposalStatuses.OPEN,
                            "voteCount": 0,
                            "votes": [],
                        }
                    ]
                }
            },
            result,
        )

    def test_should_return_proposals_user_needs_to_vote_on(self):
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                created_at="2019-01-03T12:00:00Z", name="Standing desk", owner="tom@dangerfarms.com"
            )
        )
        # Max didn't vote for 'Standing desk' yet
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-max@dangerfarms.com", "sort_key": "PROPOSAL-2019-01-03T12:00:00Z"}
        )
        # Neither did Owen
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-owen@dangerfarms.com", "sort_key": "PROPOSAL-2019-01-03T12:00:00Z"}
        )

        # This proposal is *earlier* than 'Standing desk', so should come *first*
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                created_at="2019-01-02T12:00:00Z", name="Fruit bowl", owner="max@dangerfarms.com"
            )
        )
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-owen@dangerfarms.com", "sort_key": "PROPOSAL-2019-01-02T12:00:00Z"}
        )
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-max@dangerfarms.com", "sort_key": "PROPOSAL-2019-01-02T12:00:00Z"}
        )

        query = """
            query GetProposals($filter: String) {
                proposals(filter: $filter) {
                    name
                    owner
                    votes {
                      user
                      accept
                    }
                }
            }
        """
        result = execute_query(
            query, jwt_claims={"email": "max@dangerfarms.com"}, variables={"filter": "needs_my_vote"}
        )

        first_proposal_in_result = result["data"]["proposals"][0]
        second_proposal_in_result = result["data"]["proposals"][1]

        self.assertEqual(first_proposal_in_result["name"], "Fruit bowl")
        self.assertEqual(second_proposal_in_result["name"], "Standing desk")

        # Max is not the owner of the 'Standing desk' proposal, so that field should be hidden from him
        self.assertFalse(second_proposal_in_result["owner"])

        # Max *is* the owner of the 'Standing desk' proposal, so that field should be visible to him
        self.assertEqual(first_proposal_in_result["owner"], "max@dangerfarms.com")

        # The votes object should not have been returned in the response
        self.assertFalse(first_proposal_in_result["votes"])
        self.assertFalse(second_proposal_in_result["votes"])

    def test_should_only_return_current_user_vote_values_if_proposal_is_open(self):
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                votes=[
                    {"user": "tom@dangerfarms.com", "accept": True},
                    {"user": "owen@dangerfarms.com", "accept": False},
                    {"user": "max@dangerfarms.com", "accept": True},
                ]
            )
        )

        query = """
            query {
                proposals {
                    votes {
                        user
                        accept
                    }
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"})

        self.assertEqual(
            {
                "data": {
                    "proposals": [
                        {
                            "votes": [
                                {"user": "tom@dangerfarms.com", "accept": True},
                                {"user": "owen@dangerfarms.com", "accept": None},
                                {"user": "max@dangerfarms.com", "accept": None},
                            ]
                        }
                    ]
                }
            },
            result,
        )

    def test_should_return_vote_values_if_proposal_is_closed(self):
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                status=ProposalStatuses.REJECTED,
                votes=[
                    {"user": "tom@dangerfarms.com", "accept": True},
                    {"user": "owen@dangerfarms.com", "accept": False},
                ],
            )
        )

        query = """
            query {
                proposals {
                    votes {
                        user
                        accept
                    }
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"})

        self.assertEqual(
            {
                "data": {
                    "proposals": [
                        {
                            "votes": [
                                {"user": "tom@dangerfarms.com", "accept": True},
                                {"user": "owen@dangerfarms.com", "accept": False},
                            ]
                        }
                    ]
                }
            },
            result,
        )

    def test_should_return_proposal(self):
        proposal_id = "PROPOSAL-2019-01-02T12:00:00Z"
        self.proposals_table.put_item(Item=ProposalFactory.build(created_at="2019-01-02T12:00:00Z"))

        result = get_proposal(proposal_id)

        self.assertDictEqual(
            {
                "created_at": "2019-01-02T12:00:00Z",
                "description": "Fruit bowls make excellent subjects for still-life painting.",
                "estimated_cost": 100,
                "name": "Fruit bowl",
                "owner": "owen@dangerfarms.com",
                "partition_key": "PROPOSAL",
                "sort_key": proposal_id,
                "status": ProposalStatuses.OPEN,
                "threshold": 50,
                "vote_count": 0,
                "votes": [],
                "is_recurring": False,
            },
            result,
        )

    def test_should_return_none_if_id_does_not_match_a_proposal(self):
        proposal_id = "PROPOSAL-2019-01-02T12:00:00Z"

        result = get_proposal(proposal_id)

        self.assertIsNone(result)

    def test_should_update_proposal_status_and_remaining_budget_on_purchase(self):
        self.proposals_table.put_item(Item=BudgetFactory.build(remaining_budget=1000))

        proposal = ProposalFactory.build(status=ProposalStatuses.APPROVED)

        self.proposals_table.put_item(Item=proposal)

        execute_query(
            purchaseMutation,
            jwt_claims={"email": "lewis@dangerfarms.com"},
            variables={"cost": 1000, "proposalId": proposal["sort_key"]},
        )

        proposal_item = self.proposals_table.get_item(
            Key={"partition_key": "PROPOSAL", "sort_key": proposal["sort_key"]}
        ).get("Item")
        budget_item = self.proposals_table.get_item(
            Key={"partition_key": "BUDGET", "sort_key": get_current_year()}
        ).get("Item")

        self.assertEqual(ProposalStatuses.PURCHASED, proposal_item["status"])
        self.assertEqual(0, budget_item["remaining_budget"])

    def test_should_reject_purchase_of_proposal_with_cost_exceeding_remaining_budget(self):
        self.proposals_table.put_item(Item=BudgetFactory.build(remaining_budget=1000))

        proposal = ProposalFactory.build(status=ProposalStatuses.APPROVED)

        self.proposals_table.put_item(Item=proposal)

        execute_query(
            purchaseMutation,
            jwt_claims={"email": "lewis@dangerfarms.com"},
            variables={"cost": 1001, "proposalId": proposal["sort_key"]},
        )

        proposal_item = self.proposals_table.get_item(
            Key={"partition_key": "PROPOSAL", "sort_key": proposal["sort_key"]}
        ).get("Item")
        budget_item = self.proposals_table.get_item(
            Key={"partition_key": "BUDGET", "sort_key": get_current_year()}
        ).get("Item")

        # The proposal status and the remaining budget should both be unchanged
        self.assertEqual(ProposalStatuses.APPROVED, proposal_item["status"])
        self.assertEqual(1000, budget_item["remaining_budget"])

    def test_should_reject_purchase_of_proposal_without_approved_status(self):
        self.proposals_table.put_item(Item=BudgetFactory.build(remaining_budget=1000))

        proposal = ProposalFactory.build(status=ProposalStatuses.REJECTED)

        self.proposals_table.put_item(Item=proposal)

        execute_query(
            purchaseMutation,
            jwt_claims={"email": "lewis@dangerfarms.com"},
            variables={"cost": 1000, "proposalId": proposal["sort_key"]},
        )

        proposal_item = self.proposals_table.get_item(
            Key={"partition_key": "PROPOSAL", "sort_key": proposal["sort_key"]}
        ).get("Item")
        budget_item = self.proposals_table.get_item(
            Key={"partition_key": "BUDGET", "sort_key": get_current_year()}
        ).get("Item")

        # The proposal status and the remaining budget should both be unchanged
        self.assertEqual(ProposalStatuses.REJECTED, proposal_item["status"])
        self.assertEqual(1000, budget_item["remaining_budget"])

    def test_should_mark_my_proposal_as_deleted_if_still_open(self):
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                created_at="2019-01-03T12:00:00Z", name="Standing desk", owner="tom@dangerfarms.com"
            )
        )

        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"

        mutation = """
            mutation DeleteProposal($proposal: String!) {
                deleteProposal(proposal: $proposal) {
                    ok
                }
            }
        """

        result = execute_query(
            mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables={"proposal": proposal_id}
        )

        item = self.proposals_table.get_item(Key={"partition_key": "PROPOSAL", "sort_key": proposal_id}).get("Item")

        self.assertTrue(item["is_deleted"])
        self.assertEqual({"data": {"deleteProposal": {"ok": True}}}, result)

    def test_should_not_mark_as_deleted_if_not_open(self):
        self.proposals_table.put_item(
            Item=ApprovedProposalFactory.build(
                created_at="2019-01-03T12:00:00Z", name="Standing desk", owner="tom@dangerfarms.com"
            )
        )

        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"

        mutation = """
            mutation DeleteProposal($proposal: String!) {
                deleteProposal(proposal: $proposal) {
                    ok
                }
            }
        """

        result = execute_query(
            mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables={"proposal": proposal_id}
        )
        self.assertIsNone(result["data"]["deleteProposal"])
        self.assertEqual("Proposal can only be deleted if it is still open", result["errors"][0]["message"])

    def test_should_not_mark_as_deleted_if_not_owner_of_proposal(self):
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                created_at="2019-01-03T12:00:00Z", name="Standing desk", owner="max@dangerfarms.com"
            )
        )

        proposal_id = "PROPOSAL-2019-01-03T12:00:00Z"

        mutation = """
            mutation DeleteProposal($proposal: String!) {
                deleteProposal(proposal: $proposal) {
                    ok
                }
            }
        """

        result = execute_query(
            mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables={"proposal": proposal_id}
        )
        self.assertEqual({"deleteProposal": None}, result["data"])
        self.assertEqual("Cannot delete someone else's proposal", result["errors"][0]["message"])

    def test_should_return_error_if_proposal_does_not_exist(self):
        proposal_id = "PROPOSAL-2011-01-01T13:00:00Z"

        mutation = """
            mutation DeleteProposal($proposal: String!) {
                deleteProposal(proposal: $proposal) {
                    ok
                }
            }
        """

        result = execute_query(
            mutation, jwt_claims={"email": "tom@dangerfarms.com"}, variables={"proposal": proposal_id}
        )
        self.assertEqual({"deleteProposal": None}, result["data"])
        self.assertEqual(
            "Cannot delete proposal PROPOSAL-2011-01-01T13:00:00Z, it does not exist", result["errors"][0]["message"]
        )

    def test_should_edit_proposal(self):
        proposal_timestamp = "2019-01-03T12:00:00Z"

        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                created_at=proposal_timestamp,
                name="Standing desk",
                estimated_cost=50,
                description="Get off yer ass ya lazy sod",
                owner="max@dangerfarms.com",
            )
        )

        mutation = """
            mutation EditProposal(
                $proposal: String!
                $name: String!
                $estimatedCost: Int!
                $description: String
            ) {
                editProposal(
                    proposal: $proposal
                    name: $name
                    estimatedCost: $estimatedCost
                    description: $description
                ) {
                    proposal {
                        name
                        estimatedCost
                        description
                    }
                    ok
                }
            }
        """

        result = execute_query(
            mutation,
            jwt_claims={"email": "max@dangerfarms.com"},
            variables={
                "proposal": f"PROPOSAL-{proposal_timestamp}",
                "name": "Stand up and be counted desk",
                "estimatedCost": 100,
                "description": "Sitting down all day is bad for you",
            },
        )

        item = self.proposals_table.get_item(
            Key={"partition_key": "PROPOSAL", "sort_key": f"PROPOSAL-{proposal_timestamp}"}
        ).get("Item")

        self.assertTrue(result["data"]["editProposal"]["ok"])
        self.assertEqual(
            {
                "name": "Stand up and be counted desk",
                "estimatedCost": 100,
                "description": "Sitting down all day is bad for you",
            },
            result["data"]["editProposal"]["proposal"],
        )

        self.assertEqual("Stand up and be counted desk", item["name"])

    def test_should_only_return_user_owned_proposals_when_filter_applied(self):
        proposal_id_1 = "PROPOSAL-2019-01-01T12:00:00Z"
        proposal_id_2 = "PROPOSAL-2019-01-02T12:00:00Z"
        self.proposals_table.put_item(
            Item=ProposalFactory.build(sort_key=proposal_id_1, owner="tom@dangerfarms.com", name="Fruit bowl")
        )
        self.proposals_table.put_item(
            Item=ProposalFactory.build(sort_key=proposal_id_2, owner="max@dangerfarms.com", name="Standing desk")
        )
        self.proposals_table.put_item(Item={"partition_key": "OWNER-tom@dangerfarms.com", "sort_key": proposal_id_1})
        self.proposals_table.put_item(Item={"partition_key": "OWNER-max@dangerfarms.com", "sort_key": proposal_id_2})

        query = """
            query GetProposals($filter: String) {
                proposals(filter: $filter) {
                    name
                    owner
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"}, variables={"filter": "mine"})

        self.assertDictEqual({"proposals": [{"name": "Fruit bowl", "owner": "tom@dangerfarms.com"}]}, result["data"])

    def test_should_only_return_purchased_proposals_when_filter_applied(self):
        self.proposals_table.put_item(Item=ProposalFactory.build(status=ProposalStatuses.OPEN))
        self.proposals_table.put_item(Item=ProposalFactory.build(status=ProposalStatuses.PURCHASED))
        self.proposals_table.put_item(Item=ProposalFactory.build(status=ProposalStatuses.APPROVED))

        query = """
            query GetProposals($filter: String) {
                proposals(filter: $filter) {
                    status
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"}, variables={"filter": "purchased"})

        self.assertDictEqual({"proposals": [{"status": "PURCHASED"}]}, result["data"])

    def test_should_only_return_approved_proposals_when_filter_applied(self):
        self.proposals_table.put_item(Item=ProposalFactory.build(status=ProposalStatuses.OPEN))
        self.proposals_table.put_item(Item=ProposalFactory.build(status=ProposalStatuses.PURCHASED))
        self.proposals_table.put_item(Item=ProposalFactory.build(status=ProposalStatuses.APPROVED))

        query = """
            query GetProposals($filter: String) {
                proposals(filter: $filter) {
                    status
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"}, variables={"filter": "approved"})

        self.assertDictEqual({"proposals": [{"status": "APPROVED"}]}, result["data"])

    def test_should_not_return_expired_proposals(self):
        non_expired_open_proposal_timestamp = datetime.datetime.now().isoformat()
        non_expired_closed_proposal_timestamp = datetime.datetime.now().isoformat()
        expired_proposal_timestamp = "2018-07-26T15:29:26.173349"
        self.proposals_table.put_item(Item=ProposalFactory.build(created_at=non_expired_open_proposal_timestamp))
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                created_at=non_expired_closed_proposal_timestamp, status=ProposalStatuses.APPROVED
            )
        )
        self.proposals_table.put_item(Item=ProposalFactory.build(created_at=expired_proposal_timestamp))

        query = """
            query {
                proposals {
                    id
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"})
        expired_proposal = self.proposals_table.get_item(
            Key={"partition_key": "PROPOSAL", "sort_key": f"PROPOSAL-{expired_proposal_timestamp}"}
        ).get("Item")

        self.assertTrue(len(result["data"]["proposals"]) == 2)
        self.assertDictEqual(
            {"id": f"PROPOSAL-{non_expired_closed_proposal_timestamp}"}, result["data"]["proposals"][0]
        )
        self.assertDictEqual({"id": f"PROPOSAL-{non_expired_open_proposal_timestamp}"}, result["data"]["proposals"][1])
        self.assertTrue(expired_proposal["sort_key"] == f"PROPOSAL-{expired_proposal_timestamp}")
        self.assertTrue(expired_proposal["is_deleted"])
