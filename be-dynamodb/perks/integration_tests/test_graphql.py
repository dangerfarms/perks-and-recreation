from freezegun import freeze_time
from graphene.test import Client

from perks.utils.graphql import execute_query
from perks.schemas import schema
from perks.utils.factories import BudgetFactory, ProposalFactory
from perks.utils.constants import ProposalStatuses
from perks.integration_tests import LocalDynamoDBTestCase


@freeze_time("2019-01-01T12:00:00")
class BudgetIntegrationTest(LocalDynamoDBTestCase):
    client = Client(schema)

    def test_should_return_budget_of_current_year_by_default(self):
        self.proposals_table.put_item(Item=BudgetFactory.build(sort_key="2018", remaining_budget=0, total_budget=2000))
        self.proposals_table.put_item(
            Item=BudgetFactory.build(sort_key="2019", remaining_budget=500, total_budget=2500)
        )
        query = """
            query {
                budget {
                    remainingBudget
                    totalBudget
                }
            }
        """
        result = execute_query(query, jwt_claims={"email": "owen@dangerfarms.com"})

        self.assertEqual(result, {"data": {"budget": {"remainingBudget": 500, "totalBudget": 2500}}})

    def test_should_return_budget_of_year_specified_by_argument(self):
        self.proposals_table.put_item(Item=BudgetFactory.build(sort_key="2018", remaining_budget=0, total_budget=2000))
        self.proposals_table.put_item(
            Item=BudgetFactory.build(sort_key="2019", remaining_budget=500, total_budget=2500)
        )
        query = """
            query ($year: String){
                budget(year: $year) {
                    remainingBudget
                    totalBudget
                }
            }
        """
        variables = {"year": "2018"}
        result = execute_query(query, jwt_claims={"email": "owen@dangerfarms.com"}, variables=variables)

        self.assertEqual(result, {"data": {"budget": {"remainingBudget": 0, "totalBudget": 2000}}})


class ProposalIntegrationTest(LocalDynamoDBTestCase):
    def test_should_return_all_proposals(self):
        proposals = ProposalFactory.build_batch(3)
        proposals.append(ProposalFactory.build(status=ProposalStatuses.OPEN, vote_count=3))

        with self.proposals_table.batch_writer() as batch:
            for proposal in proposals:
                batch.put_item(Item=proposal)

        proposals = sorted(proposals, key=lambda p: p["sort_key"])
        expected_proposals = [
            {
                "createdAt": proposal["created_at"],
                "estimatedCost": proposal["estimated_cost"],
                "description": proposal["description"],
                "name": proposal["name"],
                "owner": proposal["owner"] if proposal["owner"] == "owen@dangerfarms.com" else None,
                "status": proposal["status"],
                "voteCount": proposal["vote_count"],
                "votes": proposal["votes"]
                if proposal["status"] != ProposalStatuses.OPEN
                else [{"user": vote["user"], "accept": None} for vote in proposal["votes"]],
            }
            for proposal in proposals[::-1]
        ]

        query = """
            query {
                proposals {
                    createdAt
                    estimatedCost
                    description
                    name
                    owner
                    status
                    voteCount
                    votes {
                        user
                        accept
                    }
                }
            }
        """
        result = execute_query(query, jwt_claims={"email": "owen@dangerfarms.com"})

        non_ordered_dictified_proposals_result = []

        for proposal_result in result["data"]["proposals"]:
            proposal_result = dict(proposal_result)
            proposal_result["votes"] = [dict(vote) for vote in proposal_result["votes"]]
            non_ordered_dictified_proposals_result.append(proposal_result)
            if proposal_result["status"] == "OPEN":
                self.assertTrue(not proposal_result["owner"] or proposal_result["owner"] == "owen@dangerfarms.com")
                for vote in proposal_result["votes"]:
                    self.assertFalse(vote["accept"])

        self.assertEqual(non_ordered_dictified_proposals_result, expected_proposals)

    def test_should_return_proposals_user_needs_to_vote_on(self):
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                created_at="2019-01-03T12:00:00Z", name="Standing desk", owner="tom@dangerfarms.com"
            )
        )
        # Max didn't vote for 'Standing desk' yet
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-max@dangerfarms.com", "sort_key": "PROPOSAL-2019-01-03T12:00:00Z"}
        )
        # Neither did Owen
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-owen@dangerfarms.com", "sort_key": "PROPOSAL-2019-01-03T12:00:00Z"}
        )

        # This proposal is *earlier* than 'Standing desk', so should come *first*
        self.proposals_table.put_item(
            Item=ProposalFactory.build(
                created_at="2019-01-02T12:00:00Z", name="Fruit bowl", owner="max@dangerfarms.com"
            )
        )
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-owen@dangerfarms.com", "sort_key": "PROPOSAL-2019-01-02T12:00:00Z"}
        )
        self.proposals_table.put_item(
            Item={"partition_key": "PENDING_VOTE-max@dangerfarms.com", "sort_key": "PROPOSAL-2019-01-02T12:00:00Z"}
        )

        query = """
            query GetProposals($filter: String) {
                proposals(filter: $filter) {
                    name
                    owner
                    votes {
                      user
                      accept
                    }
                }
            }
        """
        result = execute_query(
            query, jwt_claims={"email": "max@dangerfarms.com"}, variables={"filter": "needs_my_vote"}
        )

        first_proposal_in_result = result["data"]["proposals"][0]
        second_proposal_in_result = result["data"]["proposals"][1]

        self.assertEqual(first_proposal_in_result["name"], "Fruit bowl")
        self.assertEqual(second_proposal_in_result["name"], "Standing desk")

        # Max is not the owner of the 'Standing desk' proposal, so that field should be hidden from him
        self.assertFalse(second_proposal_in_result["owner"])

        # Max *is* the owner of the 'Standing desk' proposal, so that field should be visible to him
        self.assertEqual(first_proposal_in_result["owner"], "max@dangerfarms.com")

        # The votes object should not have been returned in the response
        self.assertFalse(first_proposal_in_result["votes"])
        self.assertFalse(second_proposal_in_result["votes"])

    def test_should_create_new_proposal(self):
        initial_items = self.proposals_table.scan()["Items"]
        self.assertEqual(len(initial_items), 0)

        result = execute_query(
            """
            mutation CreateProposal($name: String!, $estimated_cost: Int!, $description: String) {
                createProposal(name: $name, estimatedCost: $estimated_cost, description: $description) {
                    proposal {
                        name
                        description
                        estimatedCost
                    }
                    ok
                }
            }
            """,
            jwt_claims={"email": "craig@dangerfarms.com"},
            variables={"name": "New Proposal", "description": "New Proposal Description", "estimated_cost": 1_000},
        )

        response_proposal = result["data"]["createProposal"]["proposal"]
        self.assertEqual(response_proposal["name"], "New Proposal")
        self.assertEqual(response_proposal["description"], "New Proposal Description")
        self.assertEqual(response_proposal["estimatedCost"], 1_000)

        resulting_items = self.proposals_table.scan()["Items"]
        self.assertEqual(len(resulting_items), 1)
        self.assertEqual(resulting_items[0]["name"], "New Proposal")
        self.assertEqual(resulting_items[0]["description"], "New Proposal Description")
        self.assertEqual(resulting_items[0]["estimated_cost"], 1_000)

    def test_should_create_new_recurring_proposal(self):
        initial_items = self.proposals_table.scan()["Items"]
        self.assertEqual(len(initial_items), 0)

        result = execute_query(
            """
            mutation CreateProposal($name: String!, $estimated_cost: Int!, $description: String, $is_recurring: Boolean, $first_billing_date: String) {
                createProposal(
                    name: $name,
                    estimatedCost: $estimated_cost,
                    description: $description,
                    isRecurring: $is_recurring,
                    firstBillingDate: $first_billing_date
                ) {
                    proposal {
                        name
                        description
                        estimatedCost
                        isRecurring
                        firstBillingDate
                        createdAt
                    }
                    ok
                }
            }
            """,
            jwt_claims={"email": "craig@dangerfarms.com"},
            variables={
                "name": "New Proposal",
                "description": "New Proposal Description",
                "estimated_cost": 1_000,
                "first_billing_date": "2020-09-23",
                "is_recurring": True,
            },
        )

        response_proposal = result["data"]["createProposal"]["proposal"]
        self.assertEqual(response_proposal["isRecurring"], True)
        self.assertEqual(response_proposal["firstBillingDate"], "2020-09-23")

        resulting_items = self.proposals_table.scan()["Items"]
        self.assertEqual(len(resulting_items), 1)
        self.assertEqual(resulting_items[0]["is_recurring"], True)
        self.assertEqual(resulting_items[0]["first_billing_date"], "2020-09-23")


class UserIntegrationTest(LocalDynamoDBTestCase):
    def test_should_return_user_information(self):
        self.proposals_table.put_item(
            Item={"partition_key": "USER", "sort_key": "tom@dangerfarms.com", "is_admin": False}
        )

        query = """
            query {
                me {
                    email
                    isAdmin
                }
            }
        """

        result = execute_query(query, jwt_claims={"email": "tom@dangerfarms.com"})

        self.assertEqual(result, {"data": {"me": {"email": "tom@dangerfarms.com", "isAdmin": False}}})
