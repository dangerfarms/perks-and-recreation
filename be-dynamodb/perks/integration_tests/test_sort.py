from perks.integration_tests import LocalDynamoDBTestCase
from perks.mocks import (
    mock_unsorted_proposals,
    mock_proposals_results_ascending,
    mock_proposals_results_descending,
    mock_proposals_results_popularity_ascending,
    mock_proposals_results_popularity_descending,
)
from perks.utils.graphql import execute_query


class SortGraphQLTest(LocalDynamoDBTestCase):
    def test_should_sort_proposals_by_date_ascending(self):
        with self.proposals_table.batch_writer() as batch:
            for proposal in mock_unsorted_proposals[:3]:
                batch.put_item(Item=proposal)

            query = """
            query proposals($sort: SortOptions) {
                proposals(sort: $sort) {
                    createdAt
                    estimatedCost
                    description
                    name
                    owner
                    status
                    voteCount
                    votes {
                        user
                        accept
                    }
                }
            }
        """
        result = execute_query(
            query, jwt_claims={"email": "owen@dangerfarms.com"}, variables={"sort": "DATE_ASCENDING"}
        )

        self.assertEqual(mock_proposals_results_ascending, result)

    def test_should_sort_proposals_by_date_descending(self):
        with self.proposals_table.batch_writer() as batch:
            for proposal in mock_unsorted_proposals[:3]:
                batch.put_item(Item=proposal)

            query = """
            query proposals($sort: SortOptions) {
                proposals(sort: $sort) {
                    createdAt
                    estimatedCost
                    description
                    name
                    owner
                    status
                    voteCount
                    votes {
                        user
                        accept
                    }
                }
            }
        """
        result = execute_query(
            query, jwt_claims={"email": "owen@dangerfarms.com"}, variables={"sort": "DATE_DESCENDING"}
        )

        self.assertEqual(mock_proposals_results_descending, result)

    def test_should_sort_proposals_by_popularity_ascending(self):
        with self.proposals_table.batch_writer() as batch:
            for proposal in mock_unsorted_proposals[:3]:
                batch.put_item(Item=proposal)

            query = """
            query proposals($sort: SortOptions) {
                proposals(sort: $sort) {
                    createdAt
                    estimatedCost
                    description
                    name
                    owner
                    status
                    voteCount
                    votes {
                        user
                        accept
                    }
                }
            }
        """
        result = execute_query(
            query, jwt_claims={"email": "owen@dangerfarms.com"}, variables={"sort": "POPULARITY_ASCENDING"}
        )

        self.assertEqual(mock_proposals_results_popularity_ascending, result)

    def test_should_sort_proposals_by_popularity_descending(self):
        with self.proposals_table.batch_writer() as batch:
            for proposal in mock_unsorted_proposals[:3]:
                batch.put_item(Item=proposal)

            query = """
            query proposals($sort: SortOptions) {
                proposals(sort: $sort) {
                    createdAt
                    estimatedCost
                    description
                    name
                    owner
                    status
                    voteCount
                    votes {
                        user
                        accept
                    }
                }
            }
        """
        result = execute_query(
            query, jwt_claims={"email": "owen@dangerfarms.com"}, variables={"sort": "POPULARITY_DESCENDING"}
        )

        self.assertEqual(mock_proposals_results_popularity_descending, result)
