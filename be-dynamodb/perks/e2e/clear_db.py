#!/usr/bin/env python3
import argparse
import os
import sys

# Assumes that this script is only ever called from be/
sys.path.append(os.getcwd())

from perks.utils.aws_resources import get_proposals_table  # noqa: E402


def delete_all_db_items():
    proposals_table = get_proposals_table()
    all_items = proposals_table.scan()["Items"]

    with proposals_table.batch_writer() as writer:
        for item in all_items:
            writer.delete_item(Key={"partition_key": item["partition_key"], "sort_key": item["sort_key"]})

    print("Done")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Deletes all items from the proposals table for e2e testing.")
    parser.add_argument("--table_name", help="The name of the proposals table to be cleared.", required=True)
    parser.add_argument("--region", help="The region the table is deployed in.", default="us-east-1")
    args = parser.parse_args()

    os.environ["PROPOSALS_TABLE"] = args.table_name
    os.environ["REGION"] = args.region

    delete_all_db_items()
