#!/usr/bin/env python3
import argparse
import os
import sys

# Assumes that this script is only ever called from be/
sys.path.append(os.getcwd())

from perks.utils.aws_resources import get_proposals_table  # noqa: E402
from perks.utils.constants import example_users  # noqa: E402
from perks.utils.factories import BudgetFactory, ThresholdFactory  # noqa: E402


def add_non_proposal_items():
    proposals_table = get_proposals_table()
    proposals_table.put_item(Item=BudgetFactory.build(sort_key="2019", remaining_budget=1000, total_budget=3000))
    proposals_table.put_item(Item=BudgetFactory.build(sort_key="2018", remaining_budget=0, total_budget=2000))

    proposals_table.put_item(Item=ThresholdFactory.build())

    for user in example_users:
        is_admin = False
        if user == "lewis@dangerfarms.com":
            is_admin = True
        proposals_table.put_item(Item={"partition_key": "USER", "sort_key": user, "is_admin": is_admin})

    print("Done")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Adds budget, threshold and user data to the proposals table for e2e testing."
    )
    parser.add_argument("--table_name", help="The name of the proposals table to be populated.", required=True)
    parser.add_argument("--region", help="The region the table is deployed in.", default="us-east-1")
    args = parser.parse_args()

    os.environ["PROPOSALS_TABLE"] = args.table_name
    os.environ["REGION"] = args.region

    add_non_proposal_items()
