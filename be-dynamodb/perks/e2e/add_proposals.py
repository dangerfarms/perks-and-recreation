#!/usr/bin/env python3
import argparse
import json
import os
import re
import sys

# Assumes that this script is only ever called from be/
sys.path.append(os.getcwd())

from perks.utils.factories import ProposalFactory  # noqa: E402
from perks.utils.aws_resources import get_proposals_table  # noqa: E402

################################################
# TAKEN FROM SMARTCLAIM
################################################

first_cap_re = re.compile("(.)([A-Z][a-z]+)")
all_cap_re = re.compile("([a-z0-9])([A-Z])")


def to_snake_case(camel_str):
    # https://stackoverflow.com/a/1176023/1617748
    s1 = first_cap_re.sub(r"\1_\2", camel_str)
    return all_cap_re.sub(r"\1_\2", s1).lower()


def transform_dictionary_keys_to_snake_case(dictionary):
    result = {}
    for key, value in dictionary.items():
        result[to_snake_case(key)] = value
    return result


################################################


def add_proposals(json_data):
    proposal_inputs = json.loads(json_data)

    proposals_table = get_proposals_table()

    for proposal_input in proposal_inputs:
        transformed_proposal_input = transform_dictionary_keys_to_snake_case(proposal_input)
        print(f"transformed proposal input: {transformed_proposal_input}")
        proposal = ProposalFactory.build(**transformed_proposal_input)
        proposals_table.put_item(Item=proposal)

    print("Done")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Adds proposal data to the proposals table for e2e testing.")
    parser.add_argument("--table_name", help="The name of the proposals table to be populated.", required=True)
    parser.add_argument("--json_data", help="Proposal data to populate the data, in JSON format.", required=True)
    parser.add_argument("--region", help="The region the table is deployed in.", default="us-east-1")
    args = parser.parse_args()

    os.environ["PROPOSALS_TABLE"] = args.table_name
    os.environ["REGION"] = args.region

    add_proposals(args.json_data)
