import json
from collections import OrderedDict
from perks.utils.constants import ProposalStatuses


mock_environment = {
    "GOOGLE_OAUTH_CLIENT_ID": "test-client-id",
    "MAILGUN_API_KEY": "test-mailgun-key",
    "MAILGUN_DOMAIN": "test-mailgun-domain",
    "PROPOSALS_TABLE": "test-proposals-table",
    "REGION": "us-east-1",
}

mock_unsorted_proposals = [
    {
        "partition_key": "PROPOSAL",
        "sort_key": "PROPOSAL-2019-01-01T12:00:00Z",
        "created_at": "2019-01-01T12:00:00Z",
        "name": "Fruit bowl",
        "description": "Fruit for 2019",
        "estimated_cost": 100,
        "owner": "craig@dangerfarms.com",
        "status": ProposalStatuses.OPEN,
        "threshold": 50,
        "votes": [
            {"accept": True, "user": "owen@dangerfarms.com"},
            {"accept": True, "user": "tom@dangerfarms.com"},
            {"accept": False, "user": "max@dangerfarms.com"},
        ],
    },
    {
        "partition_key": "PROPOSAL",
        "sort_key": "PROPOSAL-2017-01-01T12:00:00Z",
        "created_at": "2017-01-01T12:00:00Z",
        "name": "Fruit bowl",
        "description": "Fruit for 2017",
        "estimated_cost": 100,
        "owner": "craig@dangerfarms.com",
        "status": ProposalStatuses.OPEN,
        "threshold": 50,
        "votes": [
            {"accept": False, "user": "owen@dangerfarms.com"},
            {"accept": False, "user": "tom@dangerfarms.com"},
            {"accept": False, "user": "max@dangerfarms.com"},
        ],
    },
    {
        "partition_key": "PROPOSAL",
        "sort_key": "PROPOSAL-2018-01-01T12:00:00Z",
        "created_at": "2018-01-01T12:00:00Z",
        "name": "Fruit bowl",
        "description": "Fruit for 2018",
        "estimated_cost": 100,
        "owner": "craig@dangerfarms.com",
        "status": ProposalStatuses.OPEN,
        "threshold": 50,
        "votes": [
            {"accept": True, "user": "owen@dangerfarms.com"},
            {"accept": False, "user": "tom@dangerfarms.com"},
            {"accept": False, "user": "max@dangerfarms.com"},
        ],
    },
]

mock_proposals_results_ascending = OrderedDict(
    [
        (
            "data",
            OrderedDict(
                [
                    (
                        "proposals",
                        [
                            OrderedDict(
                                [
                                    ("createdAt", "2017-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2017"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", False)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                            OrderedDict(
                                [
                                    ("createdAt", "2018-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2018"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", True)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                            OrderedDict(
                                [
                                    ("createdAt", "2019-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2019"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", True)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                        ],
                    )
                ]
            ),
        )
    ]
)


mock_proposals_results_descending = OrderedDict(
    [
        (
            "data",
            OrderedDict(
                [
                    (
                        "proposals",
                        [
                            OrderedDict(
                                [
                                    ("createdAt", "2019-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2019"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", True)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                            OrderedDict(
                                [
                                    ("createdAt", "2018-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2018"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", True)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                            OrderedDict(
                                [
                                    ("createdAt", "2017-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2017"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", False)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                        ],
                    )
                ]
            ),
        )
    ]
)

mock_proposals_results_popularity_ascending = OrderedDict(
    [
        (
            "data",
            OrderedDict(
                [
                    (
                        "proposals",
                        [
                            OrderedDict(
                                [
                                    ("createdAt", "2017-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2017"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", False)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                            OrderedDict(
                                [
                                    ("createdAt", "2018-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2018"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", True)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                            OrderedDict(
                                [
                                    ("createdAt", "2019-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2019"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", True)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                        ],
                    )
                ]
            ),
        )
    ]
)

mock_proposals_results_popularity_descending = OrderedDict(
    [
        (
            "data",
            OrderedDict(
                [
                    (
                        "proposals",
                        [
                            OrderedDict(
                                [
                                    ("createdAt", "2019-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2019"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", True)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                            OrderedDict(
                                [
                                    ("createdAt", "2018-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2018"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", True)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                            OrderedDict(
                                [
                                    ("createdAt", "2017-01-01T12:00:00Z"),
                                    ("estimatedCost", 100),
                                    ("description", "Fruit for 2017"),
                                    ("name", "Fruit bowl"),
                                    ("owner", None),
                                    ("status", "OPEN"),
                                    ("voteCount", None),
                                    (
                                        "votes",
                                        [
                                            OrderedDict([("user", "owen@dangerfarms.com"), ("accept", False)]),
                                            OrderedDict([("user", "tom@dangerfarms.com"), ("accept", None)]),
                                            OrderedDict([("user", "max@dangerfarms.com"), ("accept", None)]),
                                        ],
                                    ),
                                ]
                            ),
                        ],
                    )
                ]
            ),
        )
    ]
)


class MockMailgunResponse:
    status_code = 200
    text = json.dumps({"id": "<a.b.c@mailgun.dangerfarms.com>", "message": "Queued. Thank you."})
