output "projectId" {
  value = var.project
}

output "region" {
  value = var.region
}

output "db_name" {
  value = module.db.db_name
}

output "db_password" {
  value = module.db.db_password
  sensitive = true
}

output "static_bucket_name" {
  value = module.static_bucket.bucket_name
}

output "media_bucket_name" {
  value = module.media_bucket.bucket_name
}
