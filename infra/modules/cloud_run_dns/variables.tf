variable "cloudflare_zone_id" {}

variable "cloud_run_target_service" {
  description = "The name of the Cloud Run service that traffic should be routed to."
}

variable "dns_name" {
  description = "The subdomain of the record, eg. 'perks-api-dev' for perks-api-dev.dangerfarms.com"
}

variable "gcloud_project" {}

variable "gcloud_region" {}
