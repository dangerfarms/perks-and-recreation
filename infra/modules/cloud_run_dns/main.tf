# This "module" feels a wee bit detached without the actual Cloud Run instance being defined
# in Terraform. Could return to this if TF improves its Cloud Run provider in future.

resource "cloudflare_record" "dns" {
  zone_id = var.cloudflare_zone_id
  name = var.dns_name
  type = "CNAME"
  value = "ghs.googlehosted.com"
}

resource "google_cloud_run_domain_mapping" "default" {
  location = var.gcloud_region
  provider = google-beta
  name     = "${var.dns_name}.dangerfarms.com"

  metadata {
    namespace = var.gcloud_project
  }

  spec {
    certificate_mode = "AUTOMATIC"
    route_name = var.cloud_run_target_service
  }
}
