output "db_name" {
  value = google_sql_database.database.name
}

output "db_password" {
  value = google_sql_user.user.password
  sensitive = true
}
