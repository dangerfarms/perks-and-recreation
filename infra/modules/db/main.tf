# Creates a DB in the shared Postgres instance from core-infrastructure

data "terraform_remote_state" "shared_postgres" {
  backend = "gcs"

  config = {
    bucket = "danger-farms-tf-state"
    prefix = "shared-postgres-database"
  }
}

resource "google_sql_database" "database" {
  name = var.db_name
  instance = data.terraform_remote_state.shared_postgres.outputs.instance_name
}

resource "google_sql_user" "user" {
  name = "postgres"
  instance = data.terraform_remote_state.shared_postgres.outputs.instance_name
  password = var.db_password
}
