resource "google_storage_bucket" "bucket" {
  name = var.bucket_name
  location = "EU"

  cors {
    origin = ["*"]
    method = ["*"]
  }
}

resource "google_storage_bucket_iam_binding" "admin-binding" {
  bucket = google_storage_bucket.bucket.name
  role = "roles/storage.admin"

  members = [
    "serviceAccount:${var.google_service_account_email}",
  ]
}

resource "google_storage_bucket_iam_binding" "all-users-binding" {
  bucket = google_storage_bucket.bucket.name
  role = "roles/storage.objectViewer"

  members = [
    "allUsers",
  ]
}
