# TODO: Add service account with correct roles

terraform {
  backend "gcs" {
    bucket = "danger-farms-tf-state"
    prefix = "perks"
  }
}

provider "google" {
  credentials = file(var.credentials_file)

  project = var.project
  region = var.region
  zone = var.zone
}

provider "google-beta" {
  credentials = file(var.credentials_file)

  project = var.project
  region = var.region
}

provider "cloudflare" {
  version = "~> 2.1"

  # Credentials pulled from CLOUDFLARE_EMAIL and CLOUDFLARE_API_KEY environment variables
}

locals {
  stage_suffix = terraform.workspace == "prod" ? "" : "-${terraform.workspace}"
  api_subdomain = "${var.perks_api_name}${local.stage_suffix}"
  web_subdomain = "${var.perks_web_name}${local.stage_suffix}"
}

module "db" {
  source ="./modules/db"
  db_name = local.api_subdomain
  db_password = var.db_password
}

module "static_bucket" {
  source = "./modules/backend_bucket"
  bucket_name = "${local.api_subdomain}-static"
  google_service_account_email = var.google_service_account_email
}

module "media_bucket" {
  source = "./modules/backend_bucket"
  bucket_name = "${local.api_subdomain}-media"
  google_service_account_email = var.google_service_account_email
}

module "cloud_run_api_dns" {
  source = "./modules/cloud_run_dns"
  cloudflare_zone_id = var.cloudflare_zone_id
  cloud_run_target_service = "perks-api-dev"
  dns_name = local.api_subdomain
  gcloud_project = var.project
  gcloud_region = var.region
}

module "cloud_run_web_dns" {
  source = "./modules/cloud_run_dns"
  cloudflare_zone_id = var.cloudflare_zone_id
  cloud_run_target_service = "perks-web-dev"
  dns_name = local.web_subdomain
  gcloud_project = var.project
  gcloud_region = var.region
}
