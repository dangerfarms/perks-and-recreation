variable "project" {}

variable "credentials_file" {}

variable "google_service_account_email" {}

variable "region" {
  default = "europe-west1"
}

variable "zone" {
  default = "europe-west1-c"
}

variable "perks_api_name" {
    description = "The name of the API, excluding the stage. E.g. 'perks-api'"
}

variable "perks_web_name" {
  description = "The subdomain prefix for the user-facing website, excluding stage eg. 'perks' for both perks.dangerfarms.com and perks-dev.dangerfarms.com"
}

variable "db_password" {}

variable "cloudflare_zone_id" {}
